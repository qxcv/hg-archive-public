/* Copyright (c) 2013 Sam Toyer <sam@qxcv.net>

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef BPACK_H_GUARD
#define BPACK_H_GUARD

#include <ctype.h>
#ifndef __cplusplus
    #include <stdbool.h>
#endif
#include <stdarg.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>

enum bpack_format_types {
    BFMT_PAD = 'x', // 1 byte
    BFMT_CHAR = 'c', // 1 byte
    BFMT_SCHAR = 'b', // 1 byte
    BFMT_UCHAR = 'B', // 1 byte
    BFMT_BOOL = '?', // 1 byte
    BFMT_SHORT = 'h', // 2 bytes
    BFMT_USHORT = 'H', // 2 bytes
    BFMT_INT = 'i', // 4 bytes
    BFMT_UINT = 'I', // 4 bytes
    BFMT_LONG = 'l', // 4 bytes
    BFMT_ULONG = 'L', // 4 bytes
    BFMT_LLONG = 'q', // 8 bytes
    BFMT_ULLONG = 'Q', // 8 bytes
    BFMT_FLOAT = 'f', // 4 bytes
    BFMT_DOUBLE = 'd', // 8 bytes
    BFMT_LITTLE = '<', // little endian flag (0 bytes)
    BFMT_BIG = '>', // big endian flag (0 bytes)
    BFMT_NET = '!', // network order flag (equivalent to big endian flag)
};

enum bpack_byte_order {
    BORD_LITTLE, // little endian
    BORD_BIG, // big endian
};

// silly names so that we avoid collisions
union bpack_tokdata_union {
    enum bpack_format_types type;
    enum bpack_byte_order flag;
};

struct bpack_tok_struct {
    int count; // this is int so it can be negative :)
    bool is_flag;
    bool is_array; // do we have to put this in an array?
    union bpack_tokdata_union data;
};

// prototypes for packed data -> native datatype
char pack_to_char(uint8_t *src);
int8_t pack_to_schar(uint8_t *src);
uint8_t pack_to_uchar(uint8_t *src);
bool pack_to_bool(uint8_t *src);
int16_t pack_to_short(uint8_t *src, enum bpack_byte_order order);
uint16_t pack_to_ushort(uint8_t *src, enum bpack_byte_order order);
int32_t pack_to_int(uint8_t *src, enum bpack_byte_order order);
uint32_t pack_to_uint(uint8_t *src, enum bpack_byte_order order);
int64_t pack_to_llong(uint8_t *src, enum bpack_byte_order order);
uint64_t pack_to_ullong(uint8_t *src, enum bpack_byte_order order);
float pack_to_float(uint8_t *src, enum bpack_byte_order order);
double pack_to_double(uint8_t *src, enum bpack_byte_order order);

// prototypes for native datatype -> packed data
void char_to_pack(char src, uint8_t *dest);
void schar_to_pack(int8_t src, uint8_t *dest);
void uchar_to_pack(uint8_t src, uint8_t *dest);
void bool_to_pack(bool src, uint8_t *dest);
void short_to_pack(int16_t src, uint8_t *dest, enum bpack_byte_order order);
void ushort_to_pack(uint16_t, uint8_t *dest, enum bpack_byte_order order);
void int_to_pack(int32_t src, uint8_t *dest, enum bpack_byte_order order);
void uint_to_pack(uint32_t src, uint8_t *dest, enum bpack_byte_order order);
void llong_to_pack(int64_t src, uint8_t *dest, enum bpack_byte_order order);
void ullong_to_pack(uint64_t src, uint8_t *dest, enum bpack_byte_order order);
void float_to_pack(float src, uint8_t *dest, enum bpack_byte_order order);
void double_to_pack(double src, uint8_t *dest, enum bpack_byte_order order);

// increments pointer to cnt until it is null, stores result in dst
// and has retcode of 0 on success (non-zero otherwise)
bool parse_fmt(char **cnt, struct bpack_tok_struct *dest);

// internal function to get the size of a representation of the single char
// format type
size_t bpack_tok_size(enum bpack_format_types type);

// estimates the number of bytes required to store data in string fmt
// returns -1 on error
int calcsize(char *fmt);

// pack arguments into dest according to the fmt string
// returns true on success, false otherwise
// pack will not terminate your destination with a null char, and assumes
// that dest is at least calcsize(fmt) chars long
bool pack(char *fmt, void *dest, ...);

// unpack data from src as per fmt string and place in arguments
// all arguments should be pointers
// returns true on success, false otherwise
bool unpack(char *fmt, void *src, ...);

// *** END DATASTRUCTURE AND PROTOTYPE DEFINITIONS ***

char pack_to_char(uint8_t *src) {
    return (char)*src;
}

int8_t pack_to_schar(uint8_t *src) {
    // we strip the leading bit
    int8_t rv = *src & ~(1 << 7);
    // apply two's complement math, if necessary
    return *src & (1 << 7) ? -1 * ((1 << 7) - rv) : rv;
}

uint8_t pack_to_uchar(uint8_t *src) {
    return (unsigned char)*src;
}

bool pack_to_bool(uint8_t *src) {
    return *src ? true : false;
}

int16_t pack_to_short(uint8_t *src, enum bpack_byte_order order) {
    uint16_t rv = pack_to_ushort(src, order);
    return rv & (1 << 15) ? -1 * ((1 << 15) - (rv & ~(1 << 15))) : rv;
}

uint16_t pack_to_ushort(uint8_t *src, enum bpack_byte_order order) {
    switch (order) {
        case BORD_LITTLE:
            return (uint16_t) (src[1] << 8) | (src[0]);
        case BORD_BIG:
            return (uint16_t) (src[0] << 8) | (src[1]);
    }
}

int32_t pack_to_int(uint8_t *src, enum bpack_byte_order order) {
    uint32_t rv = pack_to_uint(src, order);
    return rv & (1 << 31) ? -1 * ((1 << 31) - (rv & ~((1 << 31)))) : rv;
}

uint32_t pack_to_uint(uint8_t *src, enum bpack_byte_order order) {
    switch (order) {
        case BORD_LITTLE:
            return (uint32_t) ((uint32_t) src[3] << 24)
                            | ((uint32_t) src[2] << 16)
                            | ((uint32_t) src[1] << 8)
                            | (uint32_t) src[0];
        case BORD_BIG:
            return (uint32_t) ((uint32_t) src[0] << 24)
                            | ((uint32_t) src[1] << 16)
                            | ((uint32_t) src[2] << 8)
                            | (uint32_t) src[3];
    }
}

int64_t pack_to_llong(uint8_t *src, enum bpack_byte_order order) {
    const uint64_t MASK = 1L << 63;
    uint64_t rv = pack_to_ullong(src, order);
    return rv & MASK ? -1 * (int64_t)(MASK - ((uint64_t)rv & ~MASK)) : (int64_t)rv;
}

uint64_t pack_to_ullong(uint8_t *src, enum bpack_byte_order order) {
    switch (order) {
        case BORD_LITTLE:
            return (uint64_t) ((uint64_t)src[7] << 56)
                            | ((uint64_t)src[6] << 48)
                            | ((uint64_t)src[5] << 40)
                            | ((uint64_t)src[4] << 32)
                            | ((uint64_t)src[3] << 24)
                            | ((uint64_t)src[2] << 16)
                            | ((uint64_t)src[1] << 8)
                            | (uint64_t)src[0];
        case BORD_BIG:
            return (uint64_t) ((uint64_t)src[0] << 56)
                            | ((uint64_t)src[1] << 48)
                            | ((uint64_t)src[2] << 40)
                            | ((uint64_t)src[3] << 32)
                            | ((uint64_t)src[4] << 24)
                            | ((uint64_t)src[5] << 16)
                            | ((uint64_t)src[6] << 8)
                            | (uint64_t)src[7];
    }
}

float pack_to_float(uint8_t *src, enum bpack_byte_order order) {
    uint32_t from_pack = pack_to_uint(src, order);
    int32_t exp = ((from_pack >> 23) & 0xff) - 126;
    int32_t sign = from_pack >> 31 ? -1 : 1;
    // we can't actually store a "true" 0 value due to our pseudo-24-bit
    // mantissa, so ieee754 uses this particular hack, even though it makes zero
    // sense mathematically
    if (exp == 0) {
        // oh, and yes, we can have positive and negative 0
        // please don't ask why
        return sign * 0;
    }
    uint32_t u_mantissa = (from_pack & 0x7fffff) | (1 << 23);
    return sign * ldexpf((float)u_mantissa, exp - 24);
}

double pack_to_double(uint8_t *src, enum bpack_byte_order order) {
    uint64_t from_pack = pack_to_ullong(src, order);
    int64_t exp = ((from_pack >> 52) & 0x7ff) - 1022;
    int64_t sign = from_pack >> 63 ? -1 : 1;
    if (exp == 0) {
        return sign * 0;
    }
    uint64_t u_mantissa = (from_pack & 0xfffffffffffff) | ((uint64_t)1 << 52);
    return sign * ldexp((double)u_mantissa, exp - 53);
}

void char_to_pack(char src, uint8_t *dest) {
    *dest = src;
}

void schar_to_pack(int8_t src, uint8_t *dest) {
    *dest = src < 0 ? (1 << 7) | (uint8_t)((1 << 7) + src) : src;
}

void uchar_to_pack(uint8_t src, uint8_t *dest) {
    *dest = src;
}

void bool_to_pack(bool src, uint8_t *dest) {
    *dest = src ? 1 : 0;
}

void short_to_pack(int16_t src, uint8_t *dest, enum bpack_byte_order order) {
    uint16_t to_pack = src < 0 ? (1 << 15) | (uint16_t)((1 << 15) + src) : src;
    ushort_to_pack(to_pack, dest, order);
}

void ushort_to_pack(uint16_t src, uint8_t *dest, enum bpack_byte_order order) {
    switch (order) {
        case BORD_LITTLE:
            dest[0] = src & 0xFF;
            dest[1] = src >> 8;
            break;
        case BORD_BIG:
            dest[0] = src >> 8;
            dest[1] = src & 0xFF;
            break;
    }
}

void int_to_pack(int32_t src, uint8_t *dest, enum bpack_byte_order order) {
    uint32_t to_pack = src < 0 ? (1 << 31) | (uint32_t)((1 << 31) + src) : (uint32_t)src;
    uint_to_pack(to_pack, dest, order);
}

void uint_to_pack(uint32_t src, uint8_t *dest, enum bpack_byte_order order) {
    switch (order) {
        case BORD_LITTLE:
            dest[3] = src >> 24;
            dest[2] = (src >> 16) & 0xFF;
            dest[1] = (src >> 8) & 0xFF;
            dest[0] = src & 0xFF;
            break;
        case BORD_BIG:
            dest[0] = src >> 24;
            dest[1] = (src >> 16) & 0xFF;
            dest[2] = (src >> 8) & 0xFF;
            dest[3] = src & 0xFF;
            break;
    }
}

void llong_to_pack(int64_t src, uint8_t *dest, enum bpack_byte_order order) {
    const uint64_t MASK = 1L << 63;
    uint64_t to_pack = src < 0 ? MASK | (uint64_t)(MASK + src) : (uint64_t)src;
    ullong_to_pack(to_pack, dest, order);
}

void ullong_to_pack(uint64_t src, uint8_t *dest, enum bpack_byte_order order) {
    switch (order) {
        case BORD_LITTLE:
            dest[7] = src >> 56;
            dest[6] = (src >> 48) & 0xFF;
            dest[5] = (src >> 40) & 0xFF;
            dest[4] = (src >> 32) & 0xFF;
            dest[3] = (src >> 24) & 0xFF;
            dest[2] = (src >> 16) & 0xFF;
            dest[1] = (src >> 8) & 0xFF;
            dest[0] = src & 0xFF;
            break;
        case BORD_BIG:
            dest[0] = src >> 56;
            dest[1] = (src >> 48) & 0xFF;
            dest[2] = (src >> 40) & 0xFF;
            dest[3] = (src >> 32) & 0xFF;
            dest[4] = (src >> 24) & 0xFF;
            dest[5] = (src >> 16) & 0xFF;
            dest[6] = (src >> 8) & 0xFF;
            dest[7] = src & 0xFF;
            break;
    }
}

void float_to_pack(float src, uint8_t *dest, enum bpack_byte_order order) {
    int32_t exponent;
    float mantissa = frexpf(fabs(src), &exponent);
    // we want this to be 8 bits
    // we use 126 instead of 127 because frexp() isn't ieee754
    uint32_t pack_exponent = (uint32_t)(exponent + 126) & 0xff;
    // we want this to be 23 bits, excluding leading bit (assumed to be 1)
    uint32_t pack_mantissa = (uint32_t)ldexpf(mantissa, 24) & 0x7fffff;
    uint32_t to_pack = pack_mantissa | (pack_exponent << 23);
    if (src < 0) {
        to_pack |= 1 << 31;
    }
    uint_to_pack(to_pack, dest, order);
}

void double_to_pack(double src, uint8_t *dest, enum bpack_byte_order order) {
    // int32 since that's what frexp expects
    int32_t exponent;
    double mantissa = frexp(fabs(src), &exponent);
    uint64_t pack_exponent = (uint64_t)(exponent + 1022) & 0x7ff;
    uint64_t pack_mantissa = (uint64_t)ldexp(mantissa, 53) & 0xfffffffffffff;
    uint64_t to_pack = pack_mantissa | (pack_exponent << 52);
    if (src < 0) {
        to_pack |= (uint64_t)1 << 63;
    }
    ullong_to_pack(to_pack, dest, order);
}

bool parse_fmt(char **cnt, struct bpack_tok_struct *dest) { 
    dest->count = -1;
    dest->is_array = false;
    while (**cnt != '\0') {
        switch (**cnt) {
            case BFMT_PAD:
            case BFMT_CHAR:
            case BFMT_SCHAR:
            case BFMT_UCHAR:
            case BFMT_BOOL:
            case BFMT_SHORT: 
            case BFMT_USHORT:
            case BFMT_INT:
            case BFMT_UINT:
            case BFMT_LONG:
            case BFMT_ULONG:
            case BFMT_LLONG:
            case BFMT_ULLONG:
            case BFMT_FLOAT:
            case BFMT_DOUBLE:
                if (dest->count == 0) break; // we want zero of these? nope
                dest->is_flag = false;
                dest->data.type = **cnt;
                if (dest->count == -1) dest->count = 1;
                (*cnt)++; // we have lots of these to increment the char before we return
                return true;
                break;
            case BFMT_LITTLE:
            case BFMT_BIG:
            case BFMT_NET:
                if (dest->count != -1) {
                    return false;
                    (*cnt)++;
                } // no counts before byte order flags
                else {
                    dest->count = 1;
                    dest->is_flag = true;
                    dest->data.flag = **cnt == BFMT_LITTLE ? BORD_LITTLE : BORD_BIG;
                    (*cnt)++;
                    return true;
                }
                break;
            default:
                if (isspace(**cnt)) break; // we ignore whitespace
                else if (isdigit(**cnt)) {
                    if (dest->count != -1) {
                        (*cnt)++;
                        return false; // no double counts like !10 5c
                    }
                    dest->is_array = true;
                    char *endptr;
                    dest->count = strtol(*cnt, &endptr, 10);
                    *cnt = endptr - 1; // the final digit of the numeric string
                }
                else {
                    (*cnt)++;
                    return false; // illegal char
                }
                break;
        }
        (*cnt)++;
    }
    return false;
}

size_t bpack_tok_size(enum bpack_format_types type) {
    switch (type) {
        case BFMT_PAD:
        case BFMT_CHAR:
        case BFMT_UCHAR:
        case BFMT_SCHAR:
        case BFMT_BOOL:
            return 1;
        case BFMT_SHORT:
        case BFMT_USHORT:
            return 2;
        case BFMT_INT:
        case BFMT_UINT:
        case BFMT_LONG:
        case BFMT_ULONG:
        case BFMT_FLOAT:
            return 4;
        case BFMT_LLONG:
        case BFMT_ULLONG:
        case BFMT_DOUBLE:
            return 8;
        // silence useful compiler warnings :o
        default: break;
    }
    return 0;
}

int calcsize(char *fmt) {
    struct bpack_tok_struct token;
    char **cnt = &fmt;
    int size = 0;
    bool success;
    while (**cnt != '\0') {
        success = parse_fmt(cnt, &token); 
        if (!success) return -1;
        if (token.is_flag) continue;
        size += bpack_tok_size(token.data.type) * token.count;
    };
    return size;
}

// this glorious hack courtesy of http://stackoverflow.com/questions/5588855/standard-alternative-to-gccs-va-args-trick
// it's a very, VERY cool way of getting the head of a __VA_ARGS__ list!
#define VA_ARGS_HEAD(...) VA_ARGS_HEAD_TAIL(__VA_ARGS__, garbage value)
#define VA_ARGS_HEAD_TAIL(head, ...) head

// ... is meant to be passed a destination and (optionally) an order
// this seems to be the only way of having an "optional" macro parameter in C
#define PACK_LOOP_INNER(token, conv_func, actual_type, args, va_arg_type, ...) \
    if (token.is_array) {\
        actual_type* val_array = va_arg(args, void*); \
        for (int i = 0; i < token.count; i++) { \
            conv_func(val_array[i], __VA_ARGS__); \
            VA_ARGS_HEAD(__VA_ARGS__) += bpack_tok_size(token.data.type); \
        } \
    } \
    else { \
        conv_func((actual_type)va_arg(args, va_arg_type), __VA_ARGS__); \
        VA_ARGS_HEAD(__VA_ARGS__) += bpack_tok_size(token.data.type); \
    }

bool pack(char *fmt, void *dest, ...) {
    uint8_t *cdest = (uint8_t*)dest;
    va_list args;
    bool success;
    char **cnt = &fmt;
    struct bpack_tok_struct token;
    enum bpack_byte_order order = BORD_BIG;
    va_start(args, dest);
    do {
        success = parse_fmt(cnt, &token);
        if (!success) return false;
        if (token.is_flag) {
            order = token.data.flag;
            continue;
        }
        switch (token.data.type) {
            case BFMT_PAD:
                *cdest = '\0';
                cdest += bpack_tok_size(token.data.type);
                break;
            case BFMT_CHAR:
                PACK_LOOP_INNER(token, char_to_pack, char, args, int, cdest);
                break;
            case BFMT_UCHAR:
                PACK_LOOP_INNER(token, uchar_to_pack, uint8_t, args, int, cdest);
                break;
            case BFMT_SCHAR:
                PACK_LOOP_INNER(token, schar_to_pack, int8_t, args, int, cdest);
                break;
            case BFMT_BOOL:
                PACK_LOOP_INNER(token, bool_to_pack, bool, args, int, cdest);
                break;
            case BFMT_SHORT:
                PACK_LOOP_INNER(token, short_to_pack, int16_t, args, int, cdest, order);
                break;
            case BFMT_USHORT:
                PACK_LOOP_INNER(token, ushort_to_pack, uint16_t, args, unsigned int, cdest, order);
                break;
            case BFMT_INT:
            case BFMT_LONG:
                PACK_LOOP_INNER(token, int_to_pack, int32_t, args, int32_t, cdest, order);
                break;
            case BFMT_UINT:
            case BFMT_ULONG:
                PACK_LOOP_INNER(token, uint_to_pack, uint32_t, args, uint32_t, cdest, order);
                break;
            case BFMT_FLOAT:
                PACK_LOOP_INNER(token, float_to_pack, float, args, double, cdest, order);
                break;
            case BFMT_LLONG:
                PACK_LOOP_INNER(token, llong_to_pack, int64_t, args, int64_t, cdest, order);
                break;
            case BFMT_ULLONG:
                PACK_LOOP_INNER(token, ullong_to_pack, uint64_t, args, uint64_t, cdest, order);
                break;
            case BFMT_DOUBLE:
                PACK_LOOP_INNER(token, double_to_pack, double, args, double, cdest, order);
                break;
            default: break;
        }
    } while (**cnt != '\0');
    va_end(args);
    return true;
}

// Here, VA_ARGS holds the source (mandatory) and the byte order (if necessary)
#define UNPACK_LOOP_INNER(token, args, actual_type, conv_func, ...) \
    if (token.is_array) { \
        actual_type* arr = va_arg(args, actual_type*); \
        for (int i = 0; i < token.count; i++) { \
            arr[i] = conv_func(__VA_ARGS__); \
            VA_ARGS_HEAD(__VA_ARGS__) += bpack_tok_size(token.data.type); \
        } \
    } \
    else { \
        *(va_arg(args, actual_type*)) = conv_func(__VA_ARGS__); \
        VA_ARGS_HEAD(__VA_ARGS__) += bpack_tok_size(token.data.type); \
    }

bool unpack(char *fmt, void *src, ...) {
    uint8_t *csrc = (uint8_t*)src;
    va_list args;
    bool success;
    char **cnt = &fmt;
    struct bpack_tok_struct token;
    enum bpack_byte_order order = BORD_BIG;
    va_start(args, src);
    do {
        success = parse_fmt(cnt, &token);
        if (!success) return false;
        if (token.is_flag) {
            order = token.data.flag;
            continue;
        }
        switch (token.data.type) {
            case BFMT_PAD:
                csrc += bpack_tok_size(token.data.type);
                break;
            case BFMT_CHAR:
                UNPACK_LOOP_INNER(token, args, char, pack_to_char, csrc);
                break;
            case BFMT_UCHAR:
                UNPACK_LOOP_INNER(token, args, uint8_t, pack_to_uchar, csrc);
                break;
            case BFMT_SCHAR:
                UNPACK_LOOP_INNER(token, args, int8_t, pack_to_schar, csrc);
                break;
            case BFMT_BOOL:
                UNPACK_LOOP_INNER(token, args, bool, pack_to_bool, csrc);
                break;
            case BFMT_SHORT:
                UNPACK_LOOP_INNER(token, args, int16_t, pack_to_short, csrc, order);
                break;
            case BFMT_USHORT:
                UNPACK_LOOP_INNER(token, args, uint16_t, pack_to_ushort, csrc, order);
                break;
            case BFMT_INT:
            case BFMT_LONG:
                UNPACK_LOOP_INNER(token, args, int32_t, pack_to_int, csrc, order);
                break;
            case BFMT_UINT:
            case BFMT_ULONG:
                UNPACK_LOOP_INNER(token, args, uint32_t, pack_to_uint, csrc, order);
                break;
            case BFMT_FLOAT:
                UNPACK_LOOP_INNER(token, args, float, pack_to_float, csrc, order);
                break;
            case BFMT_LLONG:
                UNPACK_LOOP_INNER(token, args, int64_t, pack_to_llong, csrc, order);
                break;
            case BFMT_ULLONG:
                UNPACK_LOOP_INNER(token, args, uint64_t, pack_to_ullong, csrc, order);
                break;
            case BFMT_DOUBLE:
                UNPACK_LOOP_INNER(token, args, double, pack_to_double, csrc, order);
                break;
            default: break;
        }
    } while (**cnt != '\0');
    va_end(args);
    return true;
}

#endif // BPACK_H_GUARD
