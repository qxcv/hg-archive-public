"""This Python file generates the contents of gen.h, a bunch of auto-generated
tests of bpack."""

from struct import pack
from math import frexp

INTEGRAL_TYPES = [
        ('char', 'char', 'c', [ord('h'), 0], False),
        ('schar', 'int8_t', 'b', [-13, 126], False),
        ('uchar', 'uint8_t', 'B', [0, 248], False),
        ('bool', 'bool', '?', [0, 1], False),
        ('short', 'int16_t', 'h', [-3410, 8300, -1530], True),
        ('ushort', 'uint16_t', 'H', [8300, 18001], True),
        ('int', 'int32_t', 'i', [19038102, -181029], True),
        ('uint', 'uint32_t', 'I', [2146251539, 934], True),
        ('llong', 'int64_t', 'q', [-1238912, 870980], True),
        ('ullong', 'uint64_t', 'Q', [9223372036873705937, 187391278], True)
    ]
FLOATING_TYPES = [
        ('float', 'float', 'f', [-9.4, 0, 192.01, 123791298, 0.000129]),
        ('double', 'double', 'd', [1820938.124, 0, 183.38, 109730219, 0.00012])
    ]
BYTE_ORDERS = "<>"
FLOAT_CMP_EPSILON=0.001

def char_to_c_rep(c):
    n = ord(c)
    return '\\x' + hex(n)[2:].zfill(2)

def gen_integral_test(type_name, c_type, type_code, values, requires_bord):
    lines = []
    for value in values:
        for c_bord, py_bord in [('BORD_BIG', '>'), ('BORD_LITTLE', '<')]:
            format_string = py_bord + type_code
            if type_name == 'char':
                # struct.pack needs a special case here
                p = pack(format_string, chr(value))
            else:
                p = pack(format_string, value)
            t_pack = """{0}_to_pack({1}, dest{2});\nTEST_ASSERT(!memcmp(dest, "{3}", {4}));""".format(
                        type_name,
                        value,
                        ", {0}".format(c_bord) if requires_bord else "",
                        ''.join(map(char_to_c_rep, p)),
                        len(p)
                    )
            t_unpack = """TEST_ASSERT(pack_to_{0}(dest{1}) == {2});""".format(
                        type_name,
                        ", {0}".format(c_bord) if requires_bord else "",
                        value
                    )
            lines.append(t_pack)
            lines.append(t_unpack)
    return '\n'.join(lines)

def get_epsilon(f):
    """Get an appropriate float comparison epsilon for the given floating point
    value"""
    mantissa, exponent = frexp(f)
    return FLOAT_CMP_EPSILON * (2 ** exponent)

def gen_floating_test(type_name, c_type, type_code, values):
    lines = []
    for value in values:
        for bord in ['BORD_BIG', 'BORD_LITTLE']:
            test_line = """{0}_to_pack({1}, dest, {2});\nTEST_ASSERT({3} >= fabs({1} - pack_to_{0}(dest, {2})));""".format(
                        type_name,
                        value,
                        bord,
                        get_epsilon(value)
                    )
            lines.append(test_line)
    return '\n'.join(lines)

if __name__ == '__main__':
    for t in INTEGRAL_TYPES:
        print(gen_integral_test(*t))
    # we do float tests a little differently since they are only approximate
    for t in FLOATING_TYPES:
        print(gen_floating_test(*t))
