#include <stdint.h>
#include <string.h>

#include "unit.h"
#include "../bpack.h"

int main(int argc, char **argv) {
    uint8_t dest[1024];
    char uschar;
    int8_t sbyte;
    uint8_t ubyte;
    int16_t sshort;
    uint16_t ushort;
    int32_t sint;
    uint32_t ulong;
    int64_t squad;
    uint64_t uquad;
    int32_t slong;
    float sfloat;
    double sdouble;
    int16_t test_vec_short[3] = {182, -1283, 0};
    uint8_t test_vec_char[1] = {42};
    int16_t dest_vec_short[3];
    uint8_t dest_vec_char[1];
#include "gen.h"
    TEST_ASSERT(bpack_tok_size('c') == 1);
    TEST_ASSERT(bpack_tok_size('Q') == 8);
    TEST_ASSERT(calcsize("") == 0);
    TEST_ASSERT(calcsize("z") == -1);
    TEST_ASSERT(calcsize(">bBiLql") == 22 && calcsize("<bBiLql") == 22);
    TEST_ASSERT(pack(">bBiLql", dest, -13, 13, -16384, 4294967293, -549755813887, 0) &&
            !memcmp(dest, "\xf3\r\xff\xff\xc0\x00\xff\xff\xff\xfd\xff\xff\xff\x80\x00\x00\x00\x01\x00\x00\x00\x00", 22));
    TEST_ASSERT(unpack(">bBiLql", dest, &sbyte, &ubyte, &sint, &ulong, &squad, &slong) &&
            sbyte == -13 &&
            ubyte == 13 &&
            sint == -16384 &&
            ulong == 4294967293 &&
            squad == -549755813887 &&
            slong == 0);
    // as above, but we test the integer types we didn't test before
    TEST_ASSERT(calcsize("<cxhHIQ") == 18 && calcsize(">cxhHIQ") == 18);
    TEST_ASSERT(pack("<cxhHIQ", dest, 42, -1530, 1530, 65000, 68719476736) &&
            !memcmp(dest, "*\x00\x06\xfa\xfa\x05\xe8\xfd\x00\x00\x00\x00\x00\x00\x10\x00\x00\x00", 18));
    TEST_ASSERT(unpack("<cxhHIQ", dest, &uschar, &sshort, &ushort, &ulong, &uquad) &&
            uschar == 42 &&
            sshort == -1530 &&
            ushort == 1530 &&
            ulong == 65000 &&
            uquad == 68719476736);
    // now we test floating types, using the usual floating point testing procedure
    TEST_ASSERT(calcsize(">fd") == 12 && calcsize("<fd") == 12);
    TEST_ASSERT(pack(">fd", dest, -10.1, 259.5333) &&
            unpack(">fd", dest, &sfloat, &sdouble) &&
            (fabsf(sfloat + 10.1) < 0.1) &&
            (fabs(sdouble - 259.53333) < 0.01));
    // a test of array-handling capabilities
    TEST_ASSERT(pack("<3h1B", dest, test_vec_short, test_vec_char) &&
            !memcmp(dest, "\xb6\x00\xfd\xfa\x00\x00*", 7));
    TEST_ASSERT(unpack("<3h1B", dest, dest_vec_short, dest_vec_char) &&
            test_vec_short[0] == dest_vec_short[0] &&
            test_vec_short[1] == dest_vec_short[1] &&
            test_vec_short[2] == dest_vec_short[2] &&
            test_vec_char[0] == dest_vec_char[0]);
    // do our floating point functions handle +/-inf and NaN correctly?
    return 0;
}
