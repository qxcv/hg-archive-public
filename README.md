# Archive of old Mercurial projects from Bitbucket

This is an archive of old (public) projects I had to rescue from Bitbucket after
they deprecated Mercurial support. Most of it was written while I was learning
programming, and is probably unfinished or lacks archival value, but I'm keeping
it here in case any of it becomes useful in the future.
