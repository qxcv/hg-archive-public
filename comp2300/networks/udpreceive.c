#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>          
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>


// Eric McCreath 2011


int
main() {
   printf("UDP Send\n");
   int fd = socket(AF_INET,SOCK_DGRAM,IPPROTO_UDP);
   printf("socket: %d\n",fd);

  

   struct sockaddr_in address, send_address;
   memset((char *) &address, 0, sizeof(address));
   address.sin_family = AF_INET;
   address.sin_port = htons(4321);
   int res = inet_aton("127.0.0.1", &address.sin_addr);
   printf("inet_aton %d\n",res);
     

   int bres = bind(fd,(struct sockaddr *) &address, sizeof(address));
   printf("bind result: %d\n",bres);
   


   printf("receive\n");

   #define BUFLEN 100
   char buf[BUFLEN];
   socklen_t len;
   
   int rres = recvfrom(fd, buf, BUFLEN-1, 
                       0,(struct sockaddr *) &send_address, &len);
   buf[rres] = 0;
   printf("rres : %d result : %s\n",rres, buf );
   
   printf("close\n");
   close(fd);
   return 0;
}
