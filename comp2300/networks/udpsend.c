#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>          
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>


// Eric McCreath 2011


int 
main() {
   printf("UDP Send\n");
   int fd = socket(AF_INET,SOCK_DGRAM,IPPROTO_UDP);
   printf("socket: %d\n",fd);


   struct sockaddr_in address;
   memset((char *) &address, 0, sizeof(address));
   address.sin_family = AF_INET;
   address.sin_port = htons(4321);
   int res = inet_aton("127.0.0.1", &address.sin_addr);
   printf("inet_aton %d\n",res);
     
   
   printf("send : Hello\n");

   int stres = sendto(fd, "Hello", 5, 0,
                      (struct sockaddr *) &address, sizeof(address));

   printf("stres : %d\n",stres);

   printf("close\n");
   close(fd);
   return 0;
}
