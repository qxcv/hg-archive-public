#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>          
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>


// Eric McCreath 2011

#define LISTEN_BACKLOG 1


int
main() {
   printf("TCP Recieve\n");
   int sfd = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
   printf("socket: %d\n",sfd);

  

   struct sockaddr_in address, send_address;
   memset((char *) &address, 0, sizeof(address));
   address.sin_family = AF_INET;
   address.sin_port = htons(4321);
   int res = inet_aton("127.0.0.1", &address.sin_addr);
   printf("inet_aton %d\n",res);
     

   int bres = bind(sfd,(struct sockaddr *) &address, sizeof(address));
   printf("bind result: %d\n",bres);
   
  if (listen(sfd, LISTEN_BACKLOG) == -1)
               printf("listen problem ");

    

           int send_addr_size = sizeof(struct sockaddr);
          int cfd = accept(sfd, (struct sockaddr *) &send_address, &send_addr_size);


   printf("receive\n");

   #define BUFLEN 100
   char buf[BUFLEN];
   socklen_t len;
   
   //int rres = recvfrom(cfd, buf, BUFLEN-1, 
   //                    0,(struct sockaddr *) &send_address, &len);

   int rres = read(cfd,buf,BUFLEN-1);

   buf[rres] = 0;
   printf("rres : %d result : %s\n",rres, buf );
 
   strcpy(buf,"test");
   rres = write(cfd,buf,4);

  
   printf("close\n");
close(cfd);
   close(sfd);
   return 0;
}
