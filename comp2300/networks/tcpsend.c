#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>          
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>


// Eric McCreath 2011


int 
main() {
   printf("TCP Send\n");
   int fd = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
   printf("socket: %d\n",fd);


   struct sockaddr_in address;
   memset((char *) &address, 0, sizeof(address));
   address.sin_family = AF_INET;
   address.sin_port = htons(4321);
   int res = inet_aton("127.0.0.1", &address.sin_addr);
   printf("inet_aton %d\n",res);
     
   int con = connect(fd,(struct sockaddr *) &address, sizeof(address));
   if (con == 0) printf("connect okay");


   printf("send : Hello\n");

   int stres = write(fd, "Hello", 5);
   char retmes[20];
   


   printf("stres : %d\n",stres);
   stres = read(fd, retmes, 20);
   retmes[stres] = 0;
   printf("stres : %d  message : %s \n",stres,retmes);

   printf("close\n");
   close(fd);
   return 0;
}
