
#include<stdio.h>

// Eric McCreath 2011

void showbits(int *p) {
	int size = sizeof(int) * 8;
        int i;
        for (i=0;i<size;i++) {
          if (((*p) >> (size - i -1)) & 1 == 1) {
            printf("1");
          } else {
            printf("0");
          } 
        }
        
}


void 
main() {
   unsigned int val = 0x12345678;
   showbits((int*) &val);
   printf("\n%x\n",val);
   int i;
   char *p;
   p = &val;
for (i =0;i<4;i++) {
  
   printf("%x\n", p[i]);
}

}
