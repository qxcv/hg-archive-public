arg1: block #624129
arg2: block #2061517
0x100:
    push ZERO ; rv
    load arg1 R0
    push R0 ; x
    load arg2 R0
    push R0 ; y
    call gcd
    pop R0
    pop R0
    ; leave RV on
    ; verify that rv = 18913 (0x49e1)!
    push ZERO ; rv
    load #13 R1
    push R1 ; x
    push R1 ; y
    call gcd
    pop R1
    pop R1
    pop R1 ; rv2
    pop R0 ; rv1
    ; verify that rv = 13 (0xD)!
    halt

; int gcd(int x, int y)
; {
;     // x >= y and y >= 0
;     if (y == 0)
;         return x;
;     else
;         return gcd(y, x % y);
; }

; #-1: y
; #-2: x
; #-3: Return value
gcd:
    load SP #-2 R0
    ; r0 = x
    load SP #-1 R1
    ; r1 = y
    jumpz R1 retx
        ; x = x % y
        mod R0 R1 R0
        push ZERO ; RV
        push R1 ; y
        push R0 ; x % y
        call gcd
        pop R0 ; x % y
        pop R0 ; y
        pop R0 ; store return value in R0
    retx:
        store R0 #-3 SP
        return
