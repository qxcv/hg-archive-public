macro
msquare &val &reg
load &val &reg
push &reg
push &reg
call square
pop &reg
pop &reg
mend
0x100:
    msquare #-4 R0
    push R0
    msquare #0x8000 R0
    push R0
    msquare #0x8001 R0
    push R0
    ; 0x8001^2 (overflow)
    pop R2
    ; 0x8000^2 (maximum argument)
    pop R1
    ; (-4)^2 (arbitrary test value)
    pop R0
    halt

; #-1: x
; #-2: rv
; purpose is to rv := x^2
square:
    load SP #-1 R0
    mult R0 R0 R0
    store R0 #-2 SP
    return

; the maximum number this can square should be around 0x8000
