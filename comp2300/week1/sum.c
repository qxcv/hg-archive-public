#include <stdio.h>

int main(int argc, char **argv) {
    int sum;
    while (1) {
        int in;
        if (scanf("%i", &in) <= 0 || in == -1) break;
        sum += in;
    }
    printf("The sum is %i\n", sum);
    return 0;
}
