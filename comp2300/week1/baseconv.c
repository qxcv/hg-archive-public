#include <stdio.h>
#include <stdlib.h>

/* This program converts between different base systems (base two all the way up
 * to base 16).
 * This is probably the worst C program I have ever written. */

#define MAX_LENGTH 1024
#define looong long long /* Loooooong long is looooooooooong */

char *DIGITS = "0123456789ABCDEF";
char *USAGE = "%s <from base> <to base> <number>";

typedef struct CharList {
    /* Doubly-linked list of chars */
    char val;
    struct CharList *next;
    struct CharList *prev;
} CharList;

void write_number(ulong num, ulong base) {
    CharList *current = alloca(sizeof(CharList));
    current->next = NULL;
    while (num > 0) {
        char to_append = DIGITS[num % base];
        current->val = to_append;
        current->prev = alloca(sizeof(CharList));
        current->prev->next = current;
        current = current->prev;
        num /= base;
    }
    while (current->next != NULL) {
        putchar(current->next->val);
        current = current->next;
    }
    putchar('\n');
}

ulong to_num(char c) {
    ulong rv = 0;
    while (rv < 16 && DIGITS[rv] != c) rv++;
    return rv;
}

ulong read_number(ulong base, char *from) {
    ulong rv = 0;
    while (*from) {
        rv += to_num(*from);
        rv *= base;
        from++;
    }
    rv /= base;
    return rv;
}

asplode(){asplode();}

int main(int argc, char **argv) {
    /* ALL YOUR BASE */
    long from_base, to_base;
    if (argc != 4) {
        printf("ASMDOIASDOPIAMSODMASOIDMOAISMDOIMASOIDMASOIDM\n");
        goto AAAA;
    }
    from_base = atoi(argv[1]);
    to_base = atoi(argv[2]);
    if (from_base < 2 || from_base > 16 || to_base < 2 || to_base > 16) {
        printf("ALL YOUR BASE ARE OUT OF RANGE\n");
        while(1){asplode();}
    }
    write_number(read_number(from_base, argv[3]),
            to_base);
    return 0;
AAAA:
    return *(int*)NULL;
}
