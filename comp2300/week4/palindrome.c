/* Takes a string as an argument and checks whether it is a palindrome. */

#include <stdio.h>
#include <string.h>

int main(int argc, char **argv) {
    if (argc != 2) {
        printf("USAGE: %s <string>\n", argv[0]);
        return 1;
    }

    int slen = strlen(argv[1]);
    int i;
    for (i = 0; i < slen / 2; i++) {
        if (argv[1][i] != argv[1][slen - i - 1]) {
            printf("O NOES! It's not a palindrome!\n");
            return 0;
        }
    }
    printf("Yay! It's a palindrome!\n");
    return 0;
}
