#include <stdio.h>
#include <stdlib.h>

#define TARGET_SUM 100
#define INITIAL_VEC_SIZE 10

typedef struct Vector {
    unsigned int length;
    unsigned int capacity;
    int *data;
} Vector;

void vec_init(Vector *res) {
    res->length = 0;
    res->capacity = INITIAL_VEC_SIZE;
    res->data = malloc(sizeof(res->data[0]) * res->capacity);
}

void vec_free(Vector *v) {
    free(v->data);
}

void vec_append(Vector *res, int val) {
    while (res->length >= res->capacity) {
        res->capacity *= 2;
        res->data = realloc(res->data, sizeof(res->data[0]) * res->capacity);
    }
    res->length++;
    res->data[res->length - 1] = val;
}

int compare_ints(void *a, void *b) {
    int x = *(int*)a;
    int y = *(int*)b;
    return x - y;
}

void print_vec(Vector *v) {
    int i;
    for (i = 0; i < v->length; i++) {
        printf("%d, ", v->data[i]);
    }
    printf("\n");
}

int main(int argc, char **argv) {
    Vector vals;
    int in, i, j, k;

    vec_init(&vals);

    while (scanf("%i", &in) > 0) {
        vec_append(&vals, in);
    }

#ifndef FAST_ALGORITHM
    for (i = 0; i < vals.length; i++) {
        for (j = i + 1; j < vals.length; j++) {
            for (k = j + 1; k < vals.length; k++) {
                if (vals.data[i] + vals.data[j] + vals.data[k] == TARGET_SUM) {
                    printf("Success! %i, %i and %i add to %i!\n", vals.data[i], vals.data[j], vals.data[k], TARGET_SUM);
                    goto finish;
                }
            }
        }
    }
#else
    /* We use the Wikipedia algorithm (or some approximation thereof which I
     * remembered */
    qsort(vals.data, vals.length, sizeof(vals.data[0]), &compare_ints);
    int *d = vals.data;
    for (i = 0; i < vals.length - 2; i++) {
        int j, k;
        j = i + 1;
        k = vals.length - 1;
        while (j < k) {
            int s = d[i] + d[j] + d[k];
            if (s == TARGET_SUM) {
                printf("Success! %i, %i and %i add to %i!\n", d[i], d[j], d[k], TARGET_SUM);
                goto finish;
            }
            if (s > TARGET_SUM) {
                k--;
            }
            else {
                j++;
            }
        }
    }
#endif

    printf("There are no three numbers which sum to %i :(\n", TARGET_SUM);

finish:
    vec_free(&vals);

    return 0;
}
