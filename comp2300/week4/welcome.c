#include <stdio.h>

int main(int argc, char **argv) {
    char c = getchar();
    printf("Welcome, ");
    while (c != EOF && c != '\n') {
        putchar(c);
        c = getchar();
    }
    printf("!\n");
    return 0;
}
