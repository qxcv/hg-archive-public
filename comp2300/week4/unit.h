/* This code based on minunit, but only works with GCC */

/* Taken from http://www.jera.com/techinfo/jtns/jtn002.html with the note:
 * "You may use the code in this tech note for any purpose, with the
 *  understanding that it comes with NO WARRANTY."
 */

#include <stdio.h>

int _tests_run = 0;
#define TEST_ASSERT(test) ({_tests_run++;\
        !(test) ? ({printf("Test #%i \x1b[31mfailed\x1b[0m: '%s'\n", _tests_run, #test); 0;})\
        : ({printf("Test #%i \x1b[32mpassed\x1b[0m: '%s'\n", _tests_run, #test); 1;});})
