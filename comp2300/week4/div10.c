#include <stdio.h>

#define TRUE 1
#define FALSE 0

#define DIV10(X) ((X) % 10 ? FALSE : TRUE)

int main() {
    int in;
    scanf("%i", &in);
    if (DIV10(in)) {
        printf("TRUE\n");
    }
    else {
        printf("FALSE\n");
    }
    return 0;
}
