#include <stdio.h>

int main(int argc, char **argv) {
    float min, max, total, in;
    int n = 0;
    int initialised = 0;
    n = total = min = max = 0;
    while (scanf("%f", &in) > 0) {
        if (!initialised) {
            min = max = in;
            initialised = 1;
        }
        else if (in < min) {
            min = in;
        }
        else if (in > max) {
            max = in;
        }
        total += in;
        n++;
    }
    printf("Mean: %f\nMin: %f\nMax: %f\n", total / n, min, max);
    return 0;
}
