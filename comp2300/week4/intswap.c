#define SWAP(X, Y) { \
    (X) ^= (Y); \
    (Y) ^= (X); \
    (X) ^= (Y); \
}

int main() {
    int x = 3;
    int y = 4;
    SWAP(x, y);
    return x / y;
}
