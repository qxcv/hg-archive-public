/* Reads some integers stdin into an array and finds the minimum and maximum */

#include <stdlib.h>
#include <stdio.h>

#define INITIAL_VEC_SIZE 10

typedef struct Vector {
    unsigned int length;
    unsigned int can_carry;
    int *data;
} Vector;

void vec_init(Vector *res) {
    res->length = 0;
    res->can_carry = INITIAL_VEC_SIZE;
    res->data = malloc(sizeof(res->data[0]) * res->can_carry);
}

void vec_free(Vector *v) {
    free(v->data);
}

void vec_append(Vector *res, int val) {
    while (res->length >= res->can_carry) {
        res->can_carry *= 2;
        res->data = realloc(res->data, sizeof(res->data[0]) * res->can_carry);
    }
    res->length++;
    res->data[res->length - 1] = val;
}

int main(int argc, char **argv) {
    Vector vals;
    vec_init(&vals);
    int in, i, min, max, initialised;
    min = max = initialised = 0;
    while (scanf("%i", &in) > 0) {
        vec_append(&vals, in);
    }
    for (i = 0; i < vals.length; i++) {
        if (!initialised) {
            initialised = 1;
            min = max = vals.data[i];
            continue;
        }

        if (vals.data[i] > max) {
            max = vals.data[i];
        }

        if(vals.data[i] < min) {
            max = vals.data[i];
        }
    }
    printf("Min: %d\nMax: %d\n", min, max);
    vec_free(&vals);
    return 0;
}
