// for TEST_ASSERT()
#include "unit.h"

int same(int array[], int length) {
    int i,j;
    for (i=0;i<length;i++) {
        for (j=0;j<length;j++) {
            if (array[i] == array[j] && i != j) return 1;
        }
    }
    return 0;
}

int main(int argc, char **argv) {
    int null_case[] = {0};
    // not actually a null case, but we just check length == 0 handling
    // found a bug: improper comparison with length
    TEST_ASSERT(same(null_case, 0) == 0);

    // found a bug: code checks equality regardless of whether it is handling
    // the same elements
    int single_case[1] = {0};
    TEST_ASSERT(same(single_case, 1) == 0);

    int same_pair[2] = {-1, -1};
    TEST_ASSERT(same(same_pair, 2) == 1);

    // found a bug: "=" in equality check should have been "=="
    // this revealed other bugs
    int unsame_pair[2] = {1239, -12};
    TEST_ASSERT(same(unsame_pair, 2) == 0);

    int dup_list[8] = {10, 2, 1, 0, 2, -12, 13, 3};
    TEST_ASSERT(same(dup_list, 8) == 1);

    int uniq_list[8] = {21290, 91820, 1902, 0, 123, -1239, 91, 6};
    TEST_ASSERT(same(uniq_list, 8) == 0);

    return 0;
}
