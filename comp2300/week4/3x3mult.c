#include <stdio.h>

int read_mat(double mat[3][3]) {
    int i, j;
    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            if (scanf("%lf", &mat[i][j]) != 1) {
                return 1;
            }
        }
    }
    return 0;
}

void mul_mat(double mat1[3][3], double mat2[3][3], double res[3][3]) {
    int i, j, k;
    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            /* The value at res[i][j] is equivalent to this expression */
            res[i][j] = 0;
            for (k = 0; k < 3; k++) {
                res[i][j] += mat1[i][k] * mat2[k][j];
            }
        }
    }
}

void show_mat(double mat[3][3]) {
    int i, j;
    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            printf("%10.2lf ", mat[i][j]);
        }
        printf("\n");
    }
}
int main(int argc, char **argv) {
    double mat1[3][3], mat2[3][3], res[3][3];
    if (read_mat(mat1) || read_mat(mat2)) {
        printf("Supply 18 floats (whitespace is ignored) in stdin, and this program will multiply them.\n");
    }
    mul_mat(mat1, mat2, res);
    show_mat(res);
    return 0;
}
