#include <stdio.h>

unsigned int bitcount(char *arr) {
    unsigned int rv = 0;
    while (*arr != '\0') {
        rv += __builtin_popcount((unsigned int)*arr);
        arr++;
    }
    return rv;
}

int main(int argc, char **argv) {
    if (argc != 2) return 1;
    printf("%u\n", bitcount(argv[1]));
    return 0;
}
