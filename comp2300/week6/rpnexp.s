; 2 * (3 + x)
macro
sadd
pop R0
pop R1
add R1 R0 R0
push R0
mend

macro
smult
pop R0
pop R1
mult R1 R0 R0
push R0
mend

0x100:
    load #2 R0
    push R0
    load #3 R0
    push R0
    load varx R0
    push R0
    sadd
    smult
    pop R0
    halt

varx: block #42
