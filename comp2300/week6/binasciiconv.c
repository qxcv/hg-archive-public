#include <string.h>
#include <stdio.h>
#include <stdbool.h>

int main(int argc, char **argv) {
    if (argc != 3) {
        printf("USAGE: %s <infile> <outfile>\n", argv[0]);
        return 1;
    }
    bool tobin = false;
    if (strcmp(argv[0], "ascii2bin")) {
        // convert ASCII to binary
        tobin = true;
    }
    else { // I would check that argv[0] == "bin2ascii", but I'm lazy.
        // convert binary to ASCII
        tobin = false;
    }

    FILE *infile = fopen(argv[1], tobin ? "r" : "rb");
    if (infile == NULL) {
        fprintf(stderr, "Error: could not open %s\n", argv[1]);
        return 1;
    }
    FILE *outfile = fopen(argv[2], tobin ? "wb" : "w");
    if (outfile == NULL) {
        fclose(infile);
        fprintf(stderr, "Error: could not open %s\n", argv[2]);
        return 1;
    }

    // It's buffered, we can read byte-by-byte
    int inint;
    while (EOF != (inint = fgetc(infile))) {
        char in = (char)inint;
        if (EOF == fputc(in, outfile)) {
            fprintf(stderr, "Error: could not write to %s\n", argv[2]);
            fclose(infile);
            fclose(outfile);
            return 1;
        }
    }

    fclose(infile);
    fclose(outfile);
    return 0;
}
