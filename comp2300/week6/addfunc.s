0x100:
    push R0
    load #45 R0
    push R0
    load #-3 R0
    push R0
    call add
    pop R0 ; param 1
    pop R0 ; param 2
    pop R0
    halt

add:
    ; #0 return address
    ; #-1 a
    ; #-2 b
    ; #-3 return value
    load SP #-1 R0
    load SP #-2 R1
    add R0 R1 R0
    store R0 #-3 SP
    return
