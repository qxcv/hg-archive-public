#include <stdio.h>
#include <ctype.h>

#define MAX_NAME_LENGTH 30

typedef struct Person {
    char name[MAX_NAME_LENGTH + 1];
    float height;
} Person;

/* I ALLOCATE AS MUCH AS I WANT */
Person people[9001];

void person_init(Person *p) {
    p->height = 0;
    p->name[0] = '\0';
}

/* Returns 0 on end, -1 on error or >0 otherwise */
int read_data(Person *dest) {
    char in;
    in = getchar();
    while (isspace(in) && in != EOF) {
        in = getchar();
    }
    if (in == EOF) {
        return 0;
    }
    int name_length = 0;
    while (!isspace(in) && in != EOF) {
        if (name_length < MAX_NAME_LENGTH) {
            dest->name[name_length] = in;
            dest->name[name_length + 1] = '\0';
            name_length++;
        }
        in = getchar();
    }

    if (in == EOF) {
        return -1;
    }
    /* Now read our string in */
    if (scanf("%f", &dest->height) < 1) {
        /* OMG */
        /* what is this I don't even */
        return -1;
    }
    return 1;
}

int main() {
    int i, status, initialised, min, max;
    float height = 0;

    initialised = i = min = max = 0;

    person_init(&people[i]);

    while((status = read_data(&people[i])) > 0) {
        height += people[i].height;
        if (!initialised) {
            initialised = 1;
            min = max = i;
        } else if (people[i].height > people[max].height) {
            max = i;
        } else if (people[i].height < people[min].height) {
            min = i;
        }
        i++;
        person_init(&people[i]);
    }

    if (status == -1) {
        /* There was an error */
        perror("read_data");
        return 1;
    }

    if (!initialised) {
        printf("No data!\n");
        return 1;
    }

    printf("Average height: %f\n", height / i);
    printf("%s had the maximum height, at %f\n", people[max].name, people[max].height);
    printf("%s had the minimum height, at %f\n", people[min].name, people[min].height);

    return 0;
}
