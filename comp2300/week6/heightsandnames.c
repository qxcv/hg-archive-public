#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

#define MAX_NAME_LENGTH 30

typedef struct Person {
    char name[MAX_NAME_LENGTH + 1];
    float height;
    struct Person *next;
} Person;

void person_init(Person *p) {
    p->height = 0;
    p->next = NULL;
    p->name[0] = '\0';
}

Person *new_person() {
    Person *p = malloc(sizeof(Person));
    person_init(p);
    return p;
}

void free_person(Person *p) {
    if (p == NULL) return;
    free_person(p->next);
    free(p);
}

/* Returns 0 on end, -1 on error or >0 otherwise */
int read_data(Person *dest) {
    char in;
    in = getchar();
    while (isspace(in) && in != EOF) {
        in = getchar();
    }
    if (in == EOF) {
        return 0;
    }
    int name_length = 0;
    while (!isspace(in) && in != EOF) {
        if (name_length < MAX_NAME_LENGTH) {
            dest->name[name_length] = in;
            dest->name[name_length + 1] = '\0';
            name_length++;
        }
        in = getchar();
    }
    if (in == EOF) {
        return -1;
    }
    /* Now read our string in */
    if (scanf("%f", &dest->height) < 1) {
        /* OMG */
        /* what is this I don't even */
        return -1;
    }
    return 1;
}

int main() {
    int n, status, initialised;
    float height = 0;
    Person *list = new_person();
    Person *curr = list;
    Person *min, *max;
    min = max = NULL;

    initialised = n = 0;

    while((status = read_data(curr)) > 0) {
        n++;
        height += curr->height;
        if (!initialised) {
            initialised = 1;
            min = max = curr;
        } else if (curr->height > max->height) {
            max = curr;
        } else if (curr->height < min->height) {
            min = curr;
        }
        curr->next = new_person();
        curr = curr->next;
    }

    if (status == -1) {
        /* There was an error */
        perror("read_data");

        free_person(list);

        return 1;
    }

    printf("Average height: %f\n", height / n);
    printf("%s had the maximum height, at %f\n", max->name, max->height);
    printf("%s had the minimum height, at %f\n", min->name, min->height);

    free_person(list);

    return 0;
}
