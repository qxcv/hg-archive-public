; /* Translating the following C code to rPeANUt: */
; int number = 0xA348;
0x100:
    load num R0
; int i = 0;
    load #4 R1
    load #16 R2
    load #10 R7
    load #'0' R3
    load #'A' R4
; while (i < 4) {
    jump compare
body:
;     val = number % 16;
    mod R0 R2 R5 ; R5 holds val
;     if (val < 10) printf("%c", val + '0');
;     else printf("%c" val - 10 + 'A');
    sub R5 R7 R6 ; R7 holds 11
    jumpn R6 if
    sub R5 R7 R5
    add R5 R4 R5
    store R5 0xFFF0
    jump endif
if:
    add R5 R3 R5
    store R5 0xFFF0
;     number = number / 16;
endif:
    div R0 R2 R0
;     i++;
    sub R1 ONE R1
compare:
    jumpnz R1 body
; }
done:
    halt
num: block #0xA348
