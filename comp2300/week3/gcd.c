#include <stdio.h>

unsigned int gcd1(unsigned int a, unsigned int b) {
    if (b == 0) {
        return a;
    }
    return gcd1(b, a % b);
}

unsigned int gcd2(unsigned int a, unsigned int b) {
    while (b != 0) {
        // swap vars
        a ^= b;
        b ^= a;
        a ^= b;
        // do the a-mod-b step (with swapped operands, since we just swapped
        // vars)
        b %= a;
    }
    return a;
}

int main(int argc, char **argv) {
    if (argc != 3) {
        printf("Error: need two arguments\n");
        return 1;
    }
    unsigned int a, b;
    sscanf(argv[1], "%d", &a);
    sscanf(argv[2], "%d", &b);
    printf("gcd1: %d\ngcd2: %d\n", gcd1(a, b), gcd2(a, b));
    return 0;
}
