#include <stdio.h>
#include <math.h>

int main(int argc, char **argv) {
    float a, b, c, s;
    scanf("%f %f %f", &a, &b, &c);
    s = (a + b + c) / 2;
    printf("%f\n", sqrt(s * (s - a) * (s - b) * (s - c)));
    return 0;
}
