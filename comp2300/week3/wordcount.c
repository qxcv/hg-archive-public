/* This program counts the number of words given to it in stdin. A word is
 * considered to be a contiguous block of characters satisfying /[a-zA-Z0-9]/ */

#include <stdio.h>

/* returns 1 if the char is considered to be part of a word, and 0 otherwise */
int char_in_range(char c) {
    /* Tests 0-9, then A-Z, then a-z */
    return (c >= 48 && c <= 57) || (c >= 65 && c <= 90) || (c >= 97 && c <= 122);
}

int main(int argc, char **argv) {
    int total, in_word;
    char in;
    total = 0;
    in_word = 0;
    while ((in = getchar()) != EOF) {
        int in_range = char_in_range(in);
        if ((in_range && !in_word) || (in_word && !in_range)) {
            in_word = in_word ? 0 : 1;
            total += in_word;
        }
    }
    printf("%d\n", total);
    return 0;
}
