#include <stdio.h>
#include <stdint.h>
#include <math.h>

char USAGE[] = "USGAE: %s <floating point value>\n";

// adapted from bpack
uint32_t float_to_uint(float src) {
    int32_t exponent;
    float mantissa = frexpf(fabs(src), &exponent);
    // we want this to be 8 bits
    // we use 126 instead of 127 because frexp() isn't ieee754
    uint32_t pack_exponent = (uint32_t)(exponent + 126) & 0xff;
    // we want this to be 23 bits, excluding leading bit (assumed to be 1)
    uint32_t pack_mantissa = (uint32_t)ldexpf(mantissa, 24) & 0x7fffff;
    uint32_t to_pack = pack_mantissa | (pack_exponent << 23);
    if (src < 0) {
        to_pack |= 1 << 31;
    }
    return to_pack;
}

void printbits(uint32_t num, int numbits) {
    int i;
    for (i = numbits - 1; i >= 0; i--) {
        printf("%d", (num >> i) & 1);
    }
}

int main(int argc, char **argv) {
    uint32_t val, i;
    float in;
    if (argc != 2 || sscanf(argv[1], "%f", &in) < 0) {
        printf(USAGE, argv[0]);
        return 1;
    }
    val = float_to_uint(in);
    printf("sign: ");
    printbits(val >> 31, 1);
    putchar('\n');
    printf("exponent: ");
    printbits(val >> 23, 8);
    putchar('\n');
    printf("significand: ");
    printbits(val, 23);
    putchar('\n');
    return 0;
}
