#include <stdio.h>
#include <math.h>

int main(int argc, char **argv) {
    if (argc != 2 || !freopen(argv[1], "r", stdin)) {
        return 1;
    }
    float total = 0;
    float px, py;
    float cx, cy;
    scanf("%f %f", &px, &py);
    while (scanf("%f %f", &cx, &cy) > 0) {
        total += sqrt((cx - px) * (cx - px) + (cy - py) * (cy - py));
        px = cx;
        py = cy;
    }
    printf("total %f\n", total);
    return 0;
}
