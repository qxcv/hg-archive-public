// circle, rectangle, triangle, square

#include <stdio.h>
#include <math.h>
#include <string.h>
#include <libgen.h>

#define RECT_WIDTH 7
#define RECT_HEIGHT 4

#define SQUARE_SIDE 4

#define TRI_WIDTH  30
#define TRI_HEIGHT 30

#define CIRCLE_RAD 20

#define STAR_WIDTH 13

// height is this many times width
#define HEIGHTMUL 2.2

void repeat(char c, int num) {
    int i;
    for (i = 0; i < num; i++) {
        putchar(c);
    }
}

void capfill(char cap, char fill, int width) {
    putchar(cap);
    repeat(fill, width - 2 >= 0 ? width - 2 : 0);
    if (width > 1) {
        putchar(cap);
    }
    putchar('\n');
}

void draw_rectangle(int width, int height) {
    int i;
    width = (int)(HEIGHTMUL * (float)width);
    capfill('+', '-', width);
    for (i = 0; i < height - 2; i++) {
        capfill('|', ' ', width);
    }
    capfill('+', '-', width);
}

void draw_square(int side) {
    draw_rectangle(side, side);
}

void draw_triangle(int width, int height) {
    int i;
    width = (int)(HEIGHTMUL * (float)width);
    printf("+\n");
    for (i = 0; i < height - 2; i++) {
        // this should, in theory, be correct
        int spaces = i * (width - 2) / (height - 2);
        putchar('|');
        repeat(' ', spaces);
        putchar('\\');
        putchar('\n');
    }
    capfill('+', '-', width);
}

void draw_circle(int radius) {
    // this function is borked and not very elegant due to rounding errors
    int i;
    for (i = 0; i <= 2 * radius; i++) {
        float fwidth = 2 * sqrt(radius * radius - (i - radius) * (i - radius));
        float findent = radius - fwidth / 2;
        int width = (int) (HEIGHTMUL * fwidth);
        int indent = (int) (HEIGHTMUL * findent);
        repeat(' ', indent);
        capfill('*', ' ', width);
    }
}

void star_branch_helper(int i, int width) {
    int split = width / 2 - i;
    int fromleft = width / 2 - split - 1;
    // left
    if (fromleft >= 0) {
        repeat(' ', fromleft);
        putchar('#');
    }
    // centre
    repeat(' ', split);
    putchar('#');
    // right
    if (fromleft >= 0) {
        repeat(' ', split);
        putchar('#');
    }
    putchar('\n');
}

void draw_star(int width) {
    int i;
    // draw down to the crossbar
    for (i = 0; i < width / 2; i++) {
        star_branch_helper(i, width);
    }
    // draw the crossbar
    repeat('#', width);
    putchar('\n');
    // draw down further
    for (i = width / 2 - 1; i >= 0; i--) {
        star_branch_helper(i, width);
    }
}

int main(int argc, char **argv) {
    char *fn = basename(argv[0]);
    if (!strcmp("square", fn)) {
        draw_square(SQUARE_SIDE);
    }
    else if (!strcmp("rectangle", fn)) {
        draw_rectangle(RECT_WIDTH, RECT_HEIGHT);
    }
    else if (!strcmp("triangle", fn)) {
        draw_triangle(TRI_WIDTH, TRI_HEIGHT);
    }
    else if (!strcmp("circle", fn)) {
        draw_circle(CIRCLE_RAD);
    }
    else if (!strcmp("star", fn)) {
        draw_star(STAR_WIDTH);
    }
    else {
        printf("No such shape %s\n", fn);
        return 1;
    }
    return 0;
}
