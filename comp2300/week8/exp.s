0x100:
    load #3 R0
    load #12 R1
    push R0 ; whatever
    call exp
    pop R0 ; return value! Is this 3^12?
    halt

; R0: base
; R1: exponent
; SP-1: return value
exp:
    move ONE R2
    ; because we want this in the loop
    load #2 R7
    jump expcond
expbody:
    mod R1 R7 R3
    jumpnz R3 notdiv2
    mult R0 R0 R0
    div R1 R7 R1
    jump expcond
notdiv2:
    mult R0 R2 R2
    add R1 MONE R1
expcond:
    jumpnz R1 expbody
    store R2 #-1 SP
    return
