0x100:
    load #42 R0
    load #3 R1
    push R0
    push R0
    push R1
    call addrec
    pop R0
    pop R0
    pop R0
    halt

addrec: ; adds two non-negative integers recursively
    ; #0 return address
    ; #-1 x
    ; #-2 y
    ; #-3 return value
    load SP #-1 R0 ; R0 = x
    load SP #-2 R1 ; R1 = y
    jumpz R1 addrecbase
    add MONE R1 R1
    push R0 ; rv
    push R1 ; y
    push R0 ; x
    call addrec
    pop R0 ; pop arg
    pop R0 ; pop arg
    pop R0 ; return value
    add ONE R0 R0
addrecbase:
    store R0 #-3 SP
    return
