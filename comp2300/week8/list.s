0x100:
    load #ll R0
    store ZERO R0
    load #7 R1
    call add
    load #ll R0
    load #5 R1
    call add
    load #ll R0
    load #4 R1
    call add
    load #ll R0
    load #9 R1
    call add
    load #ll R0
    load #2 R1
    call add
    halt
add:
    ; R0 is address, R1 is element
    ; #0 is return address
    ; load size into R2
    load R0 R2
    add R0 R2 R2
    ; To get the actual offset, accounting for our size element
    add R2 ONE R2
    ; store element at base + size
    store R1 R2
    ; now we increment the size
    sub R2 R0 R2
    store R2 R0
    return
ll: block 11 ; increase this number for more elements
