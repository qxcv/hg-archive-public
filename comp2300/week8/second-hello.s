0x100:
    load #hello R0
    push R0
    call puts
    halt

hello: block #"Hello, world!\n"

puts:
    load SP #-1 R1
_putsinner:
    load R1 R2
    jumpz R2 _putsdone
    store R2 0xFFF0
    add R1 ONE R1
    jump _putsinner
_putsdone:
    return
