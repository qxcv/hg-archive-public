0x1: jump iohandler
0x3: jump timehandler

0x100:
  store ONE 0xfff2
  set TI
loop:
  jump loop
  halt

iohandler:
  push R0
  load 0xfff0 R0
  move R0 R7
  store R0 0xfff0
  reset IM
  pop R0
  return

timehandler:
  push R0
  load #'*' R0
  store R0 0xfff0
  reset IM
  pop R0
  return