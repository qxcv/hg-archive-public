0x100:
    ; R1 is reserved, but we don't care about its initial value, since we will
    ; be using R1 as a hash key
    load #1 R1
    load this R7
main:
    ; R0 stores the current cell
    load #0x3C0 R0
loop:
    add MONE R0 R0
    mult R1 R7 R2
    store R2 #0x7C40 R0
    add ONE R1 R1
loopcond:
    jumpnz R0 loop
    jump main

this: block #2012389213