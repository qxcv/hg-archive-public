# Simple echo(1) in x86 assembler
# ...which happens to convert letters to uppercase
# To compile and run execute:
#  %  gcc -c echo.s
#  %  ld -s -o echo echo.o
#  %  ./echo


.data # the data section
input: # label
        # one byte
        .space 1
.text # the code section
.globl _start
_start:
loop:
        movl    $3,%eax # read(0, input, 1)
        movl    $0,%ebx
        movl    $input,%ecx
        movl    $1,%edx
        int     $0x80

        testl %eax,%eax
        jle quit

        movb input,%al
        cmpb $97,%al
        jl continue
        cmpb $122,%al
        jg continue
        subb $32,%al
        mov %al,input

continue:
        # load 4 (code for write) into eax
        movl    $4,%eax    # write(1,input,13)
        # load 1 (stdout) into ebx
        movl    $1,%ebx
        # load the address of the hello string into ecx
        movl    $input,%ecx
        # load 13 (size of hello string) into edx
        movl    $1,%edx
        # run syscall
        int     $0x80
        jmp loop

quit:
        # load 1 (exit code) into eax
        movl    $1,%eax    # exit(0)
        # load 0 into ebx
        movl    $0,%ebx
        # run syscall (and terminate)
        int     $0x80
