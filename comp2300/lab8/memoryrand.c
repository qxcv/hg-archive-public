#include <stdlib.h>
#include<stdio.h>
#include <sys/time.h> 

// Lets given the memory a sequential and random workout.
// Eric McCreath 2012, 2013 GPL
// to compile:
//     gcc -Wall -O3 -o memoryrand memoryrand.c
// to run 
//     ./memoryrand

#define LOOPSIZE 26

static struct timeval starttime;
static struct timeval stoptime;
static struct timezone tzp;

void error(char *str) {
    printf("error: %s\n",str);
    exit(1);
}

void starttimer() {
	if(gettimeofday(&starttime, &tzp) == -1) error("problem getting start time");
}

double stoptimer() {
   if(gettimeofday(&stoptime, &tzp) == -1) error("problem getting stop time");
   double  elapsed_seconds=(double)(stoptime.tv_sec - starttime.tv_sec) + ((double)(stoptime.tv_usec - starttime.tv_usec))/1000000;
   return elapsed_seconds;
}

int main(int argc, char *argv[]) {
      
    int mainloop;
    for (mainloop=1;mainloop<=LOOPSIZE;mainloop++) {
       
        long long int arraysize = (1ll<<mainloop);
        long long int loop = (1ll<<27); // was 30
        
        
      /* printf("%ld\n", sizeof(long long int));  
         printf("arraysize : %lld\n", arraysize);
         printf("loop : %lld\n", loop);*/

        printf("%10.3f ",  4.0*(arraysize/1024.0));  // print the size of the array in KiB

        int *a, *b;
        a = malloc(sizeof(int) * arraysize);
        b = malloc(sizeof(int) * arraysize);
        long long int k, i, p1, p2;
        //set up sequential array
        for (i = 0; i < arraysize-1 ; i++) a[i] = i+1;
        a[arraysize-1] = 0;
        
        // time sequential approach
        starttimer();
	long int s = 0;
        i = 0;	
        for (k=0;k<loop;k++) {
             s += a[i];
             i = a[i];
        }
        double time = stoptimer();
        printf(" %f %ld",time,s);

        // set up random array
        for (i = 0; i < arraysize ; i++) {
            p1 = rand() % arraysize;
            p2 = rand() % arraysize;
            k = a[p1];
            a[p1] = a[p2];
            a[p2] = k;
        }
        for (i = 0; i < arraysize-1 ; i++) {
             b[a[i]] = a[i+1]; 
        }
        b[a[arraysize-1]] = a[0]; 
        
        // time random approach
        starttimer();
	s = 0;
        i = 0;	
        for (k=0;k<loop;k++) {
             s += b[i];
             i = b[i];
        }
        time = stoptimer();
        printf(" %f %ld\n",time,s);
           
        free(a);
        free(b);
    }    
    return 0;
}
