# Simple Hello World in x86 assembler 
# To compile and run execute:
#  %  gcc -c hello.s
#  %  ld -s -o hello hello.o
#  %  ./hello


.data # the data section
hellostr: # label
        # directive to output a literal string in the .data section
        .string "Hello World!\n"
.text # the code section
.globl _start
_start:
loop:
        # load 4 (code for write) into eax
        movl    $4,%eax    # write(1,hellostr,13)
        # load 1 (stdout) into ebx
        movl    $1,%ebx
        # load the address of the hello string into ecx
        movl    $hellostr,%ecx
        # load 13 (size of hello string) into edx
        movl    $13,%edx
        # run syscall
        int     $0x80
        jmp loop

        # load 1 (exit code) into eax
        movl    $1,%eax    # exit(0)
        # load 0 into ebx
        movl    $0,%ebx
        # run syscall (and terminate)
        int     $0x80
