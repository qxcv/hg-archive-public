0x0100:
loop:
	call subvar
	call fill
	call print
	load var R0
	jumpnz R0 loop

subvar:
	load var R0
	sub R0 ONE R0
	store R0 var
	return

fill:
	move R0 R1
	move R1 R2
	move R2 R3
	move R3 R4
	move R4 R5
	move R5 R6
	move R6 R7
	return

print:
	load #0 R0
printloop:
	load R0 #string R1
	jumpz R1 printend
	store R1 0xFFF0
	add R0 ONE R0
	jump printloop
printend:
	return

var:
	block #100

string:
	block #"Hello World"
