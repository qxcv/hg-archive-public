macro
    trapend
    reset IM
    return
mend

0x1:
    jump add_to_buffer

add_to_buffer:
    push R0
    push R1
    push R2
    load bufstart R2
    ; read into buffer + bufstart
    load 0xFFF0 R0
    store R0 #buffer R2
    ; increment bufstart
    add ONE R2 R2
    ; modulo bufstart
    load bufcapacity R1
    mod R2 R1 R2
    load buflength R1
    ; write out bufstart
    store R2 bufstart
    ; increment length, if needed
    add ONE R1 R1
    load bufcapacity R0
    xor R1 R0 R2
    jumpnz R2 AWESOME
    ; move the cacpacity into the length
    move R0 R1
AWESOME:
    store R1 buflength
    pop R2
    pop R1
    pop R0
    trapend

; stores an input character in R0
getc:
    load buflength R0
    jumpz R0 getc ; wait for an interrupt
    ; now we need to load buffer + (bufstart - buflength) mod bufcap
    push R1
    push R2
    push R3
    load buflength R1
    load bufcapacity R2
    load bufstart R0
    ; R3 <- bufstart - buflength
    sub R0 R1 R3
    ; if it's negative, we add the capacity to it to get the memory location
    ; which we have to read from
    jumpn R3 isneg
        jump isnotneg
    isneg:
        add R2 R3 R3
    isnotneg:
    load R3 #buffer R0 ; DONE!
    add MONE R1 R1
    store R1 buflength
    pop R3
    pop R2
    pop R1
    return

0x100:
    store ONE 0xFFF2
loop:
    call getc ; read a character into R0
    store R0 0xFFF0
    jump loop
    halt

; circular buffer JUST FOR LAUGHS
bufstart: block #0
buflength: block #0
bufcapacity: block #256
buffer: block 256
