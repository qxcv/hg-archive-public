macro
putc &c &r
load &c &r
store &r 0xFFF0
mend

0x0002:
    jump printtrap

printtrap:
    push R0
    putc #'t' R0
    putc #'r' R0
    putc #'a' R0
    putc #'p' R0
    putc #10 R0
    pop R0
    reset IM
    return

0x100:
    trap
    jump 0x100
