macro
    endsyscall
    reset IM
    return
mend

    ; Task #2:
0x0002:
    push R2
    load #syscalls R2
    add R0 R2 R0
    pop R2
    move R0 PC

syscalls:
    jump read
    jump write

read:
    load 0xFFF1 R1
    jumpz R1 read
    load 0xFFF0 R1
    endsyscall

write:
    store R1 0xFFF0
    endsyscall

0x100:
    ; Task #3:
    ; perform a read (#0)
    move ZERO R0
    trap
    ; perform a write of the character (#1)
    move ONE R0
    trap
    jump 0x100 ; repeat for echo
