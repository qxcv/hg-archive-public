#include <stdio.h>
#include <stdbool.h>

#define E_WIDTH 50
#define E_HEIGHT 100

int main(int argc, char **argv) {
    int i, j;
    for (i = 0; i < E_HEIGHT; i++) {
        bool bar = i == 0 || i == E_HEIGHT - 1 || i == E_HEIGHT / 2;
        for (j = 0; j < (bar ? E_WIDTH : 1); j++) {
            printf("*");
        }
        printf("\n");
    }
    return 0;
}
