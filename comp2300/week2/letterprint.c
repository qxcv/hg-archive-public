#include <stdio.h>

/* IT PRINTS LETTERS HAHAHAHAHA */

typedef enum Action {
    Crossbar,
    LeftDot,
    LeftRightDot
} Action;

typedef Action *Letter;

Action H[7] = {
    LeftRightDot,
    LeftRightDot,
    LeftRightDot,
    Crossbar,
    LeftRightDot,
    LeftRightDot,
    LeftRightDot
};

Action E[7] = {
    Crossbar,
    LeftDot,
    LeftDot,
    Crossbar,
    LeftDot,
    LeftDot,
    Crossbar
};

Action L[7] = {
    LeftDot,
    LeftDot,
    LeftDot,
    LeftDot,
    LeftDot,
    LeftDot,
    Crossbar
};

Action O[7] = {
    Crossbar,
    LeftRightDot,
    LeftRightDot,
    LeftRightDot,
    LeftRightDot,
    LeftRightDot,
    Crossbar
};

void print_letter(Action *letter) {
    int i;
    int j;
    for (i = 0; i < 7; i++) {
        switch (letter[i]) {
            case LeftDot:
                printf("#\n");
                break;
            case Crossbar:
                for (j = 0; j < 5; j++) {
                    printf("#");
                }
                printf("\n");
                break;
            case LeftRightDot:
                printf("#"); printf("   #\n");
                break;
        }
    }
    printf("\n\n");
}

int main() {
    Letter letters[] = {H, E, L, L, O, NULL};
    Letter *bptr = &letters[0];
    while (*bptr != NULL) {
        print_letter(*bptr++);
    }
    return 0;
}
