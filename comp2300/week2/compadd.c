/* Takes two 32 bit two's complement integers and prints the result. Note that
 * this program takes its input in text, but using binary digits (so {1,
 * 0}^32). */

#include <stdio.h>

int toint(char x) {
    return x == '1' ? 1 : 0;
}

char fromint(int x) {
    return x == 0 ? '0' : '1';
}

int validate(char *s) {
    int i;
    for (i = 0; i < 32; i++) {
        if (!s[i]) return 0;
        if (s[i] != '0' && s[i] != '1') return 0;
    }
    if (s[32]) return 0;
    return 1;
}

int main(int argc, char **argv) {
    char out[32];
    int i, carry;
    carry = 0;
    if (argc != 3) {
        printf("Need two base-2, 32 digit numbers as arguments.\n");
        return 1;
    }
    char *a = argv[1];
    char *b = argv[2];
    if (!validate(a) || !validate(b)) {
        printf("One or more of your numbers are invalid. Are they both 32 characters long and comprised only of 0s and 1s?\n");
        return 1;
    }
    for (i = 31; i >= 0; i--) {
        int opa = toint(a[i]);
        int opb = toint(b[i]);
        out[i] = fromint((opa + opb + carry) % 2);
        if (opa + opb + carry > 1) {
            carry = 1;
        }
        else {
            carry = 0;
        }
    }
    for (i = 0; i < 32; i++) {
        printf("%c", out[i]);
    }
    printf("\n");
    return 0;
}
