int main() {
    /* Answering the question: we would have to use printf(3) and multiply two
     * floats together (3.0 * 5.0) instead. */
    return 3 * 5;
}
