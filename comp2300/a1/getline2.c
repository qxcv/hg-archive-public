#include <stdio.h>

#include "getline2.h"
#include "debug.h"
#include "bstring.h"

/* See getline2.h for documentation */

ssize_t getline2(char **lptr, FILE *fp) {
    BString accumulator;
    if (!bstring_init(&accumulator)) {
        DEBUG_PRINTF("Failed to initialise bstring in getline2\n");
    }

    int character_read = getc(fp);
    /* Only return -1 if we get EOF right away */
    if (character_read == EOF) {
        return -1;
    }

    while (character_read != EOF && character_read != '\n') {
        if (!bstring_append(&accumulator, (unsigned char)character_read)) {
            bstring_free(&accumulator);
            return -1;
        }

        character_read = getc(fp);
    }

    /* Always append a trailing newline, regardless of whether the real last
     * character was \n or EOF */
    if (!bstring_append(&accumulator, '\n')) {
        bstring_free(&accumulator);
        return -1;
    }

    *lptr = accumulator.data;

    return (ssize_t)accumulator.length;
}
