/* This code based on minunit, but I have added some extensions which only work
 * with GCC. */

/* Originally adapted from http://www.jera.com/techinfo/jtns/jtn002.html, which
 * included the note:
 *
 * "You may use the code in this tech note for any purpose, with the
 *  understanding that it comes with NO WARRANTY."
 *
 * Having said that, I think this file has been around for so long (and is so
 * small) that it probably no longer shares any code with minunit.
 */

#include <stdio.h>

int _tests_run = 0;
int _tests_failed = 0;
#define TEST_ASSERT(T) ({_tests_run++;\
        !(T) ? ({printf("Test #%i \x1b[31mfailed\x1b[0m: '%s'\n", _tests_run, #T); _tests_failed++; 0;})\
        : ({printf("Test #%i \x1b[32mpassed\x1b[0m: '%s'\n", _tests_run, #T); 1;});})

#define DID_SOME_TESTS_FAIL (!!_tests_failed)
