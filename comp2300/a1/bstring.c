#include <stdlib.h>
#include <stdio.h>

#include "bstring.h"

/* Dynamic string implementation. See bstring.h for details */

int bstring_append(BString *str, char data) {
    str->length++;

    if (str->capacity <= str->length) {
        unsigned int newcap = 2 * str->capacity;

        char *newdata = realloc(str->data, sizeof(char) * newcap);
        if (newdata == NULL) {
            /* allocation error */
            perror("realloc");
            return 0;
        }

        str->data = newdata;
        str->capacity = newcap;
    }

    str->data[str->length - 1] = data;
    str->data[str->length] = '\0';

    return 1;
}

int bstring_init(BString *str) {
    str->data = malloc(sizeof(char) * BSTRING_INITIAL_CAPACITY);
    if (str->data == NULL) {
        /* allocation failure */
        perror("malloc");
        return 0;
    }

    str->data[0] = '\0';
    str->capacity = BSTRING_INITIAL_CAPACITY;
    str->length = 0;

    return 1;
}

void bstring_free(BString *str) {
    /* Prevent double-free and free-before-init */
    if (str->data != NULL) {
        free(str->data);
        str->data = NULL;
    }
    str->length = str->capacity = 0;
}

int bstring_cstring_eq(BString *a, char *b) {
    unsigned int i = 0;
    for (i = 0; i < a->length; i++) {
        if (b[i] == '\0') {
            return 0;
        }
        if (b[i] != a->data[i]) {
            return 0;
        }
    }

    /* If there are still more characters of b */
    if (b[i] != '\0') {
        return 0;
    }

    /* Otherwise, they're equal! */
    return 1;
}

char *duplicate_bstring_to_cstring(BString *a) {
    char *rv = malloc(sizeof(a->data[0]) * (a->length + 1));
    if (rv == NULL) {
        perror("malloc");
        return NULL;
    }

    unsigned int i;
    for (i = 0; i < a->length; i++) {
        rv[i] = a->data[i];
    }
    rv[a->length] = '\0';

    return rv;
}
