#ifndef _BSTRING_H_GUARD
#define _BSTRING_H_GUARD

#include "debug.h"

/* My very small, very basic dynamic string library.
 *
 * BString is short for "bounded string", but we using BString since it is more
 * concise */

/* How big do we want our strings to be initially? */
#define BSTRING_INITIAL_CAPACITY 8

/* Data stored by a BString */
typedef struct BString {
    /* Our heap-allocated storage */
    char *data;
    /* The number of chars currently stored (sans trailing nul) */
    unsigned int length;
    /* The capacity of the array (including trailing nul) */
    unsigned int capacity;
} BString;

/* Append a char to an initialised BString. Returns 0 on failure or non-zero on
 * success */
int WARN_UNUSED bstring_append(BString *str, char data);

/* Initialises a BString. This must be called before using any of the other
 * BString functions. Returns 0 on failure and non-zero otherwise. */
int WARN_UNUSED bstring_init(BString *str);

/* Frees a BString. This function does not return a value. */
void bstring_free(BString *str);

/* Compares a BString and a C string and returns 1 if they are equal, or 0
 * otherwise. */
int bstring_cstring_eq(BString *a, char *b);

/* Convert a BString to a plain C string which can be freed with free(). Return
 * NULL on failure or the freshly allocated C string on success. */
char* WARN_UNUSED duplicate_bstring_to_cstring(BString *a);

#endif // _BOUNDED_STRING_H_GUARD
