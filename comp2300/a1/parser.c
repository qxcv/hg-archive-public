/* Parsing code. Please see parser.h for documentation */

#include <stdlib.h>
#include <ctype.h>

#include "parser.h"
#include "debug.h"

void tokenizer_state_init(TokenizerState *s) {
    s->in_escape = s->in_string = 0;
}

int read_token(TokenizerState *s, BString *token, char *buf, int size) {
    /* Defensive programming */
    if (s == NULL || token == NULL) {
        DEBUG_PRINTF("read_token called with NULL tokenizer state or token bstring\n");
        return -1;
    }

    int i = 0;
    for (; i < size; i++) {
        /* We *always* ignore \0. This seems to be the behaviour of other
         * shells, and with good reason: \0 will terminate an argv string! */
        if (buf[i] == '\0') {
            continue;
        }

        /* Go over the remainder of the buffer one character at a time. */
        if (buf[i] == '\\' && !s->in_escape) {
            s->in_escape = 1;
            continue;
        }

        if ((buf[i] == '"' || buf[i] == '\'') && !s->in_escape) {
            /* Real shells actually let us have quotes *anywhere* within a token
             * without delimiting that token. We mimic this behaviour. */
            if (s->in_string && buf[i] == s->string_type) {
                s->in_string = 0;
                continue;
            }
            else if (!s->in_string) {
                s->in_string = 1;
                s->string_type = buf[i];
                continue;
            }
        }

        if (!s->in_escape && !s->in_string) {
            /* Check for terminals (spaces, |, < and >) */
            if (isspace(buf[i]) || buf[i] == '|' || buf[i] == '<' || buf[i] == '>') {
                /* We've got a terminal */
                break;
            }
        }

        /* Don't append newlines if they're being escaped */
        if (!(s->in_escape && buf[i] == '\n')) {
            if (!bstring_append(token, buf[i])) {
                /* Append failed. */
                DEBUG_PRINTF("bstring_append failed in read_token\n");
                return -1;
            }
        }

        if (s->in_escape) {
            s->in_escape = 0;
        }
    }

    if (s->in_escape || s->in_string) {
        /* hmmm, we need more data */
        return size + 1;
    }

    return i;
}

void parser_state_init(ParserState *s) {
    /* Start nowhere */
    s->in_token = 0;
    /* Signal that there was no preceding operator for this token. */
    s->block_starter = '\0';
    tokenizer_state_init(&s->tok_state);
    s->token_destination = NULL;
    s->current_command = NULL;
}

BStringLL* WARN_UNUSED bstringll_create() {
    /* If we have no list, make one */
    BStringLL *rv = malloc(sizeof(BStringLL));
    if (rv == NULL) {
        DEBUG_PRINTF("malloc failed in bstringll_create");
        perror("malloc");
        return NULL;
    }
    rv->next = NULL;
    if (!bstring_init(&rv->current)) {
        DEBUG_PRINTF("bstring_init failed in bstringll_create");
        free(rv);
        return NULL;
    }
    return rv;
}

void bstringll_free(BStringLL *l) {
    while (l != NULL) {
        BStringLL *next = l->next;
        bstring_free(&l->current);
        free(l);
        l = next;
    }
}

/* This function definition is monstrous because of the huge number of special
 * cases hiding in the parser logic. */
int parse_buffer(ParserState *s, Command *c, char *buf, int num) {
    /* Defensive programming */
    if (s == NULL || c == NULL) {
        DEBUG_PRINTF("c or s were NULL in parse_buffer\n");
        return -1;
    }

    if (s->current_command == NULL) {
        s->current_command = c;
    }

    int i;
    for (i = 0; i < num; i++) {
        if (s->in_token) {
            int left_to_read = num - i;
            int rv = read_token(&s->tok_state, s->token_destination, &buf[i], left_to_read);

            if (rv < 0) {
                /* Oh dear, read_token failed. */
                DEBUG_PRINTF("read_token failed in parse_buffer\n");
                return rv;
            } else if (rv >= 0 && rv < left_to_read) {
                /* If it returned a complete token without reaching the end of
                 * the buffer: */
                s->in_token = 0;
                i += rv;
                /* Rewind to look at current token again */
                i--;
                continue;
            } else if (rv > left_to_read) {
                /* Incomplete data */
                return 0;
            } else {
                /* We reached the end of the buffer without error or incomplete
                 * data. */
                /* Return num + 1 to indicate that we can read more data. */
                return num + 1;
            }
            /* Execution should not reach here */
            DEBUG_PRINTF("execution reached end token-processing if block in parse_buffer\n");
            return -1;
        }

        /* Otherwise, if this is non-delimiting whitespace or ASCII nul, we can skip the
         * character. */
        if ((isspace(buf[i]) && buf[i] != '\n') || buf[i] == '\0') {
            continue;
        }

        /* Just ignore escaped newlines completely */
        if (buf[i] == '\\' && i < num - 1 && buf[i + 1] == '\n') {
            i +=  2;
            continue;
        }

        /* We have a non-space (but possibly newline), non-nul character and are not in a token.
         * */
        if (s->block_starter == '<' || s->block_starter == '>') {
            if ((s->block_starter == '<' && s->current_command->infile.length == 0)
             || (s->block_starter == '>' && s->current_command->outfile.length == 0)) {
                /* If we don't have a filename, expect a token */
                if (buf[i] == '<' || buf[i] == '>' || buf[i] == '\n' || buf[i] == '|') {
                    DEBUG_PRINTF("parse_buffer expected filename, got token terminator\n");
                    return -1;
                }

                tokenizer_state_init(&s->tok_state);
                s->in_token = 1;

                if (s->block_starter == '<') {
                    s->token_destination = &s->current_command->infile;
                } else {
                    s->token_destination = &s->current_command->outfile;
                }

                /* Rewind i so that we can re-parse this character */
                i--;
                continue;
            }

            /* If we have a token, expect end-of-command, pipe or opposite
             * redirect (assuming that it has not previously been specified): */
            if ((buf[i] == '<' && (s->current_command->infile.length > 0 || s->block_starter == '<')) ||
                (buf[i] == '>' && (s->current_command->outfile.length > 0 || s->block_starter == '>'))) {
                /* Double-specified output */
                DEBUG_PRINTF("parse_buffer detected double-specification of input or output\n");
                return -1;
            }

            if (buf[i] == '>' || buf[i] == '<') {
                /* Now we specify output (no double-specification yet) */
                s->block_starter = buf[i];
                continue;
            }

            if (buf[i] == '|') {
                /* New block, with a pipe */
                s->block_starter = buf[i];
                s->current_command->pipe_to = command_list_create();
                if (s->current_command->pipe_to == NULL) {
                    DEBUG_PRINTF("command_list_create failed in parse_buffer\n");
                    return -1;
                }
                s->current_command = s->current_command->pipe_to;
                continue;
            }

            if (buf[i] == '\n') {
                /* End of command */

                /* Say we've processed this byte */
                i++;
                return i;
            }

            /* Code shouldn't reach here */
            DEBUG_PRINTF("parse_buffer reached end of handler for < and > blocks\n");
            return -1;
        } else {
            /* Either the block starter was a pipe or didn't exist (it is
             * the beginning of a line */

            if (s->current_command->argv_list != NULL && s->current_command->argv_list->current.length > 0) {
                /* If we have a token, expect anything. */

                if (buf[i] == '\n') {
                    /* We're finished reading */
                    i++;
                    break;
                }

                if (buf[i] == '|' ||  buf[i] == '>' || buf[i] == '<') {
                    /* Start a new pipe block or file input block */
                    s->block_starter = buf[i];

                    if (buf[i] == '|') {
                        /* New piped command */
                        s->current_command->pipe_to = command_list_create();
                        if (s->current_command->pipe_to == NULL) {
                            DEBUG_PRINTF("command_list_create failed in parse_buffer\n");
                            return -1;
                        }
                        s->current_command = s->current_command->pipe_to;
                    } else if (buf[i] == '<') {
                        /* New input file */
                        s->token_destination = &s->current_command->infile;
                    } else {
                        /* New output file */
                        s->token_destination = &s->current_command->outfile;
                    }

                    continue;
                }
            } else {
                /* If we don't have a token, expect a token (unless we have a
                 * newline and didn't start with a pipe, in which case we just
                 * return an empty command list). */
                if (buf[i] == '\n' && s->block_starter != '|') {
                    DEBUG_PRINTF("parse_buffer was passed an empty line\n");
                    /* Increment to indicate that we have looked at this byte: */
                    i++;
                    break;
                }

                if (buf[i] == '\n' || buf[i] == '<' || buf[i] == '>' || buf[i] == '|') {
                    /* Hmm, looks like this isn't a token. */
                    DEBUG_PRINTF("parse_buffer expected a token but got a token terminator");
                    return -1;
                }
            }

            /* At this point, we may or may not have a token, but we know that
             * we *can* parse a new token. */

            /* Parse a token */
            if (s->current_command->argv_list == NULL) {
                s->current_command->argv_list = bstringll_create();

                if (s->current_command->argv_list == NULL) {
                    DEBUG_PRINTF("error running bstringll_create in parse_buffer");
                    return -1;
                }

                s->current_command->argv_list_end = s->current_command->argv_list;
            } else {
                /* Otherwise, we append to the existing argv list */
                s->current_command->argv_list_end->next = bstringll_create();

                if (s->current_command->argv_list_end->next == NULL) {
                    DEBUG_PRINTF("error running bstringll_create in parse_buffer");
                    return -1;
                }

                s->current_command->argv_list_end = s->current_command->argv_list_end->next;
            }

            tokenizer_state_init(&s->tok_state);
            s->in_token = 1;
            s->token_destination = &s->current_command->argv_list_end->current;

            /* Let the tokenizer re-examine the current character */
            i--;

            continue;
        }
    }

    if (s->current_command->argv_list == NULL
    || (s->block_starter == '<' && s->current_command->infile.length == 0)
    || (s->block_starter == '>' && s->current_command->outfile.length == 0)) {
        /* Make sure that if we were expecting a token, we get it. */
        return 0;
    }

    return i;
}

Command* command_list_create() {
    Command *rv = malloc(sizeof(Command));
    if (rv == NULL) {
        perror("malloc");
        return NULL;
    }
    if (!bstring_init(&rv->infile) || !bstring_init(&rv->outfile)) {
        /* bstring_init failed */
        if (rv->infile.data != NULL) {
            bstring_free(&rv->infile);
        }
        return NULL;
    }
    rv->argv_list = NULL;
    rv->pipe_to = NULL;
    rv->pid = -1;
    return rv;
}

void command_list_free(Command *c) {
    while (c != NULL) {
        /* Free infile */
        bstring_free(&c->infile);

        /* Free outfile */
        bstring_free(&c->outfile);

        /* Free argv list */
        bstringll_free(c->argv_list);
        c->argv_list = NULL;

        /* Advance to the next command */
        Command *backup = c;
        c = c->pipe_to;
        free(backup);
    }
}

char **bstringll_to_argv_list(BStringLL *ll) {
    int allocated_length = 8;
    int num_elements = 0;

    char **rv = malloc(sizeof(char*) * allocated_length);

    if (rv == NULL) {
        perror("malloc");
        DEBUG_PRINTF("malloc failed in bstringll_to_argv_list\n");

        return NULL;
    }

    rv[0] = NULL;

    while (ll != NULL) {
        char *new_string = duplicate_bstring_to_cstring(&ll->current);

        if (new_string == NULL) {
            DEBUG_PRINTF("duplicate_bstring_to_cstring failed in bstringll_to_argv_list\n");

            free_argv_list(rv);

            return NULL;
        }

        if (num_elements > allocated_length - 2) {
            allocated_length *= 2;

            char **new_rv = realloc(rv, sizeof(char*) * allocated_length);

            if (new_rv == NULL) {
                perror("realloc");
                DEBUG_PRINTF("realloc failed in bstringll_to_argv_list\n");

                free_argv_list(rv);

                return NULL;
            }

            rv = new_rv;
        }

        rv[num_elements] = new_string;
        num_elements++;
        rv[num_elements] = NULL;

        ll = ll->next;
    }

    return rv;
}

void free_argv_list(char **argv) {
    if (argv == NULL) {
        return;
    }

    char **walker = argv;
    while (*walker != NULL) {
        free(*walker);
        walker++;
    }

    free(argv);
}

