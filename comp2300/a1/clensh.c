/* ISO includes */
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* POSIX includes */
#include <errno.h>
#include <sys/wait.h>
#include <unistd.h>

/* Third party includes */
#include <readline/history.h>
#include <readline/readline.h>

/* Project includes */
#include "bstring.h"
#include "parser.h"
#include "spawn.h"
#include "getline2.h"

/* Have we received a SIGINT? */
/* See
 * https://www.securecoding.cert.org/confluence/display/seccode/SIG31-C.+Do+not+access+shared+objects+in+signal+handlers
 * for information on why we use sig_atomic_t and volatile here. */
volatile sig_atomic_t sigint_received = 0;

/* Are we currently in a readline() call? */
volatile sig_atomic_t in_readline = 0;

/* Stores the "proper" top-level readline prompt */
char *toplevel_readline_prompt = NULL;

/* Our SIGINT handler */
void sigint_handler(int signal) {
    if (in_readline) {
        /* Unfortunately, we *have* to put this in here because readline does
         * not a gives us a method for overriding internal signal handlers.
         * However, mutating shared state in a signal handler like this is
         * generally a bad idea. */
        if (toplevel_readline_prompt != NULL) {
            rl_set_prompt(toplevel_readline_prompt);
        }
        /* We don't check return values here because we have nothing to do with
         * them */
        rl_delete_text(0, rl_end);
        rl_point = 0;
        rl_end = 0;
        rl_reset_line_state();
        rl_crlf();
        rl_forced_update_display();
    }
    sigint_received = 1;
}

/* Makes a prompt from the cwd and stores it in the supplied BString. Returns 0
 * on error or nonzero on success */
int make_prompt(BString *s) {
    if (s == NULL) { /* Defensive check */
        DEBUG_PRINTF("make_prompt passed NULL\n");
        return 0;
    }

    char *cwd = getcwd(NULL, 0);
    if (cwd == NULL) {
        perror("cwd");
        return 0;
    }

    /* Copy in the cwd */
    char *cwd_walker = cwd;
    while (*cwd_walker != 0) {
        if (!bstring_append(s, *cwd_walker)) {
            /* Failed */
            free(cwd);
            return 0;
        }
        cwd_walker++;
    }
    free(cwd);

    if (!bstring_append(s, ' ')
     || !bstring_append(s, '$')
     || !bstring_append(s, ' ')) {
        return 0;
    }

    return 1;
}
/* Reads a command. Returns -1 on EOF or failure, 0 on parse error or 1 on success */
int read_command(Command **command_pptr) {
    static int is_interactive = -1;
    if (is_interactive == -1) {
        is_interactive = isatty(STDIN_FILENO);
    }

    Command *rv = NULL;
    ParserState ps;
    parser_state_init(&ps);
    BString prompt;

    if (is_interactive) {
        /* Build our prompt string */
        if (!bstring_init(&prompt)) {
            DEBUG_PRINTF("bstring_init failed in read_command\n");
            return -1;
        }

        if (!make_prompt(&prompt)) {
            DEBUG_PRINTF("make_prompt failed in read_command\n");
            bstring_free(&prompt);
            return -1;
        }

        /* For redisplay on Ctrl + C */
        toplevel_readline_prompt = prompt.data;
    }

    /* We will parse this later */
    char *input_line = NULL;
    int buf_length = 0;

    /* Do we still require input? */
    int input_required = 1;
    /* Are we on our second input run? */
    int second_run = 0;

    while (input_required) {
        if (is_interactive) {
            if (!second_run) {
                in_readline = 1;
                input_line = readline(prompt.data);
                in_readline = 0;
            } else {
                sigint_received = 0;

                in_readline = 1;
                input_line = readline("> ");
                in_readline = 0;

                if (sigint_received) {
                    /* Got a sigint while reading, so we switched back over the
                     * the normal toplevel prompt */
                    if (rv != NULL) {
                        command_list_free(rv);
                        rv = NULL;
                    }

                    /* Pretend that this is our first run */
                    parser_state_init(&ps);
                    second_run = 0;
                }
            }

            if (input_line == NULL) {
                /* EOF or error */
                if (rv != NULL) {
                    command_list_free(rv);
                }
                bstring_free(&prompt);
                toplevel_readline_prompt = NULL;
                return -1;
            }

            buf_length = strlen(input_line);

            if (buf_length > 0) {
                add_history(input_line);
            }

            /* Readline doesn't return a newline, so we have to put it in ourselves
             * ;_; */
            buf_length++;
            char *new_input_line = realloc(input_line, sizeof(char) * (buf_length + 2));
            if (new_input_line == NULL) {
                /* We couldn't realloc */
                perror("realloc");
                free(input_line);
                bstring_free(&prompt);
                toplevel_readline_prompt = NULL;
                if (rv != NULL) {
                    command_list_free(rv);
                }
                rv = NULL;
                return -1;
            }

            new_input_line[buf_length] = '\0';
            new_input_line[buf_length - 1] = '\n';
            input_line = new_input_line;
        } else {
            buf_length = getline2(&input_line, stdin);
            if (buf_length < 0) {
                /* EOF or error */
                if (input_line != NULL) {
                    free(input_line);
                }
                return -1;
            }
        }

        if (!second_run) {
            int blank_line = 1;
            int i;
            for (i = 0; i < buf_length; i++) {
                if (!isspace(input_line[i])) {
                    blank_line = 0;
                    break;
                }
            }

            if (blank_line) {
                input_required = 1;
                free(input_line);
                continue;
            }
        }

        if (rv == NULL) {
            rv = command_list_create();
            if (rv == NULL) {
                /* Oops */
                DEBUG_PRINTF("command_list_create failed in read_command\n");
                free(input_line);
                if (is_interactive) {
                    bstring_free(&prompt);
                    toplevel_readline_prompt = NULL;
                }
                return -1;
            }
        }

        int pv_status = parse_buffer(&ps, rv, input_line, buf_length);
        free(input_line);
        if (pv_status < 0 || rv == NULL) {
            /* parse_buffer failed or found an empty line */
            DEBUG_PRINTF("parse_bufer failed or got an empty line\n");
            if (rv != NULL) {
                command_list_free(rv);
            }
            if (is_interactive) {
                bstring_free(&prompt);
                toplevel_readline_prompt = NULL;
            }
            return 0;
        }

        if (pv_status == 0
        || (pv_status > buf_length)) {
            /* need more data */
            input_required = 1;
            second_run = 1;
        } else {
            input_required = 0;
        }
    }

    if (is_interactive) {
        bstring_free(&prompt);
        toplevel_readline_prompt = NULL;
    }

    *command_pptr = rv;
    return 1;
}

/* Changes directory of the program */
void builtin_cd(char **argv_list) {
    if (argv_list[1] == NULL) {
        /* Switch to home */
        char *dir = getenv("HOME");
        if (dir == NULL) {
            fprintf(stderr, "%s: could not find $HOME, path required.\n", argv_list[0]);
        } else {
            if (chdir(dir)) {
                fprintf(stderr, "cd: ");
                perror(dir);
            }
        }
    } else {
        if (chdir(argv_list[1])) {
            fprintf(stderr, "cd: ");
            perror(argv_list[1]);
        }
    }
}

/* Loop over the supplied command list and spawn each command. Returns 0 on
 * success or -1 on failure. If loop fails, it stores the return value in *rv. */
int spawn_loop(Command *command_list_head, int *rv) {
    *rv = 0;

    /* Are we in a sequence of pipes? If so, we ignore builtins like cd and
     * exit. This appears to be the behaviour of bash. */
    int pipe_sequence = command_list_head->pipe_to != NULL;

    /* Did we exit the spawn loop because the sequence failed? */
    int spawn_sequence_failed = 0;

    /* Do we need to close the current pipe (if it exists) and create a new
     * one (with a closed write end) so that we can spawn the next command?
     * */
    int dummy_pipe_required = 0;

    /* Pointer used for walking through the command list */
    Command *c = command_list_head;

    /* Pipe which we use to string processes together */
    int current_pipe[2] = {-1, -1};

    while (c != NULL) {
        if (dummy_pipe_required) {
            if (current_pipe[0] != -1) {
                if (-1 == close(current_pipe[0])) {
                    DEBUG_PRINTF("close failed in dummy pipe cleanup code for command %s\n",
                            c->argv_list->current.data);
                    perror("close");
                }

                current_pipe[0] = -1;
            }

            /* Now we re-create the current pipe for the sake of this
             * process currently being spawned. */
            if (-1 == pipe(current_pipe)) {
                perror("pipe");
                spawn_sequence_failed = 1;
                break;
            }

            if (-1 == close(current_pipe[1])) {
                DEBUG_PRINTF("close failed in dummy pipe creation code\n");
                perror("close");
            }

            /* So that spawn_command does not try to close it*/
            current_pipe[1] = -1;

            dummy_pipe_required = 0;
        }

        if (c->argv_list == NULL || c->argv_list->current.length == 0) {
            /* Oh dear. We have an empty command. */
            DEBUG_PRINTF("parse_buffer returned an empty command\n");
            dummy_pipe_required = 1;
            c = c->pipe_to;
            continue;
        }

        if (sigint_received) {
            DEBUG_PRINTF("Stopping spawn loop because shell received SIGINT\n");
            *rv = 1;
            dummy_pipe_required = 1;
            spawn_sequence_failed = 1;
            break;
        }

        char **argv_list = bstringll_to_argv_list(c->argv_list);
        if (argv_list == NULL) {
            DEBUG_PRINTF("bstringll_to_argv_list failed in main\n");

            *rv = 1;
            dummy_pipe_required = 1;
            spawn_sequence_failed = 1;
            break;
        }

        /* Test if the given command was a builtin */
        if (!strcmp(argv_list[0], "exit")) {
            /* Get a return code, if one exists */
            if (!pipe_sequence && argv_list[1] != NULL) {
                if (sscanf(argv_list[1], "%d", rv) < 1) {
                    /* If we couldn't get an integer out of the argument*/
                    fprintf(stderr, "%s: Integer argument required\n", argv_list[0]);
                    *rv = 255;
                }
            }
            free_argv_list(argv_list);
            argv_list = NULL;

            dummy_pipe_required = 1;

            if (pipe_sequence) {
                c = c->pipe_to;
                continue;
            }

            spawn_sequence_failed = 1;
            break;
        }

        if (!strcmp(argv_list[0], "cd")) {
            if (!pipe_sequence) {
                builtin_cd(argv_list);
            }

            free_argv_list(argv_list);
            argv_list = NULL;

            dummy_pipe_required = 1;
            c = c->pipe_to;
            continue;
        }

        int new_output_pipe[2] = {-1, -1};
        if (c->pipe_to != NULL) {
            /* We need to create a new pipe to feed output into */
            if (-1 == pipe(new_output_pipe)) {
                /* O noes! We couldn't make the pipe :-( */
                perror("pipe");
                spawn_sequence_failed = 1;
                free_argv_list(argv_list);
                argv_list = NULL;
                break;
            }
        }

        /* Now we spawn the program itself */
        c->pid = spawn_command(argv_list,
                c->infile.length > 0 ? c->infile.data : NULL,
                c->outfile.length > 0 ? c->outfile.data : NULL,
                /* input pipe */
                current_pipe[0] != -1 ? current_pipe : NULL,
                /* output pipe */
                new_output_pipe[1] != -1 ? new_output_pipe : NULL
        );

        if (current_pipe[0] != -1) {
            if (-1 == close(current_pipe[0])) {
                DEBUG_PRINTF("failed to close in_fd after spawn code in main\n");
                perror("close");
            }
            current_pipe[0] = -1;
        }

        if (new_output_pipe[1] != -1) {
            if (-1 == close(new_output_pipe[1])) {
                DEBUG_PRINTF("failed to close out_fd after spawn code in main\n");
                perror("close");
            }
            new_output_pipe[1] = -1;
        }

        current_pipe[0] = new_output_pipe[0];
        current_pipe[1] = new_output_pipe[1];

        free_argv_list(argv_list);
        argv_list = NULL;

        c = c->pipe_to;
    }

    if (dummy_pipe_required && current_pipe[0] != -1) {
        if (close(current_pipe[0]) == -1) {
            DEBUG_PRINTF("Could not close current pipe in post-spawn-loop dummy pipe "
                    "cleanup code\n");
            perror("close");
        }
        current_pipe[0] = -1;
    }

    return spawn_sequence_failed ? -1 : 0;
}

int main(int argc, char **argv) {
    /* return value */
    int rv = 0;

    if (argc != 1) {
        fprintf(stderr, "USAGE: %s\n", argv[0]);
        return 1;
    }

    /* Set the PATH variable for the sake of execvp */
    if (setenv("PATH", ".:/usr/bin:/bin", 1) != 0) {
        perror("setenv");
        return 1;
    }

    int is_interactive = isatty(STDIN_FILENO);

    if (is_interactive) {
        /* Try to set our sigint handler */
        if (SIG_ERR == signal(SIGINT, sigint_handler)) {
            DEBUG_PRINTF("Couldn't set SIGINT handler---SIGINT will now terminate "
                    "shell as well as its children\n");
            perror("sigint");
        }
    }

    while (1) {
        Command *command_list_head = NULL;

        int rc_status = read_command(&command_list_head);
        if (rc_status < 0) {
            /* EOF */
            if (command_list_head != NULL) {
                command_list_free(command_list_head);
            }
            break;
        }

        if (rc_status == 0 || command_list_head == NULL) {
            /* Parse error */
            printf("Parse error\n");
            if (command_list_head != NULL) {
                command_list_free(command_list_head);
                command_list_head = NULL;
            }
            if (is_interactive) {
                continue;
            } else {
                break;
            }
        }

        /* Make sure we reset our sigint_received flag before spawning */
        sigint_received = 0;

        int spawn_loop_rv = spawn_loop(command_list_head, &rv);

        Command *c = command_list_head;

        /* The waitpid() loop */
        while (c != NULL) {
            if (c->pid == -1) {
                /* Do nothing if we couldn't spawn the process. */
                /* Uncomment the following line for useful (but verbose)
                 * messages in debug mode: */
                /* if (c->argv_list != NULL) {
                 *     DEBUG_PRINTF("Could not spawn %s\n",
                 *         c->argv_list.current);
                 * } */
            } else {
                if (sigint_received) {
                    /* Sent sigint to the process first */
                    /* Ignore any error if the process is already dead */
                    if(-1 == kill(c->pid, SIGINT) && errno != ESRCH) {
                        DEBUG_PRINTF("Could not kill child process\n");
                        perror("kill");
                    }
                }

                int status;
                int wait_rv = waitpid(c->pid, &status, 0);

                /* Make sure that the error was not ECHILD (which would simply
                 * imply that the process exited early, in which case we don't
                 * want to give an error). */
                if (wait_rv != c->pid && errno != ECHILD) {
                    /* Error with wait */
                    perror("waitpid");
                }
            }

            c = c->pipe_to;
        }

        /* Once we're done with the command */
        if (command_list_head != NULL) {
            command_list_free(command_list_head);
            command_list_head = NULL;
        }

        if (spawn_loop_rv == -1) {
            break;
        }
    }

    return 0;
}
