#ifndef _PARSER_H_GUARD
#define _PARSER_H_GUARD

#include "bstring.h"
#include "debug.h"

/* Miscellaneous functions, variables and data structures used for parsing input
 * to the shell. */

/* A linked list of BStrings, for storing our argv */
typedef struct BStringLL {
    BString current;
    struct BStringLL *next;
} BStringLL;

/* Linked list node which stores a parsed command and a pointer to the command
 * to which its output is piped (if any) */
typedef struct Command {
    BStringLL *argv_list;
    /* Pointer to the last element in argv_list */
    BStringLL *argv_list_end;
    /* The file to read from, if any. */
    BString infile;
    /* The file to write to, if any. */
    BString outfile;
    /* pid for the process once we launch it */
    pid_t pid;
    /* A pointer to a command struct which we should pipe output to. May be null
     * if we don't have a command to pipe to. */
    struct Command *pipe_to;
} Command;

/* So that we can process a single token over multiple calls. */
typedef struct TokenizerState {
    int in_escape, in_string;
    /* If we're in a string this will hold either a single or double quote, to
     * determine whether the string was started (and thus will be terminated)
     * with a single or double quote. */
    char string_type;
} TokenizerState;

/* Stores the internal state of the parser in a struct so that our parsing
 * function will be able to process partial data. */
typedef struct ParserState {
    int in_token;
    /* This is the type of operator (|, < or >) which preceded the token (or
     * string of tokens) which we are currently parsing). If it is < or >, we
     * can expect only one token followed by a terminal (newline, EOF, | or the
     * opposite file redirection operator). */
    char block_starter;
    TokenizerState tok_state;
    BString *token_destination;
    Command *current_command;
} ParserState;

/* Initialise a command list. Returns a pointer to the new Command or NULL on
 * failure. */
Command* WARN_UNUSED command_list_create();

/* Goes through a command list and frees everything. */
void command_list_free(Command *c);

/* Copy the elements of a BString over to an argv list. Returns a pointer to the
 * new list or NULL on failure. */
char** WARN_UNUSED bstringll_to_argv_list(BStringLL *ll);

/* Free an argv list allocated by bstringll_to_argv_list */
void free_argv_list(char **argv);

/* Initialise a linked list of BStrings. Returns a pointer to the new list or
 * NULL on failure. */
BStringLL* WARN_UNUSED bstringll_create();

/* Free a linked list of BStrings */
void bstringll_free(BStringLL *l);

/* Initialise a tokenizer state structure. */
void tokenizer_state_init(TokenizerState *s);

/* Initialise a parser state structure. */
void parser_state_init(ParserState *s);

/* Read a "word" from the specified buffer. This tries to reflect real shell
 * word parsing as closely as possible, but naturally there will be
 * discrepencies. Note that this function will trim leading whitespace. Returns
 * number of bytes read or size + 1 if more data is required. Returns a negative
 * number on error. Note that the state structure *must* be initialised before
 * calling read_token. */
int WARN_UNUSED read_token(TokenizerState *s, BString *token, char *buf, int size);

/* Parse num chars from buf into p. Return value:
 *  - On parse error: some value less than 0.
 *  - On parse succcess: the number of bytes which were parsed (including
 *    trailing newline). If this is greater to the size of the buffer passed in,
 *    then it means that parse_buffer *can* process more data, but does not *need*
 *    to.
 *  - On incomplete data: 0. This signals that more data should be read and the
 *    program called again /or/ that the shell should signal an error, if it
 *    already has as much data as it can get. A return value of zero implies
 *    that parse_buffer has processed all of the available data.
 *
 *  Note that the caller is responsible for initialising parser state and the
 *  command list. */
int WARN_UNUSED parse_buffer(ParserState *s, Command *c, char *buf, int num);

#endif // PARSER_H_GUARD
