#ifndef _SPAWN_H_GUARD
#define _SPAWN_H_GUARD

/* Header for the spawn_command function */

/* Spawns a command and returns the PID if successful or some negative value
 * indicating failure otherwise. Note that input_file and output_file can be
 * NULL, but argv must be specified. input_pipe and output_pipe must be either
 * pairs of fds (from the pipe() system call) or NULL (in which case stdin
 * and/or stdout will be used, respectively).
 *
 * Not-so-secret fact: spawn_command is really just a less featureful version of
 * posix_spawn, which is defined in SUSv3 (and thus available on any modern
 * Unix-like system). I'm just implementing it myself to see how fork() and
 * execvp() work. */
pid_t spawn_command(char **argv, char *input_file, char *output_file, int input_pipe[2], int output_pipe[2]);

#endif // _SPAWN_H_GUARD
