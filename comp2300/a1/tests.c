#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "unit.h"
#include "parser.h"
#include "bstring.h"
#include "spawn.h"

/* This file contains a whole bunch of unit tests for the CleNSH codebase. The
 * test suite takes a "let it fail" approach to errors---instead of checking
 * whether each library and system call succeeeds, it simply wraps the call
 * inside a TEST_ASSERT() and continues on regardless of success or failure.
 * This is very helpful for debugging errors which cause certain sets of tests
 * to fail. */

/* Test functions and data structures declared in bstring.h */
void test_bstring() {
    char *test_string = "Oh dear, I guess this is broken then, isn't it? Maybe not?";
    char *test_string_no_match = "This isn't the same string as the one above it.";
    int i;
    BString t;

    TEST_ASSERT(bstring_init(&t));

    TEST_ASSERT(t.length == 0);

    for (i = 0; i < strlen(test_string); i++) {
        TEST_ASSERT(bstring_append(&t, test_string[i]));
    }

    TEST_ASSERT(strcmp(t.data, test_string) == 0);
    TEST_ASSERT(bstring_cstring_eq(&t, test_string));
    TEST_ASSERT(!bstring_cstring_eq(&t, test_string_no_match));

    char *copied = duplicate_bstring_to_cstring(&t);
    TEST_ASSERT(copied != NULL
            && strcmp(t.data, copied) == 0
            && bstring_cstring_eq(&t, copied));
    free(copied);

    bstring_free(&t);
}

/* Helper data structures for the parser tests */
typedef struct TokenizerTest {
    char *test;
    char *expect;
} TokenizerTest;

/* Test the tokenizing functions declared in parser.h */
void test_tokenizer() {
    int i, rv;
    BString tok_result;
    TokenizerState tok_state;
    /* First, test tokenising ability */
    TokenizerTest tests[] = {
        {"nospaces ", "nospaces"},
        {"'simple quotes'", "simple quotes"},
        {"' single quote \" strings'", " single quote \" strings"},
        {"\" double quote stri\'ngs \"", " double quote stri\'ngs "},
        {"space terminal", "space"},
        {"pipe|terminal", "pipe"},
        {"less<terminal", "less"},
        {"greater>terminal", "greater"},
        {"terminal'| in strin'g", "terminal| in string"},
        {"escaped\\ terminal", "escaped terminal"},
        {"like\\ this like that", "like this"},
        {"trailingspaces  ", "trailingspaces"}
    };

    for (i = 0; i < sizeof(tests) / sizeof(tests[0]); i++) {
        TEST_ASSERT(bstring_init(&tok_result));
        tokenizer_state_init(&tok_state);
        rv = read_token(&tok_state, &tok_result, tests[i].test, strlen(tests[i].test));
        TEST_ASSERT(bstring_cstring_eq(&tok_result, tests[i].expect));
        bstring_free(&tok_result);
    }

    /* Now we test its handling of incomplete tokens. */
    TEST_ASSERT(bstring_init(&tok_result));
    tokenizer_state_init(&tok_state);
    /* Should return size + 1 since data is incomplete */
    TEST_ASSERT(read_token(&tok_state, &tok_result, "\\", 1) == 2);
    bstring_free(&tok_result);
    TEST_ASSERT(bstring_init(&tok_result));
    tokenizer_state_init(&tok_state);
    /* Should return size + 1 since data is incomplete */
    TEST_ASSERT(read_token(&tok_state, &tok_result, "' ", 2) == 3);
    bstring_free(&tok_result);
    TEST_ASSERT(bstring_init(&tok_result));
    tokenizer_state_init(&tok_state);
    /* Returns 0, we found a token terminator right away! */
    TEST_ASSERT(read_token(&tok_state, &tok_result, " ", 1) == 0);
    tokenizer_state_init(&tok_state);
    TEST_ASSERT(read_token(&tok_state, &tok_result, "a b", 3) == 1);
    bstring_free(&tok_result);

    /* Now we test piecewise parsing */
    TEST_ASSERT(bstring_init(&tok_result));
    tokenizer_state_init(&tok_state);
    TEST_ASSERT(read_token(&tok_state, &tok_result, "Hello\\ ", 8) == 8);
    TEST_ASSERT(read_token(&tok_state, &tok_result, "world!", 6) == 6);
    TEST_ASSERT(bstring_cstring_eq(&tok_result, "Hello world!"));
    bstring_free(&tok_result);

    /* As above, but with an incomplete token */
    TEST_ASSERT(bstring_init(&tok_result));
    tokenizer_state_init(&tok_state);
    TEST_ASSERT(read_token(&tok_state, &tok_result, "'Hello ", 7) == 8);
    TEST_ASSERT(read_token(&tok_state, &tok_result, "world!'", 7) == 7);
    TEST_ASSERT(bstring_cstring_eq(&tok_result, "Hello world!"));
    bstring_free(&tok_result);
}

/* Test the parser itself */
void test_parser() {
    ParserState s;
    Command *c;

    /* Basic parsing tests */

    c = command_list_create();
    TEST_ASSERT(c != NULL);
    parser_state_init(&s);
    char *first_test_line = "simple\n";
    TEST_ASSERT(parse_buffer(&s, c, first_test_line, strlen(first_test_line))
            == strlen(first_test_line));
    TEST_ASSERT(c->argv_list != NULL
            && bstring_cstring_eq(&c->argv_list->current, "simple"));
    TEST_ASSERT(c->pipe_to == NULL);
    TEST_ASSERT(c->infile.length == 0);
    TEST_ASSERT(c->outfile.length == 0);
    command_list_free(c);

    c = command_list_create();
    TEST_ASSERT(c != NULL);
    parser_state_init(&s);
    char *second_test_line = "somewhat         \t\t  less simple\n";
    TEST_ASSERT(parse_buffer(&s, c, second_test_line, strlen(second_test_line))
            == strlen(second_test_line));
    TEST_ASSERT(c->argv_list != NULL
            && bstring_cstring_eq(&c->argv_list->current, "somewhat"));
    TEST_ASSERT(c->argv_list->next != NULL
            && bstring_cstring_eq(&c->argv_list->next->current, "less"));
    TEST_ASSERT(c->argv_list->next->next != NULL
            && bstring_cstring_eq(&c->argv_list->next->next->current, "simple"));
    TEST_ASSERT(c->pipe_to == NULL);
    TEST_ASSERT(c->infile.length == 0);
    TEST_ASSERT(c->outfile.length == 0);
    command_list_free(c);

    c = command_list_create();
    TEST_ASSERT(c != NULL);
    parser_state_init(&s);
    char *third_test_line = "unterminated";
    TEST_ASSERT(parse_buffer(&s, c, third_test_line, strlen(third_test_line))
            > strlen(third_test_line));
    TEST_ASSERT(c->argv_list != NULL
            && bstring_cstring_eq(&c->argv_list->current, "unterminated"));
    TEST_ASSERT(c->pipe_to == NULL);
    TEST_ASSERT(c->infile.length == 0);
    TEST_ASSERT(c->outfile.length == 0);
    command_list_free(c);

    /* Redirection tests */

    c = command_list_create();
    TEST_ASSERT(c != NULL);
    parser_state_init(&s);
    char *fourth_test_line = "write_to > this\n";
    TEST_ASSERT(parse_buffer(&s, c, fourth_test_line, strlen(fourth_test_line))
            == strlen(fourth_test_line));
    TEST_ASSERT(c->argv_list != NULL
            && bstring_cstring_eq(&c->argv_list->current, "write_to"));
    TEST_ASSERT(c->pipe_to == NULL);
    TEST_ASSERT(c->infile.length == 0);
    TEST_ASSERT(bstring_cstring_eq(&c->outfile, "this"));
    command_list_free(c);

    c = command_list_create();
    TEST_ASSERT(c != NULL);
    parser_state_init(&s);
    char *fifth_test_line = "read_from <this\n";
    TEST_ASSERT(parse_buffer(&s, c, fifth_test_line, strlen(fifth_test_line))
            == strlen(fifth_test_line));
    TEST_ASSERT(c->argv_list != NULL
            && bstring_cstring_eq(&c->argv_list->current, "read_from"));
    TEST_ASSERT(c->pipe_to == NULL);
    TEST_ASSERT(c->outfile.length == 0);
    TEST_ASSERT(bstring_cstring_eq(&c->infile, "this"));
    command_list_free(c);

    c = command_list_create();
    TEST_ASSERT(c != NULL);
    parser_state_init(&s);
    char *sixth_test_line = "read_from_to< this > that\n";
    TEST_ASSERT(parse_buffer(&s, c, sixth_test_line, strlen(sixth_test_line))
            == strlen(sixth_test_line));
    TEST_ASSERT(c->argv_list != NULL
            && bstring_cstring_eq(&c->argv_list->current, "read_from_to"));
    TEST_ASSERT(c->pipe_to == NULL);
    TEST_ASSERT(bstring_cstring_eq(&c->infile, "this"));
    TEST_ASSERT(bstring_cstring_eq(&c->outfile, "that"));
    command_list_free(c);

    c = command_list_create();
    TEST_ASSERT(c != NULL);
    parser_state_init(&s);
    char *seventh_test_line = "read_from_to < this > that < the_other\n";
    TEST_ASSERT(parse_buffer(&s, c, seventh_test_line, strlen(seventh_test_line))
            == -1);
    command_list_free(c);

    c = command_list_create();
    TEST_ASSERT(c != NULL);
    parser_state_init(&s);
    char *eighth_test_line = "write_to >\n";
    TEST_ASSERT(parse_buffer(&s, c, eighth_test_line, strlen(eighth_test_line))
            < 0);
    command_list_free(c);

    /* Pipe tests */

    c = command_list_create();
    TEST_ASSERT(c != NULL);
    parser_state_init(&s);
    char *ninth_test_line = "pipe_to | this_application\n";
    TEST_ASSERT(parse_buffer(&s, c, ninth_test_line, strlen(ninth_test_line))
             == strlen(ninth_test_line));
    TEST_ASSERT(c->argv_list != NULL
            && bstring_cstring_eq(&c->argv_list->current, "pipe_to"));
    TEST_ASSERT(c->pipe_to != NULL && c->pipe_to->argv_list != NULL
            && bstring_cstring_eq(&c->pipe_to->argv_list->current, "this_application"));
    command_list_free(c);

    c = command_list_create();
    TEST_ASSERT(c != NULL);
    parser_state_init(&s);
    char *tenth_test_line = "first | second < infile | third\n";
    TEST_ASSERT(parse_buffer(&s, c, tenth_test_line, strlen(tenth_test_line))
             == strlen(tenth_test_line));
    TEST_ASSERT(c->argv_list != NULL
            && bstring_cstring_eq(&c->argv_list->current, "first"));
    TEST_ASSERT(c->pipe_to != NULL && c->pipe_to->argv_list != NULL
            && bstring_cstring_eq(&c->pipe_to->argv_list->current, "second"));
    TEST_ASSERT(bstring_cstring_eq(&c->pipe_to->infile, "infile"));
    TEST_ASSERT(c->pipe_to->pipe_to != NULL && c->pipe_to->pipe_to->argv_list != NULL
            && bstring_cstring_eq(&c->pipe_to->pipe_to->argv_list->current, "third"));
    TEST_ASSERT(c->pipe_to->pipe_to->pipe_to == NULL);
    command_list_free(c);

    c = command_list_create();
    TEST_ASSERT(c != NULL);
    parser_state_init(&s);
    char *eleventh_test_line = "incomplete | ";
    TEST_ASSERT(parse_buffer(&s, c, eleventh_test_line, strlen(eleventh_test_line))
             == 0);
    TEST_ASSERT(c->argv_list != NULL
            && bstring_cstring_eq(&c->argv_list->current, "incomplete"));
    command_list_free(c);

    /* Test incomplete command handling */

    c = command_list_create();
    TEST_ASSERT(c != NULL);
    parser_state_init(&s);
    char *twelfth_test_line = "unfinished>       ";
    TEST_ASSERT(parse_buffer(&s, c, twelfth_test_line, strlen(twelfth_test_line))
            == 0);
    command_list_free(c);

    c = command_list_create();
    TEST_ASSERT(c != NULL);
    parser_state_init(&s);
    char *thirteenth_test_line = "unfinished> \n      ";
    TEST_ASSERT(parse_buffer(&s, c, thirteenth_test_line, strlen(thirteenth_test_line))
            == -1);
    command_list_free(c);

    c = command_list_create();
    TEST_ASSERT(c != NULL);
    parser_state_init(&s);
    char *fifteenth_test_line = "This > | that";
    TEST_ASSERT(parse_buffer(&s, c, fifteenth_test_line, strlen(fifteenth_test_line))
            == -1);
    command_list_free(c);

    c = command_list_create();
    TEST_ASSERT(c != NULL);
    parser_state_init(&s);
    char *sixteenth_test_line = "this | that > the < ";
    TEST_ASSERT(parse_buffer(&s, c, sixteenth_test_line, strlen(sixteenth_test_line))
            == 0);
    command_list_free(c);

    c = command_list_create();
    TEST_ASSERT(c != NULL);
    parser_state_init(&s);
    char *seventeenth_test_line = "this| that >| the\n";
    TEST_ASSERT(parse_buffer(&s, c, seventeenth_test_line, strlen(seventeenth_test_line))
            == -1);
    command_list_free(c);
}

/* Test BString linked-list specific functionality declared in parser.h */
void test_bstring_ll() {
    /* This is mostly for testing the to_argv/free_argv functionality of
     * BStringLL */
    char *strings[] = {
        "this is the first string",
        "this is the second string",
        "this is the third string"
    };

    BStringLL *head, *this;
    head = this = NULL;

    int i;
    for (i = 0; i < sizeof(strings) / sizeof(strings[0]); i++) {
        BStringLL *new = NULL;
        TEST_ASSERT(NULL != (new = bstringll_create()));
        if (head == NULL) {
            head = new;
            this = head;
        } else {
            this->next = new;
            this = new;
        }

        char *sptr = strings[i];

        while (*sptr != '\0') {
            TEST_ASSERT(bstring_append(&this->current, *sptr));
            sptr++;
        }
    }

    char **argv;
    TEST_ASSERT(NULL != (argv = bstringll_to_argv_list(head)));

    for (i = 0; i < sizeof(strings) / sizeof(strings[0]); i++) {
        /* Now we test that the allocated array is identical to the original */
        TEST_ASSERT(argv[i] != NULL);
        TEST_ASSERT(strcmp(strings[i], argv[i]) == 0);
    }
    TEST_ASSERT(argv[i] == NULL);

    free_argv_list(argv);
    bstringll_free(head);
}

/* Test functions declared in spawn.h */
void test_spawn() {
    char *filename = "_spawn_tests_please_delete";
    char *ls[] = {"echo", "Testing spawn capabilities\n", NULL};
    char *cat[] = {"cat", NULL};
    char *rm[] = {"rm", "-v", filename, NULL};

    /* Try writing to a file */
    pid_t pid = spawn_command(ls, NULL, filename, NULL, NULL);
    TEST_ASSERT(pid != -1);
    DEBUG_PRINTF("Waiting for child process to terminate. This may hang if the previous call failed.\n");
    DEBUG_PRINTF("If this test does hang, scroll back up and fix the errors reported previously\n");
    TEST_ASSERT(pid != -1 && wait(NULL) == pid);

    /* Try reading from a file */
    pid_t pid2 = spawn_command(cat, filename, NULL, NULL, NULL);
    TEST_ASSERT(pid2 != -1);
    DEBUG_PRINTF("Please check that this terminal outputs the string 'Testing spawn capabilities'\n");
    DEBUG_PRINTF("Note that the program may hang again if there were previous test errors\n.");
    TEST_ASSERT(pid2 != -1 && wait(NULL) == pid2);

    /* Try deleting that file (will write to stdout since we're using -v) */
    pid_t pid3 = spawn_command(rm, NULL, NULL, NULL, NULL);
    TEST_ASSERT(pid3 != -1);
    DEBUG_PRINTF("Please check that the file %s is deleted\n", filename);
    DEBUG_PRINTF("As before, the program may hang if there were previous test errors\n.");
    TEST_ASSERT(pid3 != -1 && wait(NULL) == pid3);

    /* Try piping the output of ``ls /dev`` to ``grep ^tty`` */
    char *lsdev[] = {"ls", "/dev", NULL};
    char *grep[] = {"grep", "^tty", NULL};
    int ls_grep_pipe[2];
    TEST_ASSERT(0 == pipe(ls_grep_pipe));
    pid_t lsdev_pid = spawn_command(lsdev, NULL, NULL, NULL, ls_grep_pipe);
    pid_t wcl_pid = spawn_command(grep, NULL, NULL, ls_grep_pipe, NULL);
    TEST_ASSERT(0 == close(ls_grep_pipe[0]));
    TEST_ASSERT(0 == close(ls_grep_pipe[1]));
    TEST_ASSERT(lsdev_pid != -1);
    TEST_ASSERT(wcl_pid != -1);
    DEBUG_PRINTF("Please verify that this test prints a list of ttys connected to this system\n");
    DEBUG_PRINTF("As before, this program may hang if there was an error in a previous test\n");
    TEST_ASSERT(wait(NULL) != -1);
    TEST_ASSERT(wait(NULL) != -1);

    /* Try piping the output of ``ls /dev`` to ``grep ^tty`` by way of ``shuf`` */
    char *shuf[] = {"shuf", NULL};
    int ls_shuf_pipe[2];
    int shuf_grep_pipe[2];
    TEST_ASSERT(0 == pipe(ls_shuf_pipe));
    TEST_ASSERT(0 == pipe(shuf_grep_pipe));
    lsdev_pid = spawn_command(lsdev, NULL, NULL, NULL, ls_shuf_pipe);
    pid_t shuf_pid = spawn_command(shuf, NULL, NULL, ls_shuf_pipe, shuf_grep_pipe);
    TEST_ASSERT(0 == close(ls_shuf_pipe[0]));
    TEST_ASSERT(0 == close(ls_shuf_pipe[1]));
    wcl_pid = spawn_command(grep, NULL, NULL, shuf_grep_pipe, NULL);
    TEST_ASSERT(0 == close(shuf_grep_pipe[0]));
    TEST_ASSERT(0 == close(shuf_grep_pipe[1]));
    TEST_ASSERT(lsdev_pid != -1);
    TEST_ASSERT(wcl_pid != -1);
    TEST_ASSERT(shuf_pid != -1);
    DEBUG_PRINTF("Please verify that this test prints a list of ttys connected to this system, "
            "but in a different order to the one presented above\n");
    DEBUG_PRINTF("As before, this program may hang if there was an error in a previous test\n");
    TEST_ASSERT(wait(NULL) != -1);
    TEST_ASSERT(wait(NULL) != -1);
    TEST_ASSERT(wait(NULL) != -1);
}

int main(int argc, char **argv) {
    test_bstring();
    test_tokenizer();
    test_parser();
    test_bstring_ll();
    test_spawn();
    /* Will return 1 if some tests failed */
    return DID_SOME_TESTS_FAIL;
}
