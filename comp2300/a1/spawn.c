#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "spawn.h"
#include "debug.h"

/* Please see spawn.h for documentation */

/* Logic for a spawned child */
void spawned_child(char **argv, char *input_file, char *output_file, int input_pipe[2], int output_pipe[2]) {
    /* Child code */
    int in_fd = STDIN_FILENO;
    int out_fd = STDOUT_FILENO;

    if (input_file != NULL) {
        in_fd = open(input_file, O_RDONLY);
        if (in_fd == -1) {
            /* open() failed */
            perror(input_file);
            exit(2);
        }

        /* Now we close the input pipe, if there is one */
        if (input_pipe != NULL) {
            if (input_pipe[0] != -1 && -1 == close(input_pipe[0])) {
                DEBUG_PRINTF("Couldn't close read end of input pipe when an input "
                        "file was specified in spawn_command\n");
                perror("close");
            }
            if (input_pipe[1] != -1 && -1 == close(input_pipe[1])) {
                DEBUG_PRINTF("Couldn't close write end of input pipe when an input "
                        "file was specified in spawn_command\n");
                perror("close");
            }
            input_pipe[0] = input_pipe[1] = -1;
        }
    }

    if (output_file != NULL) {
        out_fd = open(output_file, O_CREAT | O_TRUNC | O_WRONLY, 00644);
        if (out_fd == -1) {
            /* open() failed */
            perror(output_file);
            if (input_file != NULL && -1 == close(in_fd)) {
                perror("close");
            }
            exit(3);
        }

        /* Now we close the output pipe, if there is one */
        if (output_pipe != NULL) {
            if (output_pipe[0] != -1 && -1 == close(output_pipe[0])) {
                DEBUG_PRINTF("Couldn't close read end of output pipe when an output "
                        "file was specified in spawn_command\n");
                perror("close");
            }
            if (output_pipe[1] != -1 && -1 == close(output_pipe[1])) {
                DEBUG_PRINTF("Couldn't close write end of output pipe when an output "
                        "file was specified in spawn_command\n");
                perror("close");
            }
            output_pipe[0] = output_pipe[1] = -1;
        }
    }

    if (input_pipe != NULL
     && input_file == NULL
     && input_pipe[0] != STDIN_FILENO) {
        in_fd = input_pipe[0];
        if (input_pipe[1] != -1 && -1 == close(input_pipe[1])) {
            DEBUG_PRINTF("spawn_command could not close write end of input pipe\n");
            perror("close");
        }
    }

    if (output_pipe != NULL
     && output_file == NULL
     && output_pipe[1] != STDOUT_FILENO) {
        out_fd = output_pipe[1];
        if (output_pipe[0] != -1 && -1 == close(output_pipe[0])) {
            DEBUG_PRINTF("spawn_command could not close read end of output pipe\n");
            perror("close");
        }
    }

    if (-1 == dup2(in_fd, STDIN_FILENO)) {
        perror("dup2");
        exit(4);
    }

    if (STDIN_FILENO != in_fd && -1 == close(in_fd)) {
        DEBUG_PRINTF("Couldn't close superfluous input fd in child\n");
        perror("close");
    }

    if (-1 == dup2(out_fd, STDOUT_FILENO)) {
        perror("dup2");
        exit(5);
    }

    if (STDOUT_FILENO != out_fd && -1 == close(out_fd)) {
        DEBUG_PRINTF("Couldn't close superfluous output fd in child\n");
        perror("close");
    }

    /* This automatically searches the PATH for the current process */
    /* Don't get the return value, since it will always be -1 if execvp returns
     * */
    execvp(argv[0], argv);

    /* We shouldn't reach this, but if we have, then there was an error
     * in our execv call. */
    perror(argv[0]);
}

pid_t spawn_command(char **argv, char *input_file, char *output_file, int input_pipe[2], int output_pipe[2]) {
    pid_t pid = fork();
    if (pid == -1) {
        /* fork() failed */
        perror("fork");
        return -1;
    }

    /* Connect input and output pipes if necessary */
    if (pid == 0) {
        /* We're the child. This branch should *always* terminate within
         * this block. */
        /* Try to reset signal handler immediately */
        if (SIG_ERR == signal(SIGINT, SIG_DFL)) {
            perror("signal");
        }

        spawned_child(argv, input_file, output_file, input_pipe, output_pipe);

        /* Shouldn't reach this */
        exit(1);
    }

    /* If we're the parent, return the child pid */
    return pid;
}
