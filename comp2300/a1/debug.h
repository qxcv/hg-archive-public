#ifndef _DEBUG_H_GUARD
#define _DEBUG_H_GUARD

#include <stdio.h>

/* Macros for debugging */

#ifdef DEBUG
#define DEBUG_PRINTF(...) fprintf(stderr, "DEBUG: "); fprintf(stderr, __VA_ARGS__)
#else
/* Otherwise, it's a no-op */
#define DEBUG_PRINTF(...)
#endif

/* We use the following macro in functions where failure can result in program
 * failure (e.g. anything that makes system calls). This makes GCC-compatible
 * compilers give us a warning. */
/* Taken from
 * http://stackoverflow.com/questions/2042780/how-to-raise-warning-if-return-value-is-disregarded-gcc-or-static-code-check
 * */
#define WARN_UNUSED __attribute__((warn_unused_result))

#endif // _DEBUG_H_GUARD
