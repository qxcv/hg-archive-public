#ifndef _GETLINE2_H_GUARD
#define _GETLINE2_H_GUARD

/* This header contains the declaration of my custom implementation of
 * getline(), which is just like GNU getline() except it automatically appends a
 * newline if it reaches EOF and *always* allocates your buffer for you. */
ssize_t getline2(char **lptr, FILE *fp);

#endif // _GETLINE2_H_GUARD
