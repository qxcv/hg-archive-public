; Copyright 2014, Sam Toyer <sam@qxcv.net>

; Permission to use, copy, modify, and/or distribute this software for any purpose
; with or without fee is hereby granted, provided that the above copyright notice
; and this permission notice appear in all copies.

; THE SOFTWARE IS PROVIDED “AS IS” AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO
; THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS.
; IN NO EVENT SHALL ISC BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
; CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA
; OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
; ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
; SOFTWARE.

; drawlang.s: rPeANUt implementation of a simple drawing language for COMP2300
; assignment 2.

macro
readhex &name &output
    ; read a hexadecimal digit
    ; name is a name unique to this instantiation
    ; output is the working register
    ; aoffset is a register holding the ASCII code for 'a'
    ; numoffset is a register holding 'a' + '0'
    readhexsafety&name:
        ; load 0xFFF1 &output not needed :-)
        load 0xFFF0 &output
        jumpz &output readhexsafety&name
        load &output #hextable &output
mend

macro
readhexpair &name &working &output
    readhex rhpa&name &working
    rotate #4 &working &output
    readhex rhpb&name &working
    or &output &working &output ; possibly faster than add
mend

macro
dispatch
    ; Here it is, the one-instruction dispatch loop :-D
    load 0xFFF0 PC
mend

macro
swap &ra &rb
    xor &ra &rb &ra
    xor &ra &rb &rb
    xor &ra &rb &ra
mend

0x00:
    ; Ignore nuls
    dispatch

; Handler table
; "clear" (c = ASCII 99) handler at offset 0
0x63: jump clear
; "fill" (f = ASCII 102) handler at offset 3
0x66: jump fill
; "halt" (f = ASCII 104) handler at offset 5
0x68: halt
; "line" (l = ASCII 108) handler at offset 9
0x6C: jump line
; "point" (p = ASCII 112) handler at offset 13
0x70: jump point
; "rectangle" (p = ASCII 114) handler at offset 15
0x72: jump rectangle

0x100: ; start of the program
    dispatch

rectangle:
    ; r<X><Y><W><H>
    ; readhexpair name working output
    readhexpair rectx R1 R0
    readhexpair recty R2 R1
    readhexpair rectw R3 R2
    readhexpair recth R4 R3
    ; now R0 = x, R1 = y, R2 = w, R3 = h

    ; R2 = x + w
    add R0 R2 R2

    load #6 R7
    ; linestart = y * 6
    mult R1 R7 R1
    load #32 SR

    ; Bitmask calculation
    ; SP holds x
    ; store SP oldsp
    move R0 SP
    ; R0 holds start
    ; start = x - x % 32
    ; this is so that start is block-aligned
    mod SP SR R0
    sub SP R0 R0

    store ZERO linemask0
    store ZERO linemask1
    store ZERO linemask2
    store ZERO linemask3
    store ZERO linemask4
    store ZERO linemask5

    jump rect_inner_for_cond
    rect_inner_for_body:
        ; left_zeros = x - start
        sub SP R0 R6
        ; left_zeros *= -1 for >0 testing
        mult MONE R6 R6
        ; left_zeros > 0
        jumpn R6 rect_left_mask
        ; R6 holds the mask
        move ZERO R6
        jump rect_left_mask_end
        rect_left_mask:
            mult MONE R6 R6
            rotate R6 ONE R6
            add R6 MONE R6
        rect_left_mask_end:

        ; right_zeros = start + 32
        add R0 SR R5
        ; right_zeros -= x + w
        sub R5 R2 R5
        mult MONE R5 R5
        jumpn R5 rect_right_mask
        jump rect_right_mask_end
        rect_right_mask:
            mult MONE R5 R5
            ; right_zeros = 32 - right_zeros
            sub SR R5 R5
            ; right_zeros = 1 << right_zeros
            rotate R5 ONE R5
            ; right_zeros--
            add R5 MONE R5
            ; right_zeros ~= right_zreos
            not R5 R5
            ; mask |= right_mask
            or R5 R6 R6
        rect_right_mask_end:
        ; We have R5 back again
        ; mask ^= -1
        xor R6 MONE R6

        ; R4 stores block offset
        div R0 SR R4
        store R6 #linemask0 R4

        ; start += 32
        add R0 SR R0
    rect_inner_for_cond:
        ; start < x + w
        sub R0 R2 R4
        jumpn R4 rect_inner_for_body

    load linemask0 R0
    load linemask1 R2
    load linemask2 R4
    load linemask3 R5
    load linemask4 R6
    load linemask5 SP

    ; Row apply logic
    jump rect_outer_for_cond
    rect_outer_for_body:
        load R1 #0x7C40 R7
        or R0 R7 R7
        store R7 #0x7C40 R1

        load R1 #0x7C41 R7
        or R2 R7 R7
        store R7 #0x7C41 R1

        load R1 #0x7C42 R7
        or R4 R7 R7
        store R7 #0x7C42 R1

        load R1 #0x7C43 R7
        or R5 R7 R7
        store R7 #0x7C43 R1

        load R1 #0x7C44 R7
        or R6 R7 R7
        store R7 #0x7C44 R1

        load R1 #0x7C45 R7
        or SP R7 R7
        store R7 #0x7C45 R1

        ; row_offset += 6
        load #6 R7
        add R1 R7 R1
        ; h--
        add R3 MONE R3
    rect_outer_for_cond:
        ; h > 0
        jumpnz R3 rect_outer_for_body

    ; load oldsp SP

    dispatch

linemask0: block 1
linemask1: block 1
linemask2: block 1
linemask3: block 1
linemask4: block 1
linemask5: block 1

point:
    ; output in R0
    readhexpair pointx R1 R0
    ; output in R1
    readhexpair pointy R2 R1
    ; now R0 holds x coord and R1 holds y coord
    ; everything else is free for us to use :-)
    ; addr = y * 6
    load R1 #multtable R1
    ; R3 = x / 32
    load R0 #divtable R3
    ; R1 = y / 6 + x / 32
    add R1 R3 R1
    ; R1 holds address
    ; R3 is now free
    ; R0 is now a bitmask
    rotate R0 ONE R0
    ; load cell
    load R1 #0x7C40 R2
    ; OR into R0
    or R2 R0 R2
    ; store back
    store R2 #0x7C40 R1
    dispatch

clear:
    include "clear.s"
    dispatch

fill:
    include "fill.s"
    dispatch

line:
    readhexpair linex0 R1 R0
    readhexpair liney0 R2 R1
    readhexpair linex1 R3 R2
    readhexpair liney1 R4 R3
    ; R0 = x_0, R1 = y_0, R2 = x_1, R3 = y_1

    ; So that we can *ahem* /appropriate/ SP
    ; store SP oldsp

    ; R4 = x1 - x0
    sub R2 R0 R4
    ; R5 = y1 - y0
    sub R3 R1 R5

    ; R6 holds abs(R4)
    jumpn R4 absdx
        move R4 R6
        jump absdxdone
    absdx:
        mult R4 MONE R6
    absdxdone:

    ; R7 holds abs(R5)
    jumpn R5 absdy
        move R5 R7
        jump absdydone
    absdy:
        mult R5 MONE R7
    absdydone:

    ; SP = abs(dx) - abs(dy)
    sub R6 R7 SP
    jumpn SP steep
        load linemulinstr SP
        store SP linemul
        load linedivinstr SP
        store SP linediv
        load linerotateinstr SP
        store SP linerotate

        jump steepdone
    steep:
        ; Swap x0 y0
        swap R0 R1

        ; Swap x1 y1, as above
        swap R2 R3

        ; Also, remember to recalc y1 - y0 and x1 - x0
        sub R2 R0 R4
        sub R3 R1 R5

        ; We'll abs these later

        ; Finally, change plot(x, y) in lineloop to plot(y, x) using our
        ; instructions with swapped operands
        load swlinemulinstr SP
        store SP linemul
        load swlinedivinstr SP
        store SP linediv
        load swlinerotateinstr SP
        store SP linerotate
    steepdone:
    ; R3 is free from here on

    ; if 0 > y1 - y0 <==> y0 > y1
    jumpn R4 swapends
        ; Let R3 contain the error term (overwriting y1)
        load #2 SP
        div R4 SP R3

        jump swapendsdone
    swapends:
        ; dx = |dx|
        mult MONE R4 R4

        ; swap(x0, x1)
        swap R0 R2
        ; swap(y0, y1)
        swap R1 R3

        ; re-calc R5, since we use it later
        sub R3 R1 R5

        ; Use ceiling division to calculate error term, since we are doing a
        ; swap.
        add ONE R4 R3
        load #2 SP
        div R3 SP R3

        ; Err -= 1 so that the < comparison in the loop turns into a <=.
        add MONE R3 R3
    swapendsdone:

    ; Now, calculate the ystep

    ; This is a little different to the Wikipedia algorithm since we have y1 -
    ; y0 in R5 (i.e. 0 > R5 <==> y0 > y1) instead of y0 - y1
    ; This seems like it should be incorrect I need to look into it later.
    jumpn R5 negystep
        load lineyincr SP
        store SP lineystep

        jump negystepdone
    negystep:
        ; if y0 > y1
        load lineydecr SP
        store SP lineystep
        ; R5 = abs(R5)
        mult MONE R5 R5
    negystepdone:

    ; x1++ so that our loop is inclusive
    add ONE R2 R2

    load #32 SR
    jump lineloopcond
    lineloop:
        ; Can I make x purely a bit mask?
        ; Labels are here so that we can swap our instructions
        ; addr = R6 = y * 6
        linemul: halt
        ; R7 = x / 32
        linediv: halt
        ; R6 = y / 6 + x / 32
        add R6 R7 R6
        ; R7 is now a bitmask
        linerotate: halt
        ; load cell
        load R6 #0x7C40 SP
        ; OR R7 into SP
        or SP R7 SP
        ; store back
        store SP #0x7C40 R6

        ; error -= dy
        sub R3 R5 R3

        ; If error < 0:
        jumpn R3 errorneg
            jump errornegdone
        errorneg:
            ; Add ystep to y
            ; This instruction gets filled in above
            lineystep: halt
            ; Add deltax to error
            add R3 R4 R3
        errornegdone:

        ; x_0++
        add ONE R0 R0
    lineloopcond:
        xor R0 R2 SP
        jumpnz SP lineloop

    ; load oldsp SP

    dispatch

    ; oldsp: block 1
    lineyincr: add ONE R1 R1
    lineydecr: add MONE R1 R1

    linemulinstr: load R1 #multtable R6
    linedivinstr: div R0 SR R7
    linerotateinstr: rotate R0 ONE R7

    swlinemulinstr: load R0 #multtable R6
    swlinedivinstr: div R1 SR R7
    swlinerotateinstr: rotate R1 ONE R7

hextable:
    block 48
    block #0
    block #1
    block #2
    block #3
    block #4
    block #5
    block #6
    block #7
    block #8
    block #9
    block 39
    block #10
    block #11
    block #12
    block #13
    block #14
    block #15

multtable:
    include "mult.s"

divtable:
    include "div.s"
