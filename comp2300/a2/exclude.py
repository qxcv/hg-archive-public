#!/usr/bin/python

from shlex import split
from sys import argv, exit, stdout

if __name__ == '__main__':
    if len(argv) != 2:
        print("USAGE: {0} <myfile.s>".format(argv[0]))
        exit(1)
    fp = open(argv[1])
    for line in fp.readlines():
        # we don't subject shlex to the entire line, since that could end in an
        # error
        ls = line.split()
        if len(ls) > 1 and ls[0] == 'include':
            ls = split(line)
            fp2 = open(ls[1])
            stdout.write(fp2.read())
            print('') # extra newline
        else:
            stdout.write(line)
