#!/usr/bin/python

if __name__ == '__main__':
    screenwidth = 192
    screenheight = 160
    screensize = screenwidth * screenheight
    start = 0x7c40
    numwords = int(screensize / 32)
    cases = {
        "clear.s": ("store ZERO {0}".format(start + i) for i in
             xrange(numwords)),
         "fill.s": ("store MONE {0}".format(start + i) for i in
             xrange(numwords)),
         "mult.s": ("block #{0}".format(y * 6) for y in xrange(screenheight)),
         "div.s": ("block #{0}".format(int(x / 32)) for x in
             xrange(screenwidth)),
        }
    for fn, gen in cases.iteritems():
        fp = open(fn, "w")
        fp.write('\n'.join(gen))
        fp.close()
