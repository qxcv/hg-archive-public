#include <stdio.h>
#include <stdlib.h>

#define SIZE 4

void show(int *pa[], int len) {
    int i;
    for (i = 0; i < len; i++) printf("%d ", *pa[i]);
    printf("\n");
}

int cmp(const void *pa, const void *pb) {
    /* Arguments are pointers-to-pointers */
    int a = **(int**)pa;
    int b = **(int**)pb;
    return a - b;
}

int main() {
    int array[SIZE] = {4, 7, 3, 8};
    int *parray[SIZE];
    int i;
    for (i = 0; i < SIZE; i++) parray[i] = &array[i];
    show(parray, SIZE);
    qsort(parray, SIZE, sizeof(parray[0]), cmp);
    show(parray, SIZE);
    return 0;
}
