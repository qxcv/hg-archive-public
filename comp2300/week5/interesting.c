#include <stdio.h>

#define SIZE 100

void a() {
    char message[SIZE] = "Please do not be alarmed, remain calm.";
    puts(message);
}

void b() {
    char message[SIZE] = "This is only a test.";
    message[20] = '.';
    message[21] = '.';
    puts(message);
}

int main() {
    a();
    b();
    return 0;
}
