#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>

#define BUF_SIZE 8192

int main(int argc, char **argv) {
    if (argc != 3) {
        printf("USAGE: %s <source> <destination>\n", argv[0]);
        return 1;
    }

    int fd1, fd2;

    if ((fd1 = open(argv[1], O_RDONLY)) == -1) {
        perror(argv[1]);
        return 1;
    }

    if ((fd2 = open(argv[2], O_WRONLY|O_TRUNC|O_CREAT, S_IRUSR|S_IWUSR)) == -1) {
        close(fd1);
        perror(argv[2]);
        return 1;
    }

    char buf[BUF_SIZE];
    int read_in;

    while (read_in = read(fd1, buf, BUF_SIZE)) {
        if (read_in == -1) {
            perror("read");
            close(fd1);
            close(fd2);
            return 1;
        }
        if (-1 == write(fd2, buf, read_in)) {
            perror("write");
            close(fd1);
            close(fd2);
        }
    }
    close(fd1);
    close(fd2);
    return 0;
}
