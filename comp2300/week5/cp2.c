#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/mman.h>

#define BUF_SIZE 8192

int main(int argc, char **argv) {
    if (argc != 3) {
        printf("USAGE: %s <source> <destination>\n", argv[0]);
        return 1;
    }

    int fd1, fd2;

    if ((fd1 = open(argv[1], O_RDONLY)) == -1) {
        perror(argv[1]);
        return 1;
    }

    if ((fd2 = open(argv[2], O_RDWR|O_TRUNC|O_CREAT, S_IRUSR | S_IWUSR)) == -1) {
        close(fd1);
        perror(argv[2]);
        return 1;
    }

    struct stat fd1_stat;
    if (-1 == fstat(fd1, &fd1_stat)) {
        perror("fstat");
        close(fd1);
        close(fd2);
        return 1;
    }

    if (ftruncate(fd2, fd1_stat.st_size) == -1) {
        perror("ftruncate");
        close(fd1);
        close(fd2);
        return 1;
    }

    char *fp1 = mmap(NULL, fd1_stat.st_size, PROT_READ, MAP_SHARED, fd1, 0);
    if (fp1 == MAP_FAILED) {
        perror("mmap1");
        close(fd1);
        close(fd2);
        return 1;
    }

    char *fp2 = mmap(NULL, fd1_stat.st_size, PROT_WRITE, MAP_SHARED, fd2, 0);
    if (fp2 == MAP_FAILED) {
        perror("mmap2");
        munmap(fp1, fd1_stat.st_size);
        close(fd1);
        close(fd2);
        return 1;
    }

    int i;
    for (i = 0; i < fd1_stat.st_size; i++) {
        fp2[i] = fp1[i];
    }

    munmap(fp1, fd1_stat.st_size);
    munmap(fp2, fd1_stat.st_size);
    close(fd1);
    close(fd2);
    return 0;
}
