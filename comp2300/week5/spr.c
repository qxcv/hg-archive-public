#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <ctype.h>

char *PROMPT = "Rock, Paper, Scissors.  Enter one of: 'r', 'p', 's'. \% ";
char *WIN_MESSAGE = "You win. :(\n";
char *DRAW_MESSAGE = "Draw.\n";
char *LOSE_MESSAGE = "Computer Wins. :)\n";
char *SCOREBOARD = "Your score: %i          Computer : %i\n";
char *SELECTED = "You selected: %s      Computer selected: %s\n";
char *ERROR = "\nPlease enter one of the available options ('r', 'p' or 's')\n";
char *ROCK = "Rock";
char *PAPER = "Paper";
char *SCISSORS = "Scissors";

char choices[3] = "rps";

char *to_string(char c) {
    if (c == 'r') return ROCK;
    if (c == 'p') return PAPER;
    return SCISSORS;
}

int main() {
    srand(time(NULL));
    int comp_score = 0;
    int player_score = 0;
    char in;
    int need_char = 1;
    while (1) {
        printf(PROMPT);
        while (isspace(in = getchar())) ;
        while (1) {
            char waste = getchar();
            if (waste == EOF || waste == '\n') {
                break;
            }
        }
        // We only care about the first character
        if (in == EOF) {
            break;
        }
        int cidx = rand() % 3;
        int pidx;
        switch (in) {
            case 'r':
                pidx = 0;
                break;
            case 'p':
                pidx = 1;
                break;
            case 's':
                pidx = 2;
                break;
            default:
                pidx = -1;
        }
        if (pidx == -1) {
            printf(ERROR);
            continue;
        }
        printf(SELECTED, to_string(in), to_string(choices[cidx]));
        if (pidx == cidx) {
            /* Draw */
            printf(DRAW_MESSAGE);
        } else if (pidx != ((cidx + 1) % 3)) {
            /* Player loses */
            printf(LOSE_MESSAGE);
            comp_score++;
        }
        else {
            /* Player wins */;
            printf(WIN_MESSAGE);
            player_score++;
        }
        printf(SCOREBOARD, player_score, comp_score);
    }
    return 0;
}
