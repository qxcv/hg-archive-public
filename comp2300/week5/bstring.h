#ifndef _BOUNDEDSTRING_H_GUARD
#define _BOUNDEDSTRING_H_GUARD

#include <stdlib.h>

/* How big do we want our strings to be initially? */
#define BSTRING_INITIAL_CAPACITY 8

/* We typedef "struct BoundedString" to "BString" for brevity */
typedef struct BoundedString {
    /* Our heap-allocated storage */
    char *data;
    /* The number of chars currently stored (sans trailing nul) */
    unsigned int length;
    /* The capacity of the array (including trailing nul) */
    unsigned int capacity;
} BString;

/* Append a char to an initialised BString. Returns 0 on failure or non-zero on
 * success */
int bstring_append(BString *str, char data);

/* Initialises a BString. This must be called before using any of the other
 * BString functions. Returns 0 on failure and non-zero otherwise. */
int bstring_init(BString *str);

/* Frees a BString. This function does not return a value. */
void bstring_free(BString *str);

/* Compares two BStrings and returns 1 if they are equal, or 0 otherwise. */
int bstring_eq(BString *a, BString *b);

int bstring_append(BString *str, char data) {
    str->length++;

    if (str->capacity <= str->length) {
        unsigned int newcap = 2 * str->capacity;

        char *newdata = realloc(str->data, sizeof(char) * newcap);
        if (newdata == NULL) {
            /* allocation error */
            return 0;
        }

        str->data = newdata;
        str->capacity = newcap;
    }

    str->data[str->length - 1] = data;
    str->data[str->length] = '\0';

    return 1;
}

int bstring_init(BString *str) {
    str->data = malloc(sizeof(char) * BSTRING_INITIAL_CAPACITY);
    if (str->data == NULL) {
        /* allocation failure */
        return 0;
    }

    str->data[0] = '\0';
    str->capacity = BSTRING_INITIAL_CAPACITY;
    str->length = 0;

    return 1;
}

void bstring_free(BString *str) {
    /* Prevent double-free and free-before-init */
    if (str->data != NULL) {
        free(str->data);
        str->data = NULL;
    }
    str->length = 0;
    str->capacity = 0;
}

int bstring_eq(BString *a, BString *b) {
    /* Our index */
    unsigned int i;

    if (a->length != b->length) {
        return 0;
    }

    /* We know that a->length == b->length, so we only need one comparison */
    for (i = 0; i < a->length; i++) {
        if (a->data[i] != b->data[i]) {
            return 0;
        }
    }

    /* They are equal! */
    return 1;
}

#endif // _BOUNDED_STRING_H_GUARD
