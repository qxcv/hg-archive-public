/* It counts words. Wordily. */

#include <unistd.h>
#include <stdint.h>
#include <stdio.h>

#define BUF_SIZE 8192

uint8_t buf[8192];

int is_wordchar(uint8_t c) {
    /* 32 is space */
    return c > 32;
}

int main(int argc, char **argv) {
    int wc, in_word, i;
    wc = in_word = 0;
    while (1) {
        ssize_t bytes_read = read(0, buf, BUF_SIZE);
        if (!bytes_read) {
            /* EOF */
            break;
        }
        if (bytes_read == -1) {
            /* Error */
            perror("read");
            return 1;
        }
        for (i = 0; i < bytes_read; i++) {
            int wordp = is_wordchar(buf[i]);
            if (wordp && !in_word) {
                wc++;
                in_word = 1;
            } else if (!wordp && in_word) {
                in_word = 0;
            }
        }
    }
    printf("%i\n", wc);
    return 0;
}
