#include <unistd.h>
#include <stdio.h>
#include <ctype.h>

#define BUF_SIZE 8192

int main(int argc, char **argv) {
    if (argc != 1) {
        printf("USAGE: %s\n", argv[0]);
        return 1;
    }

    char buf[BUF_SIZE];
    int read_in;

    while (read_in = read(0, buf, BUF_SIZE)) {
        if (read_in == -1) {
            perror("read");
            return 1;
        }
        int i;
        for (i = 0; i < read_in; i++) {
            if (islower(buf[i])) {
                buf[i] = toupper(buf[i]);
            }
        }
        if (-1 == write(1, buf, read_in)) {
            perror("write");
            return 1;
        }
    }
    return 0;
}
