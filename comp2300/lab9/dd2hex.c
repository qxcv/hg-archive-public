#include <stdio.h>
#include <arpa/inet.h>

// more than we need, but that's fine
#define BUFSIZE 128
#define usage() do {fprintf(stderr, "USAGE: %s <IPv4 address in dotted decimal notation>\n", argv[0]); } while (0);

int main(int argc, char** argv) {
    if (argc != 2) {
        usage();
        return 1;
    }
    struct in_addr address;
    if (inet_pton(AF_INET, argv[1], &address) != 1) {
        usage();
        return 1;
    }
    address.s_addr = htonl(address.s_addr);
    printf("0x%.8X\n", address.s_addr);
    return 0;
}
