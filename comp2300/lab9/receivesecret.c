#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <netdb.h>
#include <unistd.h>

#define USAGE do {printf("USAGE: %s <host> <port>\n", argv[0]);} while(0)

int main(int argc, char **argv) {
    if (argc != 3) {
        USAGE;
        return 1;
    }

    struct addrinfo hints;
    struct addrinfo *addr;
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    if (0 != getaddrinfo(argv[1], argv[2], &hints, &addr)) {
        perror("getaddrinfo");
        return 1;
    }

    int fd = socket(addr->ai_family, addr->ai_socktype, addr->ai_protocol);
    if (fd < 0) {
        perror("socket");
        freeaddrinfo(addr);
        addr = NULL;
        return 1;
    }

    if (0 != bind(fd, addr->ai_addr, addr->ai_addrlen)) {
        perror("bind");
        freeaddrinfo(addr);
        addr = NULL;
        return 1;
    }

    freeaddrinfo(addr);
    addr = NULL;

    if (0 != listen(fd, 1)) {
        perror("listen");
        return 1;
    }

    int comms_fd;
    if (-1 == (comms_fd = accept(fd, NULL, NULL))) {
        perror("accept");
        return 1;
    }

    char *buf[1024];
    ssize_t num;

    while ((num = recv(comms_fd, buf, 1024, 0)) > 0) {
        if (-1 == write(STDOUT_FILENO, buf, num)) {
            perror("write");
            break;
        }
    }
    if (num == -1) {
        perror("recv");
    }
    close(fd);
    close(comms_fd);
    return 0;
}
