#include <stdio.h>
#include <arpa/inet.h>

// more than we need, but that's fine
#define BUFSIZE 128
#define usage() do {fprintf(stderr, "USAGE: %s 0x<IPv4 address in hexadecimal notation>\n", argv[0]); } while (0);

int main(int argc, char** argv) {
    if (argc != 2) {
        usage();
        return 1;
    }
    struct in_addr address;
    if (1 != sscanf(argv[1], "0x%X", &address.s_addr)) {
        usage();
        return 1;
    }
    address.s_addr = htonl(address.s_addr);
    char dest[BUFSIZE];
    if (NULL == inet_ntop(AF_INET, &address, dest, BUFSIZE)) {
        fprintf(stderr, "Error converting to hex format\n");
        return 1;
    }
    puts(dest);
    return 0;
}
