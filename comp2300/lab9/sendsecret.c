#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <netdb.h>
#include <unistd.h>

#define USAGE do {printf("USAGE: %s <host> <port>\n", argv[0]);} while(0)

const char *msg = "Hello, world! This is a message from the client\n";

int main(int argc, char **argv) {
    if (argc != 3) {
        USAGE;
        return 1;
    }

    struct addrinfo hints;
    struct addrinfo *addr;
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    if (0 != getaddrinfo(argv[1], argv[2], &hints, &addr)) {
        perror("getaddrinfo");
        return 1;
    }

    int fd = socket(addr->ai_family, addr->ai_socktype, addr->ai_protocol);
    if (fd < 0) {
        perror("socket");
        freeaddrinfo(addr);
        addr = NULL;
        return 1;
    }

    if (0 != connect(fd, addr->ai_addr, addr->ai_addrlen)) {
        perror("connect");
        freeaddrinfo(addr);
        addr = NULL;
        return 1;
    }

    freeaddrinfo(addr);
    addr = NULL;

    char *buf[1024];
    ssize_t num;

    while ((num = read(STDIN_FILENO, &buf, 1024)) > 0) {
        if (-1 == send(fd, buf, num, 0)) {
            perror("send");
            close(fd);
            break;
        }
    }
    if (num == -1) {
        perror("read");
    }
    close(fd);
    return 0;
}
