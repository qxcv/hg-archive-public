#include <cstdio>
#include <random>

const int screen_width = 192;
const int screen_height = 160;

using namespace std;

random_device dev;
default_random_engine engine(dev());

void genpoint() {
    uniform_int_distribution<int> xdist(0, screen_width - 1);
    uniform_int_distribution<int> ydist(0, screen_height - 1);
    printf("%02x%02x", xdist(engine), ydist(engine));
}

void genpwh() {
    uniform_int_distribution<int> xdist(0, screen_width - 1);
    uniform_int_distribution<int> ydist(0, screen_height - 1);
    int x = xdist(engine);
    int y = ydist(engine);
    printf("%02x%02x", x, y);

    uniform_int_distribution<int> wdist(1, screen_width - x);
    uniform_int_distribution<int> hdist(1, screen_height - y);
    int w = wdist(engine);
    int h = hdist(engine);
    printf("%02x%02x", w, h);
}

int main(int argc, char **argv) {
    int num_operations = 10;

    vector<char> operations;
    operations.push_back('l');
    operations.push_back('p');
    operations.push_back('r');
    // Add these back in as you see fit
    /*operations.push_back('c');
    operations.push_back('f');
    operations.push_back('h');*/

    uniform_int_distribution<int> operdist(0, operations.size() - 1);

    for (int i = 0; i < num_operations; i++) {
        char oper = operations[operdist(engine)];
        putc(oper, stdout);
        switch (oper) {
            case 'l':
                genpoint();
                genpoint();
                break;
            case 'p':
                genpoint();
                break;
            case 'r':
                genpwh();
                break;
            default:
                break;
        }
    }
    printf("h");
    return 0;
}
