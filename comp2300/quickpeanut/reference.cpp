#include <cstdio>
#include <cstdlib>
#include <cstdint>

#include <stdexcept>
#include <string>

using namespace std;

const int screen_width = 192;
const int screen_height = 160;

class Screen {
    public:
        Screen() {
            clear();
        }

        ~Screen() {
        }

        uint32_t framebuffer[screen_height][screen_width / 32];

        void clear() {
            for (int i = 0; i < screen_width / 32; i++) {
                for (int j = 0; j < screen_height; j++) {
                    framebuffer[j][i] = 0;
                }
            }
        }

        void fill() {
            rectangle(0, 0, screen_width, screen_height);
        }

        void point(int x, int y) {
            if (x < 0 || x >= screen_width) {
                throw runtime_error("x coordinate out of bounds\n");
            }
            if (y < 0 || y >= screen_height) {
                throw runtime_error("y coordinate out of bounds\n");
            }
            framebuffer[y][x / 32] |= 1 << (x % 32);
        }

        void rectangle(int x, int y, int w, int h) {
            for (int voffset = y; voffset < y + h; voffset++) {
                for (int hoffset = x; hoffset < x + w; hoffset++) {
                    point(hoffset, voffset);
                }
            }
        }

        void line(int x0, int y0, int x1, int y1) {
            int dx = abs(x1 - x0);
            int dy = abs(y1 - y0);
            int sx = x0 < x1 ? 1 : -1;
            int sy = y0 < y1 ? 1 : -1;
            int err = dx - dy;

            while (true) {
                point(x0, y0);

                if (x0 == x1 && y0 == y1) break;

                int e2 = 2 * err;
                if (e2 > -dy) {
                    err -= dy;
                    x0 += sx;
                }
                if (e2 < dx) {
                    err += dx;
                    y0 += sy;
                }
            }
        }

        void display() {
            for (int j = 0; j < screen_height; j++) {
                printf("%i", j);
                for (int i = 0; i < screen_width / 32; i++) {
                    printf(":%08x", framebuffer[j][i]);
                }
                printf("\n");
            }
        }
};

int main(int argc, char** argv) {
    int rv = 0;
    Screen screen;
    while (true) {
        int instr = getc(stdin);

        int x, y, x0, y0, x1, y1, w, h;
        if (instr == EOF) {
            fprintf(stderr, "Error: encountered EOF before halt.\n");
            rv = 1;
            break;
        }

        if (instr == 'h') {
            break;
        }

        switch ((char)instr) {
            case 'c':
                screen.clear();
                break;
            case 'f':
                screen.fill();
                break;
            case 'p':
                if (scanf("%02x", &x) != 1) {
                    fprintf(stderr, "Error: point missing x argument\n");
                    rv = 1;
                    break;
                }
                if (scanf("%02x", &y) != 1) {
                    fprintf(stderr, "Error: point missing y argument\n");
                    rv = 1;
                    break;
                }
                screen.point(x, y);
                break;
            case 'r':
                if (scanf("%02x", &x) != 1) {
                    fprintf(stderr, "Error: rectangle missing x argument\n");
                    rv = 1;
                    break;
                }
                if (scanf("%02x", &y) != 1) {
                    fprintf(stderr, "Error: rectangle missing y argument\n");
                    rv = 1;
                    break;
                }
                if (scanf("%02x", &w) != 1) {
                    fprintf(stderr, "Error: rectangle missing w argument\n");
                    rv = 1;
                    break;
                }
                if (scanf("%02x", &h) != 1) {
                    fprintf(stderr, "Error: rectangle missing h argument\n");
                    rv = 1;
                    break;
                }
                screen.rectangle(x, y, w, h);
                break;
            case 'l':
                if (scanf("%02x", &x0) != 1) {
                    fprintf(stderr, "Error: line missing x0 argument\n");
                    rv = 1;
                    break;
                }
                if (scanf("%02x", &y0) != 1) {
                    fprintf(stderr, "Error: line missing y0 argument\n");
                    rv = 1;
                    break;
                }
                if (scanf("%02x", &x1) != 1) {
                    fprintf(stderr, "Error: line missing x1 argument\n");
                    rv = 1;
                    break;
                }
                if (scanf("%02x", &y1) != 1) {
                    fprintf(stderr, "Error: line missing y1 argument\n");
                    rv = 1;
                    break;
                }
                screen.line(x0, y0, x1, y1);
                break;
            default:
                fprintf(stderr, "Error: unrecognised instruction: %c\n",
                        instr);
                rv = 1;
        }
        if (rv == 1) break;
    }

    if (rv != 1) screen.display();

    return rv;
}
