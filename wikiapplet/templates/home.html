{% extends "base.html" %}

{% block title %}Proportional-Integral-Derivative Controllers{% end %}

{% block body %}
<script type="text/javascript" src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
<h1>PID Control... with Roombas! <small>from <a href="/u/qxcv">qxcv</a> / <a
            href="#">roomba-pid</a></small></h1>
<iframe src="{{ static_url('pid-demo/index.html') }}"
    sandbox="allow-scripts" id=demo></iframe>
<h2>Description</h2>

<p><em>tl;dr: change the paramters and click &quot;Vroom!&quot; to see the PID controller do its
thing. Click &quot;Reset&quot; if you want to move back to the beginning. Note that
&quot;Initial offset&quot; is the distance of the robot from the line at :math:`t = 0` and
that &quot;Steering bias&quot; is the amount that the robot tries to turn left or right in
the absence of input (for testing the integral parameter).</em></p>
<p>This demo shows how a Proportional-Integral-Derivative (PID) controller can be used to
make a robot (in this case, a crudely drawn robotic vacuum cleaner) approach
(and follow!) a straight line. Try clicking &quot;Vroom!&quot; if you want to see the
robot make a nice, clean approach to the line and then follow it forever.</p>
<p>The neat thing about this demo is that you can play with the PID controller
parameters. If you've never heard of PID controllers before, I suggest that you
go read the beginning of <a class="reference external" href="https://en.wikipedia.org/wiki/PID_controller">this</a>
Wikipedia article. Alternately, you can just read ahead for a basic (if long)
introduction to how PID controllers work. The quick summary is that a PID
controller tries to make the some &quot;process variable&quot;—say, the position of
a robot or the temperature of a fridge—approach a given &quot;set point&quot;—i.e. a line at <span class="math">\(y = 0\)</span>
 or a temperature like 5°C—by changing the
value of its own output according to the value of the process variable.</p>
<p>To build up some intuition about why PID controllers do what they do, let's
start with a much simpler version of the PID controller—the P (or
&quot;proportional&quot;) controller!
The P controller just gives an output (in this case, the amount we steer
left or right) according to <span class="math">\(u(t) = K_p e(t)\)</span>
, where:</p>
<blockquote>
<ul class="simple">
<li><span class="math">\(u(t)\)</span>
 is the output of our controller at time <span class="math">\(t\)</span>
 (the amount we
steer left or right)</li>
<li><span class="math">\(e(t)\)</span>
 is the process variable at time <span class="math">\(t\)</span>
 (our distance from the
line we want to follow)</li>
<li><span class="math">\(K_p\)</span>
 is just a constant (we can use this to control the magnitude of
our response)</li>
</ul>
</blockquote>
<p>Now, every few milliseconds, our robot will measure its distance from the line.
If the line is to its right, it will turn a little bit more towards the right.
If the line is to its left, it will turn a bit more towards the left. The amount
it turns will be proportional to its distance from the line (hence the name
&quot;proportional controller&quot;).</p>
<p>Alas, our robot doesn't converge on the line as we hoped it would. Why do you
think this may be? If you need a clue, examine the following image of a P
controller in action:</p>
<img alt="oscillation.png" src="oscillation.png" />
<p>Have you figured out what the problem is? It turns out that when our robot
reaches its &quot;set point&quot; (that is, its path intersects with the line we want it
to follow), its trajectory is away from the line, because it doesn't realise
that it has to steer back the other way until it reaches the other side of the
line! Once it turns back towards the line, we have the same problem again,
resulting in the pattern of oscillation above.</p>
<p>We can fix this by upgrading our old, worn-out P controller to a shiny new PD
(&quot;proportional-derivative&quot;) controller. The PD controller uses a slightly different
equation which incorporates the derivative (that is, the rate of change) of our
process variable. Mathematically, this looks like:</p>
<div class="math">
\begin{equation*}
u(t) = K_p e(t) + K_d \frac{\mathrm{d}}{\mathrm{d}t} e(t)
\end{equation*}
</div>
<p>Where <span class="math">\(\frac{\mathrm{d}}{\mathrm{d}t} e(t)\)</span>
 is the rate at which our process variable (our
proximity to the line) is changing at time <span class="math">\(t\)</span>
, and <span class="math">\(K_d\)</span>
 is a
constant which changes how much our controller's output is affected by the
rate-of-change of our process variable.</p>
<p>At first glance, this looks like an excellent system. As the robot turns towards
the line, our distance from the line will decrease, which means that the
rate-of-change of our process variable will be a big negative number. Thus, the
robot will start turning back away from the line before it reaches it,
preventing it from overshooting the line and then oscillating from side to side.</p>
<p>Of course, if it really was an excellent system, I wouldn't have prepended &quot;At
first glance&quot; to the previous paragraph. Let's see how this new system fails:</p>
<img alt="pd.png" src="pd.png" />
<p>Well, that's... weird. The robot seems to be following <em>a</em> line, but it's not
the line we want it to follow. Instead, it appears to be following an invisible
line slightly below the one we wanted it to follow. What could be causing this?</p>
<p>It turns out that I previously made a small omission of fact (an engineer might
call this a &quot;lie&quot;). Our robot is actually defective. The motors are not
correctly calibrated, so the left wheel turns a little faster than the right
one. This means that our robot is constantly trying to turn right! It reaches an
equilibrium point at the &quot;invisible line&quot; that I mentioned before, whereby the
steering to the left by the PD controller is exactly equal to the steering to
the right by our uncalibrated motors.</p>
<p>We can fix this by making another upgrade. This time, we'll move to a full PID
controller, with a proportional term, a derivative term and an integral term. The
purpose of the integral term is to continue to increase in size so long as our
process variable is not equal to the set point. Thus, the longer it takes to
converge with the line, the more the robot will turn. Mathematically:</p>
<div class="math">
\begin{equation*}
u(t) = K_p e(t) + K_i \int_0^t\! e(x) \, \mathrm{d}x + K_d \frac{\mathrm{d}}{\mathrm{d}t} e(t)
\end{equation*}
</div>
<p>Where <span class="math">\(\int_0^t\! e(x) \, \mathrm{d}x\)</span>
 is the time-integral of our process
variable and <span class="math">\(K_i\)</span>
 is another constant which we use to change how much our
integral term affects the overall controller output. Don't worry too much if you
don't understand this business about integrals. Just remember that the integral
term will become larger the longer our robot takes to reach the line (this
definition is slightly inaccurate, although it will do for our purposes).
However, if you're still curious about integrals, I suggest you check out <a class="reference external" href="https://youtu.be/LkdodHMcBuc">this
video</a>.</p>
<p>Now, let's plug in our new formula and see what happens:</p>
<img alt="pid-begin.png" src="pid-begin.png" />
<p>At first, we seem to have the same problem as before, but then we start moving
back towards the line:</p>
<img alt="pid-end.png" src="pid-end.png" />
<p>After a while, we converge:</p>
<img alt="pid-final.png" src="pid-final.png" />
<p>Victory! It took a while, but we finally got the robot to do our bidding!
There's still room for improvement, though: even with our derivative term, the
new integral term caused the robot to overshoot the line significantly. Not only
that, but the robot then took a long time to get back to the line. This is where
those parameters—<span class="math">\(K_p\)</span>
,<span class="math">\(K_i\)</span>
 and <span class="math">\(K_d\)</span>
, —that we
talked about previously come in. If we find the right value for each of these
parameters, we can improve the robot's behaviour even further. Try putting some
of your own parameters into the applet above, and see how well you can make the
robot behave.</p>
{% end %}
