1. Get a base design
#. Design the homepage
#. Write models
#. Code up homepage
#. Move everything into "wikiapp" (this is specified as the main package
   setup.py, but is empty as of this commit)
#. Code applet rendering
#. Code applet creation
#. Code applet editing
#. Code applet deletion
#. Code applet versioning
#. Code user profiles
#. Code comments
#. Code user forking ability (for personal experimentation)
#. ???
#. Profiterole.
