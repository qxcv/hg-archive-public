WikiApplet: The collaborative tool for building applets
=======================================================

What is it?
-----------

WikiApplet is a way to build awesome educational applets and share them
with the world. If you've ever had a blast playing with the interactive aerofoil
lift demonstrations over at NASA's website or been enraptured by those Java
programs which let you create your own electronic circuits, then you'll know
exactly why applets are such a great way of teaching physical concepts. The aim
of WikiApplet is twofold: firstly, to foster the development of new applets by
providing source control and hosting. Secondly, to act as a public resource for
teachers and students who want the invaluable hands-on experience given by
practical demonstrations, but who don't have the time, money or expertise to set
them up.
