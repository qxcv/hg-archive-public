// constants
var TARGET_FRAMERATE = 30,
    STATE_READY = 0,
    STATE_RUNNING = 1,
    // why not?
    MARKER_RATE = Math.sqrt(2),
    MAX_MARKERS = 30 * MARKER_RATE,
    MAX_OMEGA = 3 * Math.PI

// actual logic

/* TODO:
 *  - Debugging output (P, I & D values, distance travelled)
 */

function PIDController(kp, ki, kd, p) {
    return {
        kp: kp,
        ki: ki,
        kd: kd,
        p: p,
        i: 0,
        d: 0,

        update: function(dt, p) {
            this.d = (p - this.p) / dt
            this.i = this.i + dt * (p + this.p) / 2
            this.p = p
        },

        get_value: function() {
            return this.kp * this.p + this.ki * this.i + this.kd * this.d
        }
    }
}

function add_vec(v1, v2) {
    return [v1[0] + v2[0], v1[1] + v2[1]]
}

function ptoc(r, theta) {
    // note that theta is counterclockwise (unlike roomba heading)
    return [r * Math.cos(theta), r * Math.sin(theta)]
}

function draw_roomba(ctx) {
    // draws a roomba facing straight forward at the origin
    // this may take some work to make it look convincing
    var rad = 0.15,
        inner_rad = 0.95 * rad,
        outer_bar_angle = 3 * Math.PI / 4,
        inner_bar_angle = 0.7 * outer_bar_angle,
        bar_inner_rad = 0.5 * rad,
        inner_fill = 'rgb(218, 218, 218)',
        inner_stroke = 'rgb(192, 192, 192)',
        inner_stroke_width = 0.2 * rad,
        bar_fill = 'rgb(50, 50, 50)',
        bar_stroke = 'rgb(45, 45, 45)',
        bar_stroke_width = 0.1 * rad,
        button_fill = 'rgb(250, 250, 250)',
        button_stroke = 'rgb(180, 180, 180)',
        button_stroke_width = 0.05 * rad,
        button_text = 'rgb(80, 80, 80)'

    // draw body
    ctx.beginPath()
    ctx.strokeStyle = inner_stroke
    ctx.fillStyle = inner_fill
    ctx.lineWidth = inner_stroke_width
    ctx.arc(0, 0, inner_rad, 0, 2 * Math.PI, false)
    ctx.stroke()
    ctx.fill()

    // draw front bar (fender)
    var deviation_outer = outer_bar_angle / 2,
        deviation_inner = inner_bar_angle / 2
    ctx.beginPath()
    ctx.fillStyle = bar_fill
    ctx.strokeStyle = bar_stroke
    ctx.lineWidth = bar_stroke_width
    // inner arc
    ctx.arc(0.2 * rad, 0, bar_inner_rad, -deviation_inner, deviation_inner, false)
    // outer arc
    ctx.arc(0, 0, rad, deviation_outer, -deviation_outer, true)
    ctx.closePath()
    ctx.stroke()
    ctx.fill()

    // draw button
    ctx.beginPath()
    ctx.fillStyle = button_fill
    ctx.strokeStyle = button_stroke
    ctx.lineWidth = button_stroke_width
    ctx.moveTo(0, 0.2 * rad)
    ctx.bezierCurveTo(0.1 * rad, 0.2 * rad, 0.1 * rad, -0.2 * rad, 0, -0.2 * rad)
    ctx.bezierCurveTo(-0.1 * rad, -0.2 * rad, -0.1 * rad, 0.2 * rad, 0, 0.2 * rad)
    ctx.stroke()
    ctx.fill()
    // button text
    ctx.beginPath()
    ctx.save()
    ctx.rotate(Math.PI / 2)
    ctx.fillStyle = button_text
    var font_size = 0.1 * rad
    ctx.font = 'bold ' + font_size + 'px sans-serif'
    ctx.fillText("clean", -2 * font_size, font_size / 4)
    ctx.restore()
}

function draw_marker(ctx, alpha) {
    // markers are 5cm across
    var marker_rad = 0.025,
        marker_line_thickness = 0.02,
        stroke_colour = 'rgba(80, 255, 80, ' + alpha + ')'
    ctx.beginPath()
    ctx.lineWidth = marker_line_thickness
    ctx.strokeStyle = stroke_colour
    ctx.arc(0, 0, marker_rad, 0, 2 * Math.PI, false)
    ctx.stroke()
}

function draw_line(ctx, width) {
    // always be 5px across
    var line_width = 5,
        line_colour = "rgb(255, 0, 0)"
    ctx.beginPath()
    ctx.lineWidth = line_width
    ctx.strokeStyle = line_colour
    ctx.moveTo(0, 0)
    ctx.lineTo(width, 0)
    ctx.stroke()
}

var roomba = {
        xy: [0, 0.5],
        v: 0.4,
        // radians, clockwise
        heading: 0,
        bias: 0
    },
    state = STATE_READY,
    camera = {
        // distances in metres
        // "top" and "right" are both screen edge locations in world form
        left: -1,
        top: 1,
        // pixels per metre
        zoom: 150,
        // width and height in pixels
        width: 0,
        height: 0,
    },
    // this is just a list of coordinates
    markers = [],
    // this is reset when the roomba is moved before starting
    controller = PIDController(1, 0, 0, roomba.xy[1]),
    // for tracking when we need to put down a marker
    last_marker_time = 0,
    zoom_levels = [600, 300, 150, 75, 37.5],
    current_zoom = parseInt(zoom_levels.length / 2)

function draw_frame_wrapper(ctx, elem) {
    var prev_time = Date.now() / 1000
    function inner() {
        ct = Date.now() / 1000
        dt = ct - prev_time
        draw_frame(ctx, elem, dt)
        prev_time = ct
    }
    return inner
}

function draw_frame(ctx, elem, dt) {
    function to_screen(xy) {
        // convert world coordinates into screen coordiantes
        return [
            (xy[0] - camera.left) * camera.zoom,
            (camera.top - xy[1]) * camera.zoom
        ]
    }

    // this code from
    // https://stackoverflow.com/questions/2142535/how-to-clear-the-canvas-for-redrawing
    // everything except the clearRect call is unnecessary unless we have
    // uncleared transforms, in which case clearrect's arguments will get
    // transformed according to those transforms which we have applied (and we
    // thus need to make a new "reset" transform)
    // Store the current transformation matrix
    // ctx.save()

    // Use the identity matrix while clearing the canvas
    // ctx.setTransform(1, 0, 0, 1, 0, 0)
    ctx.clearRect(0, 0, camera.width, camera.height)

    // Restore the transform
    // ctx.restore()

    if (state == STATE_RUNNING) {
        // if the roomba is moving...
        controller.update(dt, roomba.xy[1])
        var diff = controller.get_value() * dt + roomba.bias * dt
        var max = MAX_OMEGA * dt
        diff = diff > max ? max : diff
        diff = diff < -max ? -max : diff
        roomba.heading -= diff

        roomba.heading = roomba.heading > Math.PI / 2 ? Math.PI / 2 : roomba.heading
        roomba.heading = roomba.heading < -Math.PI / 2 ? -Math.PI / 2 : roomba.heading

        var mv = ptoc(roomba.v * dt, roomba.heading)
        roomba.xy = add_vec(roomba.xy, mv)

        var max_right = 0.75 * camera.width / camera.zoom
        if (roomba.xy[0] > camera.left + max_right) {
            camera.left = roomba.xy[0] - max_right
        }

        var ct = Date.now() / 1000
        if ((ct - last_marker_time) >  1 / MARKER_RATE) {
            // put down a marker
            markers.unshift(roomba.xy)
            last_marker_time = ct
            if (markers.length > MAX_MARKERS) {
                markers.pop()
            }
        }
    }

    // draw main line
    // it is always at y = 0
    ctx.save()
    var drawc = to_screen([0, 0])
    ctx.translate(0, drawc[1])
    draw_line(ctx, elem.width)
    ctx.restore()

    // draw markers, weed out old markers
    for (var i = 0; i < markers.length; i++) {
        var alpha = (MAX_MARKERS - i) / MAX_MARKERS
        ctx.save()
        var drawc = to_screen(markers[i])
        ctx.translate(drawc[0], drawc[1])
        ctx.scale(camera.zoom, camera.zoom)
        draw_marker(ctx, alpha)
        ctx.restore()
    }

    // draw roomba
    ctx.save()
    var drawc = to_screen(roomba.xy)
    ctx.translate(drawc[0], drawc[1])
    ctx.scale(camera.zoom, camera.zoom)
    ctx.rotate(-roomba.heading)
    draw_roomba(ctx)
    ctx.restore()
}

function camera_left_ready() {
    camera.left = -0.25 * camera.width / camera.zoom
}

function toggle_state() {
    state = state == STATE_READY ? STATE_RUNNING : STATE_READY
    on_state_change()
}

function on_state_change() {
    var b = document.querySelector("#go")
    if (state == STATE_READY) {
        roomba.v = 0
        roomba.xy = [0, 0]
        roomba.heading = 0
        markers = []
        b.textContent = "Vroom!"

        camera_left_ready()

        on_offset_change()
    }
    else {
        roomba.v = 0.5
        controller = PIDController(controller.kp, controller.ki, controller.kd, roomba.xy[1])
        b.textContent = "Reset"
    }
}

function on_offset_change() {
    var e = document.getElementById("offset")
    var v = parseFloat(e.value)
    if (isNaN(v)) {
        // XXX: DRY
        e.style.borderColor = "red"
    }
    else {
        e.style.borderColor = "gray"
        if (state == STATE_READY) {
            roomba.xy[1] = v
        }
    }
}

function on_zoom_change(zoomout) {
    if (!zoomout) {
        return function() {
            if (current_zoom == zoom_levels.length - 1) {
                return
            }
            camera.zoom = zoom_levels[++current_zoom]
            on_resize()
        }
    }
    return function() {
        if (current_zoom == 0) {
            return
        }
        camera.zoom = zoom_levels[--current_zoom]
        on_resize()
    }
}

function on_parameter_change() {
    labels = ["kp", "ki", "kd", "bias"]
    for (var i = 0; i < labels.length; i++) {
        var e = document.getElementById(labels[i])
        var v = parseFloat(e.value)
        if (isNaN(v)) {
            e.style.borderColor = "red"
        }
        else {
            e.style.borderColor = "gray"
            if (labels[i] == "bias") {
                roomba.bias = v
            }
            else {
                controller[labels[i]] = v
            }
        }
    }
}

function on_resize_wrapper(ctx, elem) {
    // I am a serious closure abuser
    return function() {
        // resize code
        // make the canvas the right size
        var elem = document.querySelector('canvas')
        elem.width = elem.offsetWidth
        elem.height = elem.offsetHeight
        camera.width = elem.width
        camera.height = elem.height

        // make it so that y = 0 is always in the middle of the screen
        camera.top = camera.height / camera.zoom / 2

        if (state == STATE_READY) {
            camera_left_ready()
        }
    }
}

document.addEventListener('DOMContentLoaded', function() {
    // initialisation code
    var elem = document.querySelector('canvas')
    var ctx = elem.getContext('2d')

    on_resize = on_resize_wrapper(ctx, elem)
    on_resize()
    window.addEventListener('resize', on_resize, false)

    param_ids = ["kp", "ki", "kd", "bias"]
    for (var i = 0; i < param_ids.length; i++) {
        var e = document.getElementById(param_ids[i])
        e.addEventListener("change", on_parameter_change)
    }

    var e = document.getElementById("offset")
    e.addEventListener("change", on_offset_change)
    on_offset_change()

    var b = document.getElementById("go")
    b.addEventListener("click", toggle_state)
    on_state_change()

    var b = document.getElementById("zoomin")
    b.addEventListener("click", on_zoom_change(true))
    var b = document.getElementById("zoomout")
    b.addEventListener("click", on_zoom_change(false))

    window.setInterval(draw_frame_wrapper(ctx, elem), 1000 / TARGET_FRAMERATE)
});
