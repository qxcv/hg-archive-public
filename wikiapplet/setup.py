#!/usr/bin/python
from distutils.core import setup

# XXX this hasn't been structured properly. Rewrite this when I have some code
# (and more deps)

setup(
        name='wikiapplet',
        version='0',
        author='qxcv',
        license='GNU AGPL v3',
        description='The code that powers wikiapplet',
        long_description=open('README.rst').read(),
        packages=['wikiapp'],
        requires=['tornado'],
)
