#!/usr/bin/python

"""This program handles everything to do with running and maintaining a
wikiapplet instance."""

from os import path

from tornado.ioloop import IOLoop
from tornado.options import define, options
from tornado.web import Application, RequestHandler, StaticFileHandler

define('debug', default=True, type=bool,
        help="Start in debug mode")

settings = {
        'debug':         options.debug,
        'template_path': path.join(path.dirname(__file__), 'templates'),
        'static_path':   path.join(path.dirname(__file__), 'static'),
}

class BaseHandler(RequestHandler):
    pass

class HomeHandler(BaseHandler):
    def get(self):
        self.render('home.html')

# In the Tornado tutorial, they prefix all of their url patterns with an "r"
# (like r"/home"), which means that string escapes (like \n) are ignored. This
# is handy for escaping regex characters (if you want to match a literal [ or ],
# for example). However, the "r" prefix has been omitted here where not
# necessary.
urls = [
    ('/', HomeHandler),
    ('/static/(.+)', StaticFileHandler, {'path', settings['static_path']}),
]

if __name__ == '__main__':
    app = Application(urls, **settings)
    app.listen(8080)
    IOLoop.instance().start()
