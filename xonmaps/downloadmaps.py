#!/usr/bin/python

from os.path import isdir, isfile, join
import re
from sys import argv, exit
from urllib import urlopen
from urlparse import urljoin

from BeautifulSoup import BeautifulSoup

SHA1_RE = re.compile(r'(?:-full)?(?:-[0-9a-fA-F]{40})+\.pk3')

def soupify_page(url):
    page_obj = urlopen(url)
    page_data = page_obj.read()
    return BeautifulSoup(page_data)

def remove_map_sha1(mapname):
    """Removes the git sha1 cruft from autobuild map names"""
    return SHA1_RE.sub('.pk3', mapname)

def parse_anchorlist(anchors, page_root):
    rv = []
    for anchor in anchors:
        attrs = dict(anchor.attrs)
        href = attrs.get('href', None)
        # make sure it's there, it's a pk3 and it's in the current directory
        if href is not None and href.endswith(".pk3") and '/' not in href:
            pk3name = remove_map_sha1(href)
            # not join() because we're not using native FS pathnames
            rv.append((urljoin(page_root, href), pk3name))
    return rv

def scrape_autobuilds():
    page_root = "http://beta.xonotic.org/autobuild-bsp/"
    print('Scraping {0}'.format(page_root))
    soup = soupify_page(page_root)
    tds = soup.findAll(attrs={'class':'fullpk3'})
    anchors = filter(
                    lambda x: x is not None,
                    map(lambda x: x.find("a"), tds)
            )
    return parse_anchorlist(anchors, page_root)

def scrape_q3maps():
    rv = []
    return rv

def scrape_octopusit():
    # mirror down at the moment
    return []
    page_root = "http://octopusit.net.au/xonotic_maps/"
    print('Scraping {0}'.format(page_root))
    soup = soupify_page(page_root)
    return parse_anchorlist(soup.findAll("a"), page_root)

def build_pk3_list():
    return scrape_octopusit() + scrape_q3maps() + scrape_autobuilds()

def download_map(url, dest):
    print('Downlading {url} to {dest}'.format(**locals()))
    mapdl = urlopen(url)
    fp = open(dest, 'w')
    fp.write(mapdl.read())

if __name__ == '__main__':
    if len(argv) != 2 or not isdir(argv[1]):
        print("USAGE: {progname} <download dir>".format(progname=argv[0]))
        exit(1)
    targets = build_pk3_list()
    for target, mapname in targets:
        dest = join(argv[1], mapname)
        if isfile(dest):
            print("Download of {mapname} from {target} failed: destination exists".format(**locals()))
            continue
        download_map(target, dest)
