#!/usr/bin/python

from collections import defaultdict
from fnmatch import filter as gfilter
from glob import iglob
from hashlib import sha1
from json import dumps
from os import path, sep
import re
from shutil import copy
from zipfile import ZipFile, is_zipfile

from tornado.template import Template

MAP_PATH = './maps/'
BUILD_PATH = './build/'
STATIC_PATH = './static/'
# map information
MAPDB_FILENAME = 'mapdb.json'
# map fts index
FTSINDEX_FILENAME = 'ftsindex.json'
CONFIG_FILENAME = 'config.json'
THUMB_PATH = 'thumbs/'
TEMPLATE_FILENAME = 'template.html'
FTS_FIELDS = [
        # (field name, score weight)
        ('pk3name', 10),
        ('title', 10),
        ('description', 5),
        ('author', 5),
    ]
# for q3a .arena/.defi file translation
DEFI_FIELD_TRANSLATION = {
        'map': 'title',
        'longname': 'description',
        'author': 'author',
    }
# no \w here due to it including the underscore
WORD_REGEX = r'[a-zA-Z0-9]+'
_WORD_REGEX_COMP = re.compile(WORD_REGEX)
CONFIG = {
            'map_download_root': '/',
            'thumb_path': THUMB_PATH,
            # Xonotic has too many gametypes :P
            # TODO: generate this automatically
            'gametypes': ['dm', 'lms', 'arena', 'rune', 'ft', 'ka', 'ctf', 'nexball', 'rc', 'kh', 'ca', 'freezetag', 'keepaway', 'cts', 'race', 'as', 'dom', 'ons'],
            'fts_fields': FTS_FIELDS,
            'word_regex': WORD_REGEX,
            'trackers': ["udp://tracker.openbittorrent.com:80", "udp://tracker.publicbt.com:80", "udp://tracker.ccc.de:80"],
         }

class Map(object):
    def __init__(self, filename):
        """Initialise a Map object from a filename. Must be a valid zip file"""
        self.data = ZipFile(filename)
        self.meta = self.parse_meta()
        self.meta['pk3name'].append(path.basename(filename))
        self.meta['size'] = path.getsize(filename)
        fp = open(filename, 'rb')
        self.meta['sha1'] = sha1(fp.read()).hexdigest()
        fp.close()

    def parse_meta(self):
        """Find the mapinfo in self.data and parse it, returning a defaultdict"""
        names = self.data.namelist()
        is_defi = False
        metafile_candidates = gfilter(names, 'maps/*.mapinfo')
        metafile_candidates.extend(gfilter(names, '*.mapinfo'))
        meta = defaultdict(lambda: [])
        if len(metafile_candidates) == 0: 
            defi_candidates = gfilter(names, '[Ss]cripts/*.defi')
            defi_candidates.extend(gfilter(names, '[Ss]cripts/*.arena'))
            if len(defi_candidates) != 0:
                is_defi = True
                metafile_candidates = defi_candidates
            else: return meta
        metafile = self.data.read(metafile_candidates[0])
        metadata = filter(lambda x: not x.isspace(), metafile.splitlines())
        for line in metadata:
            data = line.split('//')[0].split(None, 1)
            # if we have a key but no value...
            if len(data) < 2: continue
            if is_defi:
                if data[0] in DEFI_FIELD_TRANSLATION:
                    data[0] = DEFI_FIELD_TRANSLATION[data[0]]
                # get rid of double quotes and spaces in the field
                data[1] = data[1].strip('" ')
                # handle CPMA maps
                if data[0] == 'style' and data[1] == 'run':
                    data[0] = 'gametype'
                    data[1] = 'cts'
            # this handles consecutive lines like gametype dm\ngametype ctf
            # to produce meta['gametype'] = ['dm', 'ctf']
            # it also fixes "gametype ctf 0 30 100" to produce "gametype ctf"
            if data and data[0] == 'gametype' and len(data) >= 2:
                meta[data[0]].append(data[1].split()[0])
            elif data: meta[data[0]].extend(data[1:])
        return meta

    def get_thumb(self):
        """returns a (data, extension) tuple for the level's thumbnail"""
        names = self.data.namelist()
        thumb_candidates = gfilter(names, '[Mm]aps/*.tga')
        thumb_candidates.extend(gfilter(names, '[Mm]aps/*.jpg'))
        thumb_candidates.extend(gfilter(names, '[Ll]evelshots/*.jpg'))
        thumb_candidates.extend(gfilter(names, '[Ll]evelshots/*.tga'))
        thumb_candidates = filter(lambda x: len(x.split(sep)) == 2, thumb_candidates)
        if len(thumb_candidates) == 0:
            return (None, None)
        else:
            candidate = thumb_candidates[0]
            return (self.data.read(candidate), path.splitext(candidate)[1])

    def __getitem__(self, itemname):
        return self.meta[itemname]

    def __setitem__(self, itemname, value):
        self.meta[itemname].append(value)

    def __repr__(self):
        return self['pk3name'][0]

def map_iterator(map_path):
    fn_iter = iglob(path.join(map_path, '*.pk3'))
    for item in fn_iter:
        if is_zipfile(item):
            yield item

def create_ftsindex(mapdb):
    fieldnames = [field[0] for field in FTS_FIELDS]
    rv = {fieldname: defaultdict(lambda: defaultdict(lambda: 0)) for fieldname in fieldnames}
    for key in mapdb:
        for fieldname, weight in FTS_FIELDS:
            field = mapdb[key][fieldname]
            if not field:
                continue
            words = (m.group().lower() for m in _WORD_REGEX_COMP.finditer(' '.join(mapdb[key][fieldname])))
            for word in words:
                # don't index the word 'pk3' in a pk3name, as it will always be there
                if fieldname == 'pk3name' and word == 'pk3':
                    continue
                rv[fieldname][word][key] += 1
    return rv

def build_mapdb(files, write_images=False, image_path=path.join(BUILD_PATH, THUMB_PATH)):
    maps = (Map(fn) for fn in files)
    mdb = {}
    for m in maps:
        if write_images:
            thumb, ext = m.get_thumb()
            if thumb is not None:
                fp = open(path.join(image_path, m['pk3name'][0] + ext), 'w')
                fp.write(thumb)
                fp.close()
                m['has_thumb'] = True
        m['has_thumb'] = False
        mdb[repr(m)] = m.meta
    return mdb

def write_json(data, path, name=None):
    print "Writing data to {0}".format(path)
    js = dumps(data)
    if name is not None: js = 'var ' + name + '=' + js
    fp = open(path, 'w')
    fp.write(js)
    fp.close()

def render_template(path, **kwargs):
    fp = open(path, 'r')
    t = Template(fp.read(), compress_whitespace=True)
    fp.close()
    return t.generate(**kwargs)

def transfer_static_files(src, dest):
    for static_file in iglob(path.join(src, '*')):
        print 'Copying static file "{0}" to build directory'.format(static_file)
        copy(static_file, BUILD_PATH)

def render_index(src, dest, **kwargs):
    d = path.join(dest, 'index.html')
    print 'Rendering "{0}" to "{1}"'.format(src, d)
    html = render_template(src, **kwargs)
    html_fp = open(d, 'w')
    html_fp.write(html)
    html_fp.close()

if __name__ == '__main__':
    mapdb_dict = build_mapdb(map_iterator(MAP_PATH), write_images=True)
    print "Loaded {0} maps".format(len(mapdb_dict))
    ftsindex_dict = create_ftsindex(mapdb_dict)
    write_json(mapdb_dict, path.join(BUILD_PATH, MAPDB_FILENAME), 'mapdb')
    write_json(ftsindex_dict, path.join(BUILD_PATH, FTSINDEX_FILENAME), 'ftsindex')
    write_json(CONFIG, path.join(BUILD_PATH, CONFIG_FILENAME), 'config')
    render_index(TEMPLATE_FILENAME, BUILD_PATH, conf=CONFIG, mapdb=mapdb_dict)
    transfer_static_files(STATIC_PATH, BUILD_PATH)
