#!/bin/sh

for image in $(find "$1" -type f)
do
    outimage="$(echo "$image" | sed -e s/\\.\\w\\+\$/.jpg/)"
    convert -quality 95 -adaptive-resize 200x200\> "$image" "$(echo "$image" | sed -e s/\\.\\w\\+\$/.jpg/)"
    # optimise jpeg
    jpegtran -outfile "$outimage" -o "$outimage"
    case "$image" in
        *.jpg) ;;
            *) rm "$image" ;;
    esac
done
