XonMaps: A simple Xonotic map repository
========================================

XonMaps aims to be an easy-to-use and lightweight map repository view for
Xonotic. It consists of a single Python script which scans the maps/ directory
and outputs static HTML, CSS and JavaScript files into the build/ directory,
including map metadata indices for quick client-side searching.

Just chmod +x ./buildall.sh; ./buildall.sh; $BROWSER build/index.html to get
started!
