/* client side logic for xonmaps */

var word_re;

function magnet_url(mapid) {
    var tracker_fragment = config['trackers'].map(function(t) {
        return encodeURIComponent(t);
    }).join('&tr=');
    return 'magnet:?xt=urn:sha1:' + mapdb[mapid]['sha1'] + '&as='
        + encodeURIComponent(direct_url(mapid)) + '&dn=' + encodeURIComponent(mapid)
        + '&xl=' + mapdb[mapid]['size'] + tracker_fragment;
}

function direct_url(mapid) {
    return config['map_download_root'] + encodeURIComponent(mapid);
}

function map_detail(ev) {
    var jqe = $(ev.currentTarget);
    var mapid = jqe.find(".rot").text();
    var mag = magnet_url(mapid);
    var dir = direct_url(mapid);
    var box = $("#downloadbox");
    box.find("h2").text(mapid);
    box.find(".magnet").attr('href', mag);
    box.find(".direct").attr('href', dir);
    $("#downloadbox, #shadow").fadeIn();
}

function snap_header() {
    var header = $("#header");
    if ($(document).scrollTop() > header.height()) header.addClass("floatinghead");
    else header.removeClass("floatinghead");
}

Array.prototype.deduplicate = function() {
    // in-place removal of duplicated elements
    var new_arr = [];
    while (this.length > 0) {
        var c = this.pop();
        if (this.indexOf(c) != -1) continue;
        new_arr.push(c);
    }
    while (new_arr.length > 0) {
        this.push(new_arr.pop());
    }
}

function Query(query) {
    this.query = query;
    var matches = query.match(word_re);
    /* yes, match() returns null when there are no matches instead of an empty
     * array. another minor JS WTF */
    if (matches == null) {
        this.words = [];
        this.gametypes = [];
        return;
    }
    var words = matches.map(function(e) {
        return e.toLowerCase();
    });
    // don't double up query parameters!
    words.deduplicate();
    function in_gt(e) {
        return config['gametypes'].indexOf(e) != -1;
    }
    this.gametypes = words.filter(in_gt);
    this.words = words.filter(function(e) {
        return !in_gt(e);
    });
}

function score_mapid(query_obj, mapid) {
    /* Provides a search score for the given mapid and given query as part of
     * the filtering process. FTS can be implemented more efficiently, but with
     * a small number of maps our bottleneck is the DOM rather than outright
     * search speed */
    var score = 0;
    if (query_obj.gametypes.length > 0 && mapdb[mapid]['gametype'] == undefined) {
        return 0;
    }
    // filter by gametype
    for (var i=0; i<query_obj.gametypes.length; i++) {
        if (mapdb[mapid]['gametype'].indexOf(query_obj.gametypes[i]) == -1) {
            return 0;
        }
        else if (query_obj.words.length == 0) {
            score += 1;
        }
    }
    for (var i1=0; i1<query_obj.words.length; i1++) {
        for (var i2=0; i2<config['fts_fields'].length; i2++) {
            var m1 = ftsindex[config['fts_fields'][i2][0]][query_obj.words[i1]];
            if (m1 == undefined) continue;
            var m2 = m1[mapid];
            if (m2 == undefined) continue;
            // m2 is num occurrences and config['fts_fields'][1] is the field weight
            score += m2 * config['fts_fields'][i2][1];
        }
    }
    return score;
}

function toggle_gametype_filter(ev) {
    ev.stopPropagation();
    var name = $(this).text();
    name = $.trim(name);
    var qe = $("#q");
    var q = new Query(qe.val());
    if (q.gametypes.indexOf(name) == -1) {
        // add the gametype to search
        if (q.query.length == 0) qe.val(name);
        else qe.val(q.query.replace(/\s*$/, ' ' + name));
    }
    else {
        // XXX: there should be a better method of doing this, but short of escaping
        // the string myself it would appear said method does not exist
        var name_re = RegExp("(\\W|^)+" + name + "((\\W|$)+)", "gi");
        qe.val(q.query.replace(name_re, "$1"));
    }
    return false; // stops link from activating
}

function on_search() {
    var q = new Query($(this).find("#q").val());
    $("#maps").hide(600, function() {
        var e = $("#maps tr");
        e.show();
        if (q.words.length > 0 || q.gametypes.length > 0) {
            e.filter(function() {
                var mid = $(this).find(".rot").text();
                var score = score_mapid(q, mid);
                return score_mapid(q, mid) <= 0;
            }).hide();
        }
        $("#maps").show(600);
    });
}

$(document).ready(function() {
    $("#searchbox").toggle(300);
    $("#shadow").click(function() {$("#shadow, #downloadbox").fadeOut();});
    $("#maps tr").click(map_detail);
    // we're showing the download box when the user clicks a #maps tr, so why
    // not give them a familiar hand?
    // for aesthetics only
    $("#maps tr .mapname, #maps tr .description, #maps tr .author").css(
        "cursor", "pointer");
    // make sure that the download box instead of the direct link shows when not
    // using js
    $("#maps th a").attr("href", "javascript:void(0);");
    $(document).scroll(snap_header);
    word_re = new RegExp(config['word_regex'], 'g');
    $(".gametypes .button").click(toggle_gametype_filter);
    $("#qf").submit(on_search);
    // in case we're part way down the page already and the search box is still
    // up the top!
    $(document).scroll();
});
