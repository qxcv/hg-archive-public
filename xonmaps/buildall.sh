#!/bin/sh

echo "Downloading maps into maps/ ..."
mkdir -p maps/
./downloadmaps.py maps/
echo "Building map DB ..."
mkdir -p build/thumbs/
./xonmaps.py
echo "Converting thumbs ..."
./convert_thumbs.sh build/thumbs/
echo "Finished. View rendered page with '$BROWSER build/index.html'"
