#!/usr/bin/python2

from math import exp
from operator import mul

MAX_P = 0.05

def fact(n):
    return reduce(mul, xrange(1, n+1), 1)

def p(x):
    return exp(-5) * pow(5, x) / fact(x)

if __name__ == '__main__':
    total_p = 0
    num_crashes = 0
    while total_p < 1 - MAX_P:
        total_p += p(num_crashes)
        num_crashes += 1
    print('There is at most a {0} chance it will crash >={1} times'
            .format(MAX_P, num_crashes))
