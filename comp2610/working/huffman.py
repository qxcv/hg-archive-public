"""This the file which done do them Huffman thang."""

from itertools import combinations

class HuffmanNode(object):
    left = None
    right = None
    value = None
    prob = 0

    def __init__(self, v, p, l = None, r = None):
        self.left = l
        self.right = r
        self.value = v
        self.prob = p

    def __cmp__(self, other):
        return cmp(self.prob, other)

def build_binomial_ensemble(n, p):
    alphabet = []
    # from https://stackoverflow.com/questions/1851134/generate-all-binary-strings-of-length-n-with-k-bits-set
    for n_zeros in range(n + 1):
        for positions in combinations(range(n), n_zeros):
            s = ['1'] * n
            for i in positions:
                s[i] = '0'
            alphabet += [''.join(s)]
    probabilities = [pow(p, s.count('0')) * pow(1 - p, s.count('1'))
            for s in alphabet]
    return (alphabet, probabilities)

def huffman_from_dist(alphabet, dist):
    # NOTE: This will break if we have a null alphabet
    # It may also break with a singleton alphabet
    trees = [HuffmanNode(a, p) for a, p in zip(alphabet, dist)]
    while len(trees) > 1:
        trees.sort()
        left = trees[0]
        right = trees[1]
        joined = HuffmanNode(None, left.prob + right.prob, left, right)
        trees = [joined] + trees[2:]
    return trees[0]

def huffman_to_dict(tree, accumulator=''):
    if tree.value is not None:
        return {tree.value: accumulator}
    rv = huffman_to_dict(tree.left, accumulator + '0')
    rv.update(huffman_to_dict(tree.right, accumulator + '1'))
    return rv

def dict_value_lengths(d):
    return sorted(len(k) for k in d.itervalues())

def expected_length(alphabet, dist, encoded):
    rv = 0
    for i, s in enumerate(alphabet):
        rv += dist[i] * len(encoded[s])
    return rv
