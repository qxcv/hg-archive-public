#!/usr/bin/python3

# NOTE: This file possibly has a bug; in lectures, they claimed that |s| can be
# greater than w, but here we are only allowed to repeat the contents of the
# window once.

try:
    range = xrange
except NameError:
    pass

try:
    input = raw_input
except NameError:
    pass

def find_offset(window, t, xn):
    w = len(window)
    for i in range(0, len(window)):
        if t == window[w - i - 1 : w - i - 1 + len(t)] + xn:
            return i
    raise Exception('t should be in the window, but it is not')

def lz77_encode(data, w=3):
    s = ''
    for index, xn in enumerate(data):
        # t is last w characters of s, plus xn
        t = s[max(len(s) - w, 0):] + xn
        window = data[max(index - w, 0) : index]
        if not t in window:
            if s == '':
                print('(0, {0})'.format(xn))
            else:
                i = find_offset(window, t, xn)
                print('(1, {0}, {1})'.format(i, len(s)))
                s = ''
        else:
            s += xn
        print("Xn, index: ", xn, index)
        print("S: ", s)
        print("T: ", t)
        print("Last W characters: ", window)
        print("===========")
    if s != '':
        print('Got some leftover data')
        i = find_offset(window, s, '')
        print('(1, {0}, {1})'.format(i, len(s)))

if __name__ == '__main__':
    lz77_encode(input(), 3)
