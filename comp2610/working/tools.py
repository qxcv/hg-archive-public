from math import log, ceil

def entropy(dist, do_sum_check=True):
    if do_sum_check and abs(sum(dist) - 1) > 0.0000001:
        raise Exception('Distribution does not add up to 1! Check it!')
    return sum(-p * log(p, 2) for p in dist)

def shannon_lengths(dist):
    # add 0.5 so that we don't accidentally round down
    # this hack won't work for negative numbers
    return [int(ceil(log(1.0 / p, 2)) + 0.5) for p in dist]

def bs_to_float(bs):
    rv = 0
    for i, b in enumerate(bs):
        if b == '1':
            rv += 2.0 ** (-i - 1)
    return rv

def mutual_information(source_dist, Q):
    # first, calculate H(Y)
    py = [0] * len(Q)
    for i in xrange(len(Q)):
        for j, px in enumerate(source_dist):
            py[i] += Q[i][j] * px
    hy = entropy(py)
    # now, calculate H(Y|X)
    hyx = 0
    for j, px in enumerate(source_dist):
        total = 0
        for i in xrange(len(Q)):
            pyx = Q[i][j]
            if pyx != 0:
                total += float(pyx) * log(1.0 / pyx, 2)
        hyx += px * total
    return hy - hyx
