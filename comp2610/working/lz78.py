#!/usr/bin/python3

try:
    input = raw_input
except NameError:
    pass

def lz78_encode(data):
    s = ''
    D = {'': 0}
    num_entries = 1
    for xn in data:
        sxn = s + xn
        if sxn in D:
            s = sxn
        else:
            i = D[s]
            print('({0}, {1})'.format(i, xn))
            D[sxn] = num_entries
            num_entries += 1
            s = ''
    if s != '':
        print("({0}, {1})".format(D[s[:-1]], s[-1]))

if __name__ == '__main__':
    lz78_encode(input())
