#include <stdio.h>
#include <string.h>

/* This incarnation of LZ77 is taken from:
 * http://oldwww.rasip.fer.hr/research/compress/algorithms/fund/lz/lz77.html
 * (approximately). */

int match_length(char *string, char *substring, int string_length, int substring_length) {
    int n = string_length < substring_length ? string_length : substring_length;
    for (int i = 0; i < n; i++)
        if (string[i] != substring[i])
            return i;
    // everything matches!
    return n;
}

void lz77_encode(char *data, int length, int w) {
    int n = 0;
    while (n < length) {
        // rifle through the window for the longest match to the lookahead
        // buffer
        int best_offset = -1, longest_match = 0;
        int window_length = n > w ? w : n;
        for (int i = 1; i <= window_length; i++) {
            int ml = match_length(&data[n - i], &data[n], i, length - n);
            if (ml > longest_match) {
                best_offset = i;
                longest_match = ml;
            }
        }
        if (longest_match > 0) {
            // output how far back the match is, how long the match is, and what
            // the next character in the lookahead buffer (after the match) is
            int end = n + longest_match;
            printf("%i %i %c", best_offset, longest_match, end >= length ?
                    '-' : data[end]);
            n += longest_match;
        } else {
            // just output this character, and some garbage for how far back the
            // match is and how long it is
            printf("- - %c", data[n]);
            n += 1;
        }
        printf("\n");
    }
}

int main(int argc, char **argv) {
    if (argc != 2) {
        fprintf(stderr, "USAGE: %s <string>", argv[0]);
        return -1;
    }
    lz77_encode(argv[1], strlen(argv[1]), 256);
}
