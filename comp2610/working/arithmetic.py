#!/usr/bin/python

from sys import argv, exit, stderr
from collections import defaultdict

"""An arithmetic encoder/decoder which works on bits. Adapted from the
(atrocious) Python code on Mackay's website."""

def dirichlet_prob(symbol, counts, fake_counts, alphabet):
    numer = counts[symbol] + fake_counts[symbol]
    denom = sum(counts[k] + fake_counts[k] for k in alphabet)
    return float(numer) / denom

def arithmetic_encode(data, fake_counts, alphabet):
    counts = defaultdict(lambda: 0)
    u = 0.0
    v = 1.0
    idxs = {sym: idx for idx, sym in enumerate(alphabet)}
    for c in data:
        probs = [dirichlet_prob(sym, counts, fake_counts, alphabet) for sym in
                alphabet]
        idx = idxs[c]
        lower_prob = sum(probs[:idx])
        upper_prob = lower_prob + dirichlet_prob(c, counts, fake_counts,
                alphabet)
        p = v - u
        v = u + p * upper_prob
        u = u + p * lower_prob
        counts[c] += 1
        print u, v
    return shortest_interval_inside(u, v)

def fractional_bits(bits):
    rv = 0.0
    i = 1
    for b in bits:
        rv += b * 0.5**i
        i += 1
    return rv

def interval(bits):
    num = int(bits if bits != '' else '0', 2)
    upper_num = num + 1
    lower_bound = num / (2.0 ** len(bits))
    upper_bound = upper_num / (2.0 ** len(bits))
    return lower_bound, upper_bound

def shortest_interval_inside(u, v):
    bits = ''
    lower, upper = interval(bits)
    while lower <= u or upper >= v:
        if lower - u > v - upper:
            bits += '0'
        else:
            bits += '1'
        lower, upper = interval(bits)
    return bits

def arithmetic_decode(data):
    pass

if __name__ == '__main__':
    if len(argv) != 2 or argv[1] not in ['encode', 'decode']:
        stderr.write('Must provide an argument "encode" or "decode"\n')
        exit(1)
    if argv[1] == 'encode':
        pass
    else:
        pass
