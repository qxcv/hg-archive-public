<%inherit file="_templates/site.mako" />
% if bf.config.blog.posts:
<h1>Latest Posts</h1>
% for post in bf.config.blog.posts[:5]:
    <%include file="_templates/post_excerpt.mako", args="post=post" />
% endfor
Look in the <a href="${bf.util.site_path_helper(bf.config.blog.path)}">archives</a> for more.<br/><br/>
% else:
<h2>I haven't written or done anything yet, but when I do it will come up here</h2>
% endif
