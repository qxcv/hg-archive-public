#!/usr/bin/python

import logging
from json import loads
from os import path
from subprocess import call
from threading import Lock
from time import time
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from tornado.web import RequestHandler, Application

# here is my Tornado listener for updating this repository
# it waits for a commit in the remote repository, and then BAM
# hg pull; blogofile build

LISTEN_URL = '/'
LISTEN_PORT = 6039
LISTEN_HOST = '0.0.0.0'
REPO_URL = '/qxcv/qxcv.net/'
LOCAL_REPOSITORY_PATH = './'
TRIGGER_PHRASE = 'REBUILD'
PULL_TIME = 10
DEBUG = False

last_pull = time()
last_pull_lock = Lock()

def trigger_phrase_in_commits(commits):
    """Scans a list of commits for TRIGGER_PHRASE"""
    for commit in commits:
        if TRIGGER_PHRASE in commit['message']:
            return True
    return False

class BBRemoteHandler(RequestHandler):
    def get(self):
        self.write(u'<html><head><title></title><meta charset="utf-8" /></head><body>')
        self.write(u"<h1>This isn't the URL you are looking for.</h1>")
        self.write(u'</body></html>')
        self.finish()

    def post(self):
        global last_pull
        payload = loads(self.get_argument('payload'))
        if (REPO_URL is not None and payload['repository']['absolute_url'] != REPO_URL) or (TRIGGER_PHRASE is not None and not trigger_phrase_in_commits(payload['commits'])):
            # use string.format to play nice with unicode
            logging.debug(u'Repo URL is incorrect ({0}) or trigger phrase not present ({0}). Will not pull or rebuild')
            self.write('Wrong repo name or trigger phrase absent')
            self.finish()
            return
        with last_pull_lock:
            if time() - last_pull < 10:
                logging.debug('Not pulling due to nospam')
                self.write('Please wait a few seconds between requests')
                self.finish()
                return
        # okay, looks legit now
        with last_pull_lock:
            last_pull = time()
        logging.debug('Starting pull from remote repository')
        local_repo_path = LOCAL_REPOSITORY_PATH
        if local_repo_path is None or not path.isabs(local_repo_path):
            local_repo_path = path.join(path.dirname(path.abspath(__file__)), local_repo_path)
        retcode = call(['hg', '-R', local_repo_path, 'pull'])
        retcode |= call(['hg', '-R', local_repo_path, 'update'])
        if retcode != 0:
            self.error('Mercurial failed with code %i' % retcode)
            self.write('Pull failed')
            self.finish()
            return
        logging.debug('Pull successful. Building with Blogofile')
        retcode = call(['blogofile', '-s', local_repo_path, 'build'])
        if retcode != 0:
            self.error('Blogofile build failed with code %i' % retcode)
            self.write('Build failed')
            self.finish()
            return
        logging.info('Build successful')
        self.write('Update success!')
        self.finish()

if __name__ == '__main__':
    app = Application(
            [
                (LISTEN_URL, BBRemoteHandler),
            ],
            debug=DEBUG,
        )
    logging.info('Starting HTTP server')
    httpd = HTTPServer(app)
    httpd.listen(LISTEN_PORT, LISTEN_HOST)
    IOLoop.instance().start()
    logging.info('HTTP daemon started on %s:%i' % (LISTEN_HOST, LISTEN_PORT))
