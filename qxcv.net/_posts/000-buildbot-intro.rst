---
title: My Blogofile buildbot
categories: blogofile, tornado, python
date: 2012/06/13 12:44:00
---

After setting up my new Blogofile-based website, I decided that it would be a
good idea to set up a buildbot which could watch this website's Bitbucket
repository for changes and do a ``hg pull; hg update; blogofile build`` if a
certain keyword is present.

Needless to say, I made the entire solution unnecessarily complicated for the
sake of learning a little bit of Tornado. Here's how I did it:

1. I configured Bitbucket's "service integration" feature (Repo page |rarr|
   Admin |rarr| Services |rarr| Select "POST" |rarr| Put in URL |rarr| Save settings)
   to send an HTTP POST request to my site whenever I make a commit.

#. I used Tornado to write a little listener daemon capable of handling
   Bitbucket's callbacks. This daemon can be found
   `here <https://bitbucket.org/qxcv/qxcv.net/src/79c4abbf6e46/_bb_listener.py>`_.

#. I twiddled my thumbs in anticipation of the sweet, sweet automated building
   which I would soon be privy to.

For future reference, here is the format in which Bitbucket will deliver the
information (note that this can  be accessed through the ``payload`` POST
argument):

.. code:: json

 {"canon_url": "https://bitbucket.org",
  "commits": [{"author": "Sam",
                "branch": "default",
                "files": [{"file": "README", "type": "modified"}],
                "message": "Okay, now we're in the \"shotgun debugging\" phase",
                "node": "a8eec885c27f",
                "parents": ["fe9f80b30a21"],
                "raw_author": "Sam <samATqxcvDOTnet>",
                "raw_node": "a8eec885c27f35485bf724c339cb4e4922c5ba33",
                "revision": 79,
                "size": -1,
                "timestamp": "2012-06-13 04:05:48",
                "utctimestamp": "2012-06-13 02:05:48+00:00"}],
  "repository": {"absolute_url": "/qxcv/qxcv.net/",
                  "fork": false,
                  "is_private": false,
                  "name": "qxcv.net",
                  "owner": "qxcv",
                  "scm": "hg",
                  "slug": "qxcv.net",
                  "website": "http://qxcv.net/"},
  "user": "qxcv"}

Another advantage of this buildbot is that it gives me an excuse to write a blog
post which will, in turn, test the buildbot.

Buildbot away!


.. |rarr| unicode:: U+2192
