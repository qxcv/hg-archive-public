---
title: Reverse Polish Notation calculator in one line of Python
categories: python, ohgodwhy
date: 2012/06/28 15:32:00
---

I can't believe I'm responsible for this *atrocity*. Here is an RPN calculator
in a single line of Python. 449 bytes, to be exact.

.. code:: python

   rpn=lambda c,s=False:reduce(lambda s,n:({'+':lambda s,n:s.append(s.pop()+s.pop()),'*':lambda s,n:s.append(s.pop()*s.pop()),'/':lambda s,n:s.append(s.pop()/float(s.pop())),'-':lambda s,n:s.append(s.pop()-s.pop()),'%':lambda s,n:s.append(s.pop()%s.pop()),'^':lambda s,n:s.append(s.pop()**s.pop()),'p':lambda s,n:__import__('sys').stdout.write(repr(s[-1])+'\n')}.get(n,lambda s,n:s.append(int(n)if n.isdigit()else float(n)))(s,n))or s,c.split(),s or[])

Usage
-----

Copy and paste the following into an interactive Python session, and voilà:

.. code:: python
   
   >>> rpn('2 3 +')
   [5]
   >>> stack = rpn('2 3 + p')
   5
   >>> stack
   [5]
   >>> rpn('2 ^', stack)
   [32]

Note that all instructions are whitespace delimited (fixing this is reasonably
trivial but would require a few more bytes). Available operators are:

`*`:code:
  Pop two values from the stack and push their product.

`/`:code:
  Pop two values from the stack and push the ratio between them.

`^`:code:
  Pop two values from the stack and push the first value to the second value.

`-`:code:
  Pop two values from the stack and push their difference.

`+`:code:
  Pop two values from the stack and push their sum.

`p`:code:
  Print the value on the top of the stack (or raise and exception if the stack
  is empty)

Naturally, there are a couple of subtle bugs here. See if you can find them ;)
