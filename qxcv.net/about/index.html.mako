<%inherit file="_templates/site.mako" />
<h1>About</h1>
<p>My name is Sam, and I'm a student from Brisbane, Australia. I like Linux, <a href="http://fsf.org">Free Software</a> and abuse, of commas.</p>
<p>I also have way too many projects, some of which are available on the Internet:</p>
<ul>
    <li><b>This site</b> is currently powered by Blogofile, and is available on <a href="http://bitbucket.org/qxcv/qxcv.net">Bitbucket</a>. I throw
        my code, posts and ideas in there - it's all good!
    </li>
    <li>Armed with knowledge from Sebastian Thrun's <a href="http://www.udacity.com/overview/Course/cs373">CS373 - Programming a Robotic Car</a>,
        I decided to start my own robotics project by writing an autopilot, with a plan to throw it in a COTS RC aircraft and make a fully functioning
        <b>UAV</b>. The code is located <a href="http://bitbucket.org/qxcv/uav-airborne">here (airborne code)</a>
    </li>
</ul>
<p>If you want to contact me, send an email to <a href="mailto:samATqxcvDOTnet">samATqxcvDOTnet</a> or try and find <tt>qxcv</tt> on <tt>irc.freenode.org</tt></p>
