#!/usr/bin/env python

# this is a program to encode arbitrary data as credit card numbers and then
# decode it again afterwards

# at the moment, the encoding is horrifyingly inefficient, but that could change
# in the future

from random import Random
from datetime import datetime
from sys import argv, exit, stdout

# the convention used for generated ccns is standard Visa:
#  - 6 digit IIN (always 401757)
#  - 9 digit account number
#  - 1 digit luhn code

# minimum and maximum number of months until the cards expire
MIN_EXPIRY_DISTANCE = 3
MAX_EXPIRY_DISTANCE = 18

IIN = 401757

def luhn_checksum(number):
    """Calculate the (single digit) Luhn checksum for the given number."""
    digits = [int(digit) for digit in repr(number)]
    # we sum every second element of each pair
    summed_digits = sum(digits[1::2])
    doubled = [2 * digit for digit in digits[0::2]]
    doubled_smaller = [(x // 10) + (x % 10) if x > 9 else x for x in doubled]
    summed_digits += sum(doubled_smaller)
    return summed_digits % 10

def cvv(ccn):
    """Calculate the CCV for a given credit card number. We don't have acccess
    to the bank's keys or methods, so we just use an (insecure) method whereby
    we use Python's random generator seeded with the CCN."""
    number_gen = Random(ccn)
    return number_gen.randint(100, 999)

def expiry(ccn):
    """Calculate an expiry date (in the form (month, year)) for the given CCN. This is just to make it look
    legitimate, and is not actually used in any calculations."""
    number_gen = Random(ccn)
    months_in_future = number_gen.randint(MIN_EXPIRY_DISTANCE, MAX_EXPIRY_DISTANCE)
    current_date = datetime.now()
    incr_month = current_date.month + months_in_future
    month = incr_month % 12 + 1
    year = current_date.year + incr_month // 12
    return (month, year)


def encode_single_ccn(data):
    """Take 29 arbitrary bits of data (as a number) and make a CCN out of it."""
    ccn = data * 10
    ccn += IIN * 10 ** 10
    ccn += luhn_checksum(ccn)
    ccn_str = repr(ccn).zfill(16)[:16]
    ccn_blocks = []
    for i in xrange(0, 13, 4):
        ccn_blocks.append(ccn_str[i:i+4])
    card_cvv = cvv(ccn)
    exp_month, exp_year = map(repr, expiry(ccn))
    return "{0} {1} {2}/{3}".format(
            '-'.join(ccn_blocks), 
            card_cvv,
            exp_month.zfill(2),
            exp_year.zfill(2))

def decode_single_ccn(ccn):
    """Takes a single CCN (in the form dddd-dddd-dddd-dddd) and returns a number
    of bytes corresponding to that CCN."""
    ccn_str = ''.join(ccn.split('-'))
    # take out checksum and IIN
    account_str = ccn_str[6:-1]
    account_number = int(account_str)
    num_bytes = account_number >> 24
    output = ''
    for i in xrange(num_bytes):
        output += chr(account_number & 0xff)
        account_number >>= 8
    return output

def ccn_encode_stream(input_stream):
    """Take a stream of input bytes (as a file) return a generator which will
    yield output CCNs"""
    block_size = 3 # 3 * 8 = 24, leaving 29 - 24 = 5 length bits
    while True:
        to_encode = input_stream.read(block_size)
        if not to_encode:
            # no more data
            break
        list_to_encode = map(ord, to_encode)
        encode_block_length = len(to_encode)
        number_to_encode = encode_block_length << (8 * block_size)
        for i in xrange(encode_block_length):
            number_to_encode |= list_to_encode[i] << (8 * i)
        encoded_ccn = encode_single_ccn(number_to_encode)
        yield encoded_ccn

def ccn_decode_stream(input_stream):
    """Take a stream of line-separated CCNs and return a generator which yields
    3-byte groups as each CCN is encoded."""
    while True:
        line = input_stream.readline()
        if not line:
            break
        # ignore blank lines
        if line.isspace():
            continue
        ccn = line.split()[0]
        yield decode_single_ccn(ccn)

def usage():
    print "USAGE: {0} encode|decode <filename>".format(argv[0])
    exit(1)

if __name__ == '__main__':
    if len(argv) != 3:
        usage()
    action = argv[1]
    if not action in ['encode', 'decode']:
        usage()
    filename = argv[2]
    try:
        fp = open(filename, 'r')
    except:
        print "File '{0}' does not exist!".format(filename)
        usage()
    if action == 'encode':
        for ccn in ccn_encode_stream(fp):
            print ccn
    else:
        for bytegroup in ccn_decode_stream(fp):
            stdout.write(bytegroup)
        stdout.flush()

