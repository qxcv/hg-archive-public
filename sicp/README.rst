.. vim: ft=rst
SICP Exercises
==============

This is my little repo where I like to work on exercises from `Structure and
Interpretation of Computer Programs <http://mitpress.mit.edu/sicp/>`_. I plan
to work through the book slowly and thoroughly, and these exercises are placed
online so that I can refer back to them in future. It's also handy for backup.

I'm using the wonderful `Dr. Racket <http://racket-lang.org/>`_ IDE in Scheme
R5RS mode, which I simply can't recommend enough as a learning tool.
