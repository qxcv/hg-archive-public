; ex 1.22
#lang racket
; find the smallest divisor of n
(define (smallest-divisor n)
  (find-divisor n 2)
  )

; find a divisor, starting at d
(define (find-divisor n d)
  (cond 
    ((> (* d d) n) n)
    ((divides? n d) d)
    (else (find-divisor n (+ d 1)))
    )
  )

; is n divisible by d?
(define (divides? n d)
  (= 0 (remainder n d))
  )

(define (prime? n)
  (= n (smallest-divisor n)))

(define (timed-prime-test n)
  (newline)
  (display n)
  (start-prime-test n (current-milliseconds)))

(define (report-time ms)
  (display "\n***\n")
  (display (/ ms 1000)))

(define (start-prime-test n start-time)
  (cond 
    ((prime? n)
      (report-time (- (current-milliseconds) start-time)))
    (else (display " "))))

(define (smallest-prime lower)
  (cond
    ((= 0 (remainder lower 2)) (smallest-prime (+ lower 1)))
    ((timed-prime-test lower) lower)
    (else (smallest-prime (+ lower 2)))))

(smallest-prime 100)
(smallest-prime 1000)
(smallest-prime 100000)
(smallest-prime 1000000)

(define (lots-o-primes n)
  (cond
    ((> 1 n) (timed-prime-test (exp 10 n)) (lots-o-primes (- n 1)))
))

(lots-o-primes 10)