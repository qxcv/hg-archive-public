;; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-advanced-reader.ss" "lang")((modname ex1.21) (read-case-sensitive #t) (teachpacks ()) (htdp-settings #(#t constructor repeating-decimal #t #t none #f ())))
; find the smallest divisor of n
(define (smallest-divisor n)
  (find-divisor n 2)
  )

; find a divisor, starting at d
(define (find-divisor n d)
  (cond 
    ((> (* d d) n) n)
    ((divides? n d) d)
    (else (find-divisor n (+ d 1)))
    )
  )

; is n divisible by d?
(define (divides? n d)
  (= 0 (remainder n d))
  )

(smallest-divisor 199)
(smallest-divisor 1999)
(smallest-divisor 19999)