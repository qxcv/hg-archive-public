﻿' This module is for the various "things" which can appear in a 3D scene:
' objects, lights, rays, etc.
Public Module SceneObjects
    ' Represents a line in 3D space which starts at some point and extends on
    ' to infinity
    Structure Ray
        Dim Root As Vec3
        Dim Direction As Vec3

        Sub New(ByVal Origin As Vec3, ByVal NewDirection As Vec3)
            Root = Origin
            Direction = NewDirection.Normalised()
        End Sub
    End Structure

    ' A point light
    Structure Light
        Dim Location As Vec3
        Dim Intensity As Double

        Sub New(ByVal NewLocation As Vec3, ByVal NewIntensity As Double)
            Location = NewLocation
            Intensity = NewIntensity
        End Sub
    End Structure

    ' Stores colour data for objects in the scene
    Structure Material
        ' Highlight colour
        Dim Specular As Vec3
        ' Colour of light reflected from point lights
        Dim Diffuse As Vec3
        ' Self-emitted colour
        Dim Emission As Vec3
        ' How shiny is this object?
        Dim Shininess As Double

        Sub New(ByVal NewSpecular As Vec3, ByVal NewDiffuse As Vec3, ByVal NewEmission As Vec3, ByVal NewShininess As Double)
            Specular = NewSpecular
            Diffuse = NewDiffuse
            Emission = NewEmission
            Shininess = NewShininess
        End Sub
    End Structure

    ' ABC for objects which can be rendered
    Interface RenderPrimitive
        Property ObjMaterial As Material
        Function Intersect(ByRef Intersector As Ray) As Double?
        Function SurfaceNormal(ByRef Intersection As Vec3) As Vec3
    End Interface

    ' Implementation of the above ABC which represents a sphere in 3D space
    Class Sphere
        Implements RenderPrimitive

        Dim Centre As Vec3
        Dim Radius As Double
        Dim ObjMat As Material

        ' This is a property so that we can satisfy the ABC requirements
        Public Property ObjMaterial As Material Implements RenderPrimitive.ObjMaterial
            Get
                Return ObjMat
            End Get
            Set(ByVal value As Material)
                ObjMat = value
            End Set
        End Property

        Sub New(ByVal NewMat As Material, ByVal NewCentre As Vec3, ByVal NewRadius As Double)
            ObjMat = NewMat
            Centre = NewCentre
            Radius = NewRadius
        End Sub

        ' This sphere intersection function simply solves the quadratic formula
        ' determine whether the intersector intersects 0, 1 or 2 times with the
        ' sphere
        Function Intersect(ByRef R As Ray) As Double? Implements RenderPrimitive.Intersect
            Dim NewOrig As Vec3 = R.Root - Centre
            ' a, b and c are the coefficients of the quadratic equation which we need to solve
            Dim a As Double = R.Direction.Dot(R.Direction)
            Dim b As Double = 2 * NewOrig.Dot(R.Direction)
            Dim c As Double = NewOrig.Dot(NewOrig) - Radius ^ 2
            Dim SqrtInner As Double = b ^ 2 - 4 * a * c
            If SqrtInner < 0 Then
                ' No intersection (quadratic formula result has imaginary part)
                Return Nothing
            End If
            Dim FirstTerm As Double = -b
            If SqrtInner = 0 Then
                ' We intersect with the edge of the sphere
                Return FirstTerm / (2 * a)
            End If
            Dim SqrtOuter = Math.Sqrt(SqrtInner)
            If FirstTerm > SqrtOuter Then
                ' Two collisions: pick the closest
                Return (FirstTerm - SqrtOuter) / (2 * a)
            End If
            If FirstTerm + SqrtOuter > 0 Then
                ' We are inside the sphere
                Return (FirstTerm + SqrtOuter) / (2 * a)
            End If
            ' We didn't hit
            Return Nothing
        End Function

        ' Get a vector perpendicular to the surface at some point
        Function SurfaceNormal(ByRef Intersection As Vec3) As Vec3 Implements RenderPrimitive.SurfaceNormal
            Return (Intersection - Centre).Normalised()
        End Function
    End Class
End Module
