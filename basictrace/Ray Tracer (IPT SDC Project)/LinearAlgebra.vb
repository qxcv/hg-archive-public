﻿' Module for all vector mathematics. This is used heavily in
' SceneObjects.vb and Scene.vb
Public Module LinearAlgebra
    ' Structure representing a basic three dimensional vector
    Structure Vec3
        Public ReadOnly x, y, z As Double

        ' Constructor from values (allows New Vec3(1, 2, 3))
        Sub New(ByVal newX As Double, ByVal newY As Double, ByVal newZ As Double)
            x = newX
            y = newY
            z = newZ
        End Sub

        ' Constructor from another vector (allows New Vec(oldvec))
        Sub New(ByVal Other As Vec3)
            x = Other.x
            y = Other.y
            z = Other.z
        End Sub

        ' Vector addition
        Shared Operator +(ByVal This As Vec3, ByVal OtherVec As Vec3) As Vec3
            Return New Vec3(This.x + OtherVec.x, This.y + OtherVec.y, This.z + OtherVec.z)
        End Operator

        ' Vector subtraction
        Shared Operator -(ByVal This As Vec3, ByVal OtherVec As Vec3) As Vec3
            Return New Vec3(This.x - OtherVec.x, This.y - OtherVec.y, This.z - OtherVec.z)
        End Operator

        ' Scalar multiplication
        Shared Operator *(ByVal This As Vec3, ByVal Scalar As Double) As Vec3
            Return New Vec3(This.x * Scalar, This.y * Scalar, This.z * Scalar)
        End Operator

        ' Sclar multiplication (reverse order)
        Shared Operator *(ByVal Scalar As Double, ByVal This As Vec3) As Vec3
            Return This * Scalar
        End Operator

        ' Elementwise vector multiplication
        Shared Operator *(ByVal This As Vec3, ByVal Other As Vec3) As Vec3
            Return New Vec3(This.x * Other.x, This.y * Other.y, This.z * Other.z)
        End Operator

        ' Scalar division
        Shared Operator /(ByVal This As Vec3, ByVal Scalar As Double) As Vec3
            Return New Vec3(This.x / Scalar, This.y / Scalar, This.z / Scalar)
        End Operator

        ' Vector dot product
        Function Dot(ByVal Other As Vec3) As Double
            Return x * Other.x + y * Other.y + z * Other.z
        End Function

        ' Vector cross product
        Function Cross(ByVal Other As Vec3) As Vec3
            Return New Vec3(Me.y * Other.z - Other.y * Me.z,
                            Other.x * Me.z - Me.x * Other.z,
                            Me.x * Other.y - Other.x * Me.y)
        End Function

        ' Vector magnitude
        Function Magnitude() As Double
            Return Math.Sqrt(x ^ 2 + y ^ 2 + z ^ 2)
        End Function

        ' Returns current vector "normalised" to have a magnitude of 1
        Function Normalised() As Vec3
            Dim m = Magnitude()
            If m = 0 Then
                Throw New Exception("Could not normalise vector, magnitude is zero")
            End If
            Return New Vec3(1 / m * Me)
        End Function
    End Structure
End Module
