﻿Imports System
Imports NUnit.Framework

' These are the NUnit tests for BasicTrace
' They require NUnit to run, and NUnit can be had from http://www.nunit.org/index.php?p=download
' Most of these are uncommented because they are self-explanatory. Note that this file is for
' development purposes only, and is NOT required for release builds.

' This class tests the code in LinearAlgebra.vb
<TestFixture()>
Public Class VectorTest
    <Test()>
    Public Sub TestAddition()
        Assert.IsTrue(ApproxEqVec(New Vec3(-1.5, 2 / 3, 0) + New Vec3(-101002, 980213.1293, 9012), New Vec3(-101003.5, 980213.79596666, 9012)))
        Assert.IsTrue(ApproxEqVec(New Vec3(0, 0, 0) + New Vec3(0, 0, 0), New Vec3(0, 0, 0)))
    End Sub

    <Test()>
    Public Sub TestSubtraction()
        Assert.IsTrue(ApproxEqVec(New Vec3(-1.5, 2 / 3, 0) - New Vec3(-101002, 980213.1293, 9012), New Vec3(101000.5, -980212.46263333, -9012)))
        Assert.IsTrue(ApproxEqVec(New Vec3(0, 0, 0) - New Vec3(0, 0, 0), New Vec3(0, 0, 0)))
    End Sub

    <Test()>
    Public Sub TestScalarMultiplication()
        Dim Ident As New Vec3(1, 1, 1)
        Dim Zero As New Vec3(0, 0, 0)
        Dim Test1 As New Vec3(-1293.321, 192, 1)
        Dim Test2 As New Vec3(9, 91203.0001, 9123)
        Dim Result As New Vec3(-11639.889, 17510976.0192, 9123)
        Assert.IsTrue(ApproxEqVec(Ident * Test1, Test1))
        Assert.IsTrue(ApproxEqVec(Ident * Test2, Test2))
        Assert.IsTrue(ApproxEqVec(Zero * Test1, Zero))
        Assert.IsTrue(ApproxEqVec(Zero * Test1, Zero))
        Assert.IsTrue(ApproxEqVec(Test1 * Test2, Result))
    End Sub

    <Test()>
    Public Sub TestScalarDivision()
        Dim Test1 As New Vec3(-1293.321, 192, 1)
        Dim Test2 As New Vec3(9, 91203.0001, 9123)
        Dim Result1 As New Vec3(208.728, 2115179.9783192, 211580.616)
        Assert.IsTrue(ApproxEqVec(Test1 / -1, Test1 * -1))
        Assert.IsTrue(ApproxEqVec(Test2 * 23.192, Result1))
    End Sub

    <Test()>
    Public Sub TestVectorMultiplication()
        Dim Zero As New Vec3(0, 0, 0)
        Dim One As New Vec3(1, 1, 1)
        Dim Test1 As New Vec3(592.5955571478811, -720.91451663227747, 302.46742707050879)
        Dim Test2 As New Vec3(863.37698921426977, 176.5265614415683, 488.66545300955312)
        Dim Result1 As New Vec3(511633.36795209034, -127260.56071440624, 147805.38227004415)
        Assert.IsTrue(ApproxEqVec(Test1 * Zero, Zero))
        Assert.IsTrue(ApproxEqVec(Test1 * One, Test1))
        Assert.IsTrue(ApproxEqVec(Test1 * Test2, Result1))
    End Sub

    <Test()>
    Public Sub TestDotProduct()
        Dim Zero As New Vec3(0, 0, 0)
        Dim One As New Vec3(1, 1, 1)
        Dim Test1 As New Vec3(-1, 1, 2)
        Dim Test2 As New Vec3(0, 1, -2)
        Dim Test3 As New Vec3(-622.83953529673261, 584.98132172434828, 555.186983681227)
        Dim Test4 As New Vec3(388.992839139191, 22.724967202148562, 528.66673817013771)
        Assert.IsTrue(ApproxEq(Test1.Dot(Zero), 0))
        Assert.IsTrue(ApproxEq(Test2.Dot(Zero), 0))
        Assert.IsTrue(ApproxEq(Test3.Dot(Zero), 0))
        Assert.IsTrue(ApproxEq(Test4.Dot(Zero), 0))
        Assert.IsTrue(ApproxEq(Test1.Dot(One), 2))
        Assert.IsTrue(ApproxEq(Test2.Dot(One), -1))
        Assert.IsTrue(ApproxEq(Test1.Dot(Test2), -3))
        Assert.IsTrue(ApproxEq(Test1.Dot(Test2), Test2.Dot(Test1)))
        Assert.IsTrue(ApproxEq(Test3.Dot(Test4), 64522.4539241))
        Assert.IsTrue(ApproxEq(Test3.Dot(Test4), Test4.Dot(Test3)))
    End Sub

    <Test()>
    Public Sub TestCrossProduct()
        Dim Test1 As New Vec3(1, 0, 0)
        Dim Test2 As New Vec3(0, -1, 0)
        Dim Result1 As New Vec3(0, 0, -1)
        Assert.IsTrue(ApproxEqVec(Test1.Cross(Test2), Result1))
        Dim Test3 As New Vec3(1, 0, 0)
        Dim Test4 As New Vec3(Math.Cos(60), Math.Sin(60), 0)
        Dim Result2 As New Vec3(0, 0, Math.Sin(60))
        Assert.IsTrue(ApproxEqVec(Test3.Cross(Test4), Result2))
        Dim Test5 As New Vec3(-811.43716516203267, 36.648782131428746, -348.6093879401937)
        Dim Test6 As New Vec3(-319.91924176140009, 41.252507432794296, -539.87995198591079)
        Dim Result3 As New Vec3(-5404.9313703131456, -326551.80670652923, -21749.167096126854)
        Assert.IsTrue(ApproxEqVec(Test5.Cross(Test6), Result3))
    End Sub

    <Test()>
    Public Sub TestMagnitude()
        Dim Test1 As New Vec3(1, 0, 0)
        Dim Test2 As New Vec3(1, -2, 3)
        Dim Test3 As New Vec3(0, 0, 0)
        Assert.IsTrue(ApproxEq(Test1.Magnitude(), 1))
        Assert.IsTrue(ApproxEq(Test2.Magnitude(), 3.741657387))
        Assert.IsTrue(ApproxEq(Test3.Magnitude(), 0))
    End Sub

    <Test()>
    Public Sub TestNormalised()
        Dim Test1 As New Vec3(1230, -12939123.129312, 0.000112)
        Assert.IsTrue(ApproxEq(1, Test1.Normalised().Magnitude()))
    End Sub
End Class

' This class tests the code in Camera.vb
<TestFixture()>
Public Class CameraTest
    <Test()>
    Public Sub TestRayThroughCamera()
        Dim OriginRay = New Ray(New Vec3(0, 0, 0), New Vec3(1, 0, 0))
        Dim OffsetRay = New Ray(New Vec3(1, -1, 0.5), New Vec3(1, 0, 0))
        Dim C1 As New Camera(90, 1, OriginRay)
        Dim C2 As New Camera(90, 16.0 / 9.0, OffsetRay)
        ' Shooting through the centre of the camera should return the camera ray
        Assert.IsTrue(ApproxEqRay(C1.GetCameraRay(0, 0), OriginRay))
        Assert.IsTrue(ApproxEqRay(C2.GetCameraRay(0, 0), OffsetRay))
    End Sub
End Class

' This class tests the code in SceneObjects.vb
<TestFixture()>
Public Class SphereTest
    Function DistanceEq(ByVal Distance As Double, ByVal Sphere As Sphere, ByVal ROrigin As Vec3, ByVal RDist As Vec3) As Boolean
        Return ApproxEq(Distance, Sphere.Intersect(New Ray(ROrigin, RDist)))
    End Function

    <Test()>
    Public Sub TestSphereIntersection()
        Dim M As New Material() ' we don't need this to be correctly initialised, it's never used
        Dim S1 As New Sphere(M, New Vec3(0, 0, 0), 1)
        Assert.IsTrue(DistanceEq(1, S1, New Vec3(-1, 0, 1), New Vec3(1, 0, 0)))
        Assert.IsTrue(DistanceEq(1, S1, New Vec3(0, 0, 0), New Vec3(91, -12, 13)))
        Assert.IsTrue(DistanceEq(0.5, S1, New Vec3(-1.5, 0, 0), New Vec3(1, 0, 0)))
    End Sub

    <Test()>
    Public Sub TestSphereNormal()
        Dim M As New Material()
        Dim S1 As New Sphere(M, New Vec3(0, 0, 0), 1)
        Dim S2 As New Sphere(M, New Vec3(1, -1, 1), 0.5)
        Assert.IsTrue(ApproxEqVec(New Vec3(1, 0, 0), S1.SurfaceNormal(New Vec3(1, 0, 0))))
        Assert.IsTrue(ApproxEqVec(New Vec3(1, 0, 0), S2.SurfaceNormal(New Vec3(1.5, -1, 1))))
    End Sub
End Class