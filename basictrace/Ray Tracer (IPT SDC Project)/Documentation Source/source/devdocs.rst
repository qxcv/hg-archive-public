Extending BasicTrace
====================

One of the main goals of BasicTrace is to be a simple platform for experimenting
with and understanding ray tracers. As such, significant effort has been
expended to make BasicTrace's code base as hackable as possible, and this
document should serve as a worthy companion when exploring BasicTrace's source
code. If you want a good overview of the computer graphics theory behind ray
tracers, it's worth checking out `Ravi Ramamoorthi's
<http://www.cs.berkeley.edu/~ravir/>`_ free CS184X course `at EdX
<https://www.edx.org/course/uc-berkeley/cs184-1x/foundations-computer-graphics/576>`_.
Note that this document is written for programmers, and so assumes familiarity
with basic programming techniques and jargon.

The 10,000ft View
-----------------

BasicTrace is divided into a number of files, which each serve a different
purpose:

- ``MainForm.vb``: This is where all of the GUI handling code goes. Outside of
  ``MainForm.vb``, there are no WinForms procedure calls *at all*, so if you
  want to work on the GUI, this is the file for you.
- ``Renderer.vb``: Glue code to join the renderer logic (in ``Scene.vb``) to the
  main form. The code in ``Renderer.vb`` runs in a background thread and sends
  rendered pixels back to ``MainForm.vb`` using a queue, which is periodically
  emptied using a timer event.
- ``Scene.vb``: This is where the logic for ray tracing goes. The most important
  part of ``Scene.vb`` is the ``Trace()`` method, which shoots a ray into the
  scene and returns the colour of the ray.
- ``Camera.vb``: Logic for the scene's "camera". Cameras are responsible for
  converting pixel locations on the virtual screen into "rays" which can be
  traced by the **ray** tracer.
- ``SceneObjects``: Where all of the things like material and object structure
  definitions go. This also contains intersection functions for scene objects,
  which are called by ``Scene.vb`` when rendering.
- ``Util.vb``: Utility functions which don't really fit anywhere else. Things
  like vector and float comparison, colour conversion, etc. end up in here.
- ``UnitTests.vb``: Unit tests for BasicTrace. Unit testing allows the
  programmer to specify expected behaviour for certain software components (for
  instance, classes or functions) and automatically execute code which checks
  whether the software components conform to the expected behaviour.

``AddHandler RenderButton.Click, AddressOf ThisSection``
--------------------------------------------------------

So, what happens when you click the render button? First, WinForms calls the
``RenderButton_Click()`` method in ``MainForm.vb``, which does a couple of
things:

#. Instantiates the render canvas on the main form using the supplied
   resolution.
#. Creates a new draw queue (``DrawQueue``) so that it can be notified of
   updates sent by the renderer background thread.
#. Sets a timer to periodically call a function (``WhileRenderingTick()``) which
   empties the draw queue and renders the data in the draw queue to the screen.
#. Makes a ``Renderer`` instance (defined in ``Renderer.vb``) using the supplied
   scene and image resolution, as well as the draw queue it just created.
#. Finally, it starts a thread so that ``Renderer.Run()`` can be executed in the
   background.

Now all of the work is being done by ``Renderer.Run()`` in ``Renderer.vb``.
``Run()`` starts by creating a new ``Scene`` object (defined in ``Scene.vb``).
The ``Scene`` object's constructor sets various local variables and parses the
scene description string---as read from a file by ``MainForm.vb``---in its
``Parse()`` method. Once the scene has been parsed, ``Run()`` then splits the
image into a number of chunks, and renders each chunk using a different worker
thread. After a pixel has been rendered, ``Run()`` puts it on the draw queue,
where it is later emptied by ``WhileRenderingTick()`` and rendered.

The ray tracing itself, however, is performed by ``Scene.Trace()`` (defined in
``Scene.vb``). ``Trace()`` takes an instance of the ``Ray`` class (defined in
``SceneObjects.vb``) and "shoots" it into the scene. If the ray intersects with
an object, the colour of the object at the intersection location is calculated
and combined with the colour of a "reflected" ray fired recursively from the
intersection location back out into the scene. The combined value (hit location
colour plus colour of reflected ray) is then returned by ``Trace()``. On the
other hand, if the ray does not intersect with an object, ``Trace()`` simply
returns the background colour of the scene. Note that the specific mechanism by
which ``Trace()`` works is very difficult to explain in a small document like
this one, and so it is recommended that interested readers investigate `Ravi
Ramamoorthi's <http://www.cs.berkeley.edu/~ravir/>`_ aforementioned CS184X
course `at EdX
<https://www.edx.org/course/uc-berkeley/cs184-1x/foundations-computer-graphics/576>`_.



Once rendering has finished, ``Renderer.Run()`` exits, killing the background
thread and alerting ``MainForm.vb`` that the render has finished. The user is
then given a "render finished" message and the option to save their finished
render.
