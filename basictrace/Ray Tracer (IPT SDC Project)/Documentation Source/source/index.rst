.. BasicTrace documentation master file, created by
   sphinx-quickstart on Mon Aug 12 21:00:34 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to BasicTrace's documentation!
======================================

BasicTrace is a ray tracer written in Visual Basic .NET. Ray tracers are
programs responsible for turning abstract descriptions of 3D scenes, like the
following::

  camera 0 0 0 1 0 0 90
  background 0 0 0 # black background
  ambient 0 0 0 # no ambient emission
  emission 0 0 0 # no emission
  shininess 100 # lots of shininess
  specular 0.5 0.5 0.5 # tint reflections white
  diffuse 0 1 0 # green
  sphere 1.5 -0.75 0 0.5
  diffuse 1 0 0 # red
  sphere 1.5 0.75 0 0.5
  diffuse 0 0 1 # blue
  sphere 2.8 0 0 0.5
  light 2.15 0 1 5

Into rendered images:

.. image:: ../images/three-sphere-render-256x256.jpg

This documentation serves to guide users through the functionality offered by
BasicTrace, and give a brief overview of BasicTrace's code organisation.

.. toctree::
   :maxdepth: 2

   using
   fileformat
   devdocs

