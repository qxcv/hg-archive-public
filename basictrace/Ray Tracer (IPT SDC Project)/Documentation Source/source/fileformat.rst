Your First BasicTrace Scene
===========================

In order to fully utilise BasicTrace, an understanding of its scene file format
is required. This section gives an overview of the BasicTrace file format, and
ends with instructions for making a simple scene to test BasicTrace's rendering
capabilities.

The ``.scn`` File Format
------------------------

BasicTrace uses the ``.scn`` file format to store scene descriptions. ``.scn``
files are simple text files which may be edited in Notepad, and are parsed by
BasicTrace on a line-by-line basis. Each line can either be blank (in which case
it is ignored) or contain a command name followed by zero or more decimal number
"arguments". The number of arguments required is decided by the command in
question. For instance, the ``sphere`` command has four arguments: location on
the X axis, location on the Y axis, location on the Z axis and radius, so the
following line creates a sphere at the origin with radius 0.5::

 sphere 0 0 0 0.5

The ``.scn`` format also has provisions for line commenting, so if a hash symbol
(``#``) is encountered, the remainder of the line will be ignored. Thus, the
following are valid lines from a scene description file::

  # This line will be treated as a blank line
   # So will this one, since it only consists of a space followed by a comment
  sphere 0 0 0 1 # This line will create a unit sphere at the origin

File Format Commands
--------------------

``camera <xloc> <yloc> <zloc> <xdir> <ydir> <zdir> <fov>``
  This command specifies the location, direction and field-of-view of the
  scene's camera. The first three parameters---``xloc``, ``yloc`` and
  ``zloc``---specify the location of the camera on the X, Y and Z axes,
  respectively. The next three parameters---``xdir``, ``ydir`` and
  ``zdir``---are the components of a vector describing the direction in which
  the camera is aimed. For instance, ``1 0 0`` would aim the camera directly
  down the X axis.  ``fov`` is the Field of View (FOV) value in degrees. FOV is
  the angle between the leftmost object visible in the scene and the rightmost
  object visible in the scene. Video games use FOVs of between 70 and 110
  degrees, whilst humans have a FOV of up to 190 degrees.

``background <r> <g> <b>``
  Sets the colour of the scene background using red, green and blue channels.
  The background colour of the scene is displayed when a ray does not intersect
  any object in the scene.

``light <xloc> <yloc> <zloc> <intensity>``
  Creates a new light at the location (``xloc``, ``yloc``, ``zloc``) with an
  intensity of ``intensity``. Lights in BasicTrace are represented by points in
  3D space, and affect surrounding objects based on their distance from the
  object and their intensity, with a greater intensity making the surrounding
  objects brighter. Colour is not specified, since the only possible light
  colour in BasicTrace is white.

``sphere <xloc> <yloc> <zloc> <radius>``
  Creates a sphere at the coordinates ``(xloc, yloc, zloc)`` and with a radius
  of ``radius``. Material parameters for the sphere are set earlier in the scene
  file using the ``diffuse``, ``emission``, ``shininess`` and ``specular``
  properties, described below. Note that the material parameter commands must
  come BEFORE the sphere command for them to be applied to the sphere.

``diffuse <r> <g> <b>``
  Sets the current diffuse colour in Red-Green-Blue (RGB) format. Diffuse colour
  is the shade of light reflected from an object when photons from a light
  bounce off it (except for when the interaction is specular, as explained
  below). For instance, an object with a diffuse colour of green will appear
  green when exposed to white light.

``emission <r> <g> <b>``
  Sets the emission colour---or "glow"---of an object. It is not dependent on
  the presence of point lights, but is still affected by attenuation. Note that
  light from emission is not currently reflected on neighbouring objects, so
  emission colour will only illuminate the object to which it is applied.

``specular <r> <g> <b>``
  Sets the current specular colour. Specular colour is the colour of the
  highlights which appear on an object when exposed to a bright light. For
  example, holding a shiny sphere in direct light will illuminate the sphere,
  but also result in a patch of very bright light on the sphere's surface. The
  colour of this patch is referred to as "specular" colour.

``shininess <s>``
  Sets the current shininess constant, ``s``, for a material. A greater shininess
  constant means that more light is reflected by an object, whilst a lower
  shininess means that less light is reflected.

``ambient <r> <g> <b>``
  Ambient light is the default colour of any object in the scene, regardless of
  whether it is lit by a light or not. Ambient colour does not fade with
  distance, so scenes which only use ambient lighting make it impossible to
  tell how far away an object is. Ambient colour is useful for "silhouetting" objects
  in a dark scene, so that they can still be distinguished from the background.
  It should be reiterated that, unlike other material parameters, the
  ``ambient`` command sets the ambient colour for ALL objects in the scene, not
  just subsequent objects.

A Simple Scene
--------------

Armed with an understanding of BasicTrace's file format and various commands, it
is trivial to start writing scenes of your own. In the following scene, three
equidistant spheres are placed against a black background with a light above
them, demonstrating the various kinds of light interactions possible with a ray
tracer.

First, we need a camera. We'll place our camera at the origin, and have it face
down the X axis (where our three spheres will be). The field-of-view will be 90
degrees, to ensure that all spheres are visible::

  camera 0 0 0 1 0 0 90

Now we can set the parameters which will affect all the objects::

  background 0 0 0 # black background
  ambient 0 0 0 # no ambient emission
  emission 0 0 0 # no emission
  shininess 100 # lots of shininess
  specular 0.5 0.5 0.5 # tint reflections white
   
We'll have three spheres in our scene, the first being the left sphere, which
will be coloured green::

  diffuse 0 1 0 # green
  sphere 1.5 -0.75 0 0.5

The right one will be coloured red::

  diffuse 1 0 0 # red
  sphere 1.5 0.75 0 0.5

Finally, the back sphere will be blue::

  diffuse 0 0 1 # blue
  sphere 2.8 0 0 0.5

In order to illuminate the scene, we just need to add a single light::

  light 2.15 0 1 5

If all went to plan, you'll have a file like the following which you can load
into BasicTrace and render::

  camera 0 0 0 1 0 0 90
  background 0 0 0 # black background
  ambient 0 0 0 # no ambient emission
  emission 0 0 0 # no emission
  shininess 100 # lots of shininess
  specular 0.5 0.5 0.5 # tint reflections white
  diffuse 0 1 0 # green
  sphere 1.5 -0.75 0 0.5
  diffuse 1 0 0 # red
  sphere 1.5 0.75 0 0.5
  diffuse 0 0 1 # blue
  sphere 2.8 0 0 0.5
  light 2.15 0 1 5

Which should produce the following image:

.. we downscale here to produce a nicer looking image

.. image:: ../images/three-sphere-render-256x256.jpg

Note that this scene is included in the BasicTrace distribution as
``triple-sphere-reflections.scn``. Try toying with the various parameters listed
in here to gain a greater understanding of what each one does.
