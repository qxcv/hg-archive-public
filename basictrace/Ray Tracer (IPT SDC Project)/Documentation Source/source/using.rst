Basic Rendering with BasicTrace
===============================

This section will guide you through the user interface of BasicTrace and
introduce some of the concepts used throughout the program. By the end of this
section, you will be able to load and render a supplied scene in BasicTrace,
and understand the various render options BasicTrace gives.

The Main Window
---------------

Upon starting BasicTrace, you'll be greeted with the following window:

.. image:: ../images/main-before-render.png

From top to bottom, the main window is split into the following regions.

Menu Bar
++++++++

.. image:: ../images/filehelp.png

The menu bar is split into two drop-down menu items:

**File**
  Provides options for loading scenes and saving renderings. Note that the "Save
  render" button is enabled once a render has finished.
  
  .. image:: ../images/filemenu.png

**Help**
  Home to the help system and about box.
  
  .. image:: ../images/helpmenu.png

Render Preview
++++++++++++++

.. image:: ../images/preview-before-render.png

When no rendering has taken place, the render preview pane will simply display a
help string instructing you to load a scene. When rendering is taking place,
however, the render preview window will display the rendered data as it becomes
available, as in the following image of BasicTrace rendering a spiral of spheres:

.. image:: ../images/preview-rendering.png

The above illustration is a good example of the horizontal barring effect which
occurs as BasicTrace splits render work across multiple CPU cores, with each
core choosing to render a certain section of the image in parallel.

Configuration
+++++++++++++

.. image:: ../images/config-before-render.png

The configuration area allows you to specify a scene path, change the image
resolution and finally render the scene. Note that the "Save Render" button is
initially disabled, but will be automatically enabled once rendering has
finished. Likewise, the "Render" button will be replaced by a "Cancel" button
once rendering starts. Clicking the "Cancel" button at any point during
rendering will cause rendering to terminate immediately.

Status Bar
++++++++++

.. image:: ../images/statusbar-before-render.png

The status bar space is shared between a progress bar and a "user hint". When a
render is in progress, the progress bar will show a green line indicating the
approximate percentage completion of the render, whilst the "user hint" will
read "Rendering...":

.. image:: ../images/statusbar-rendering.png

At completion, the render bar will display a "Render finished" message and
the progress bar will be set to 100%:

.. image:: ../images/statusbar-rendered.png

Your First Render
-----------------

Now that you're familiar with the interface, it's time for your first render.
BasicTrace comes with a number of "testing" scenes which can be used to verify
that the program is working correctly. In this case, we'll be rendering one of
the simplest scenes---a single unit sphere on a black background. The steps for
accomplishing this are as follows:

#. In the configuration area of the main window, click the "Browse" button
   located next to the "Scene path" textbox.

   .. image:: ../images/step1.png

#. In the file selection window which appears, navigate to the BasicTrace
   directory and double click on the file named ``single-sphere.scn`` in the
   "Test Scenes" folder.

   .. image:: ../images/step2.png

#. Change the resolution of the image to 128x128 pixels using the resolution
   number boxes.

   .. image:: ../images/step3.png

#. Click "Render" and wait for rendering to complete. You will know rendering is
   complete because a "Finished render" message will be displayed in the status
   bar, and the "Save Render" button will be enabled.

   .. image:: ../images/step4.png

#. Click the "Save Render" button and use the dialog box which appears to choose
   an appropriate location for the rendered scene. Note that you may select the
   image file format using the dropdown menu in the bottom right-hand corner of
   the dialog.  The two supported formats are PNG (``.png``) and JPEG
   (``.jpg``).

   .. image:: ../images/step5.png

Assuming no error messages appear, you will have just produced your first render
using BasicTrace. The rendered image should look somewhat like the following:

.. image:: ../images/step7.png

Proceed to the next section of the documentation to learn how to create your own
(hopefully more interesting) scenes to render in BasicTrace.
