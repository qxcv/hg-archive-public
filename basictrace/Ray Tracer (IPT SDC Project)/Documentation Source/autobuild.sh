#!/bin/sh

while true
do
    echo "== Waiting..."
    inotifywait -e modify source/*.rst
    echo "== Making..."
    make html
    echo "== Sleeping..."
    sleep 1
    echo "== Done"
done
