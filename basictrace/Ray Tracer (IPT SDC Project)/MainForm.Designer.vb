﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
        Me.MainFormStatus = New System.Windows.Forms.StatusStrip()
        Me.ProgressBar = New System.Windows.Forms.ToolStripProgressBar()
        Me.UserHint = New System.Windows.Forms.ToolStripStatusLabel()
        Me.MainFormMenu = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OpenSceneMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SaveRenderMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AboutMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ScenePreviewGroupBox = New System.Windows.Forms.GroupBox()
        Me.RenderPreview = New System.Windows.Forms.PictureBox()
        Me.PreviewPromptLabel = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.CancelRenderButton = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.HeightBox = New System.Windows.Forms.NumericUpDown()
        Me.WidthBox = New System.Windows.Forms.NumericUpDown()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.SaveRenderButton = New System.Windows.Forms.Button()
        Me.FileBrowseButton = New System.Windows.Forms.Button()
        Me.SceneFilePathBox = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.RenderButton = New System.Windows.Forms.Button()
        Me.MainFormStatus.SuspendLayout()
        Me.MainFormMenu.SuspendLayout()
        Me.ScenePreviewGroupBox.SuspendLayout()
        CType(Me.RenderPreview, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.HeightBox, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.WidthBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MainFormStatus
        '
        Me.MainFormStatus.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ProgressBar, Me.UserHint})
        Me.MainFormStatus.Location = New System.Drawing.Point(0, 360)
        Me.MainFormStatus.Name = "MainFormStatus"
        Me.MainFormStatus.Size = New System.Drawing.Size(484, 22)
        Me.MainFormStatus.TabIndex = 0
        Me.MainFormStatus.Text = "MainFormStatus"
        '
        'ProgressBar
        '
        Me.ProgressBar.Name = "ProgressBar"
        Me.ProgressBar.Size = New System.Drawing.Size(100, 16)
        '
        'UserHint
        '
        Me.UserHint.Name = "UserHint"
        Me.UserHint.Size = New System.Drawing.Size(227, 17)
        Me.UserHint.Text = "Press Ctrl+O to load a scene for rendering"
        '
        'MainFormMenu
        '
        Me.MainFormMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.HelpToolStripMenuItem})
        Me.MainFormMenu.Location = New System.Drawing.Point(0, 0)
        Me.MainFormMenu.Name = "MainFormMenu"
        Me.MainFormMenu.Size = New System.Drawing.Size(484, 24)
        Me.MainFormMenu.TabIndex = 1
        Me.MainFormMenu.Text = "MainFormMenu"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.OpenSceneMenuItem, Me.SaveRenderMenuItem, Me.ExitMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'OpenSceneMenuItem
        '
        Me.OpenSceneMenuItem.Name = "OpenSceneMenuItem"
        Me.OpenSceneMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.O), System.Windows.Forms.Keys)
        Me.OpenSceneMenuItem.Size = New System.Drawing.Size(198, 22)
        Me.OpenSceneMenuItem.Text = "Open scene file"
        '
        'SaveRenderMenuItem
        '
        Me.SaveRenderMenuItem.Enabled = False
        Me.SaveRenderMenuItem.Name = "SaveRenderMenuItem"
        Me.SaveRenderMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.SaveRenderMenuItem.Size = New System.Drawing.Size(198, 22)
        Me.SaveRenderMenuItem.Text = "Save render"
        '
        'ExitMenuItem
        '
        Me.ExitMenuItem.Name = "ExitMenuItem"
        Me.ExitMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.F4), System.Windows.Forms.Keys)
        Me.ExitMenuItem.Size = New System.Drawing.Size(198, 22)
        Me.ExitMenuItem.Text = "Exit"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.HelpMenuItem, Me.AboutMenuItem})
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.HelpToolStripMenuItem.Text = "Help"
        '
        'HelpMenuItem
        '
        Me.HelpMenuItem.Name = "HelpMenuItem"
        Me.HelpMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F1
        Me.HelpMenuItem.Size = New System.Drawing.Size(144, 22)
        Me.HelpMenuItem.Text = "View help"
        '
        'AboutMenuItem
        '
        Me.AboutMenuItem.Name = "AboutMenuItem"
        Me.AboutMenuItem.Size = New System.Drawing.Size(144, 22)
        Me.AboutMenuItem.Text = "About"
        '
        'ScenePreviewGroupBox
        '
        Me.ScenePreviewGroupBox.Controls.Add(Me.RenderPreview)
        Me.ScenePreviewGroupBox.Controls.Add(Me.PreviewPromptLabel)
        Me.ScenePreviewGroupBox.Location = New System.Drawing.Point(12, 27)
        Me.ScenePreviewGroupBox.Name = "ScenePreviewGroupBox"
        Me.ScenePreviewGroupBox.Size = New System.Drawing.Size(460, 210)
        Me.ScenePreviewGroupBox.TabIndex = 2
        Me.ScenePreviewGroupBox.TabStop = False
        Me.ScenePreviewGroupBox.Text = "Scene preview"
        '
        'RenderPreview
        '
        Me.RenderPreview.Location = New System.Drawing.Point(6, 19)
        Me.RenderPreview.Name = "RenderPreview"
        Me.RenderPreview.Size = New System.Drawing.Size(448, 185)
        Me.RenderPreview.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.RenderPreview.TabIndex = 1
        Me.RenderPreview.TabStop = False
        Me.RenderPreview.Visible = False
        '
        'PreviewPromptLabel
        '
        Me.PreviewPromptLabel.AutoSize = True
        Me.PreviewPromptLabel.Location = New System.Drawing.Point(91, 83)
        Me.PreviewPromptLabel.Name = "PreviewPromptLabel"
        Me.PreviewPromptLabel.Size = New System.Drawing.Size(278, 13)
        Me.PreviewPromptLabel.TabIndex = 0
        Me.PreviewPromptLabel.Text = "Please load a scene and click ""render"" to view a preview"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.CancelRenderButton)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.HeightBox)
        Me.GroupBox1.Controls.Add(Me.WidthBox)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.SaveRenderButton)
        Me.GroupBox1.Controls.Add(Me.FileBrowseButton)
        Me.GroupBox1.Controls.Add(Me.SceneFilePathBox)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.RenderButton)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 243)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(460, 114)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Configure scene"
        '
        'CancelRenderButton
        '
        Me.CancelRenderButton.Location = New System.Drawing.Point(278, 85)
        Me.CancelRenderButton.Name = "CancelRenderButton"
        Me.CancelRenderButton.Size = New System.Drawing.Size(75, 23)
        Me.CancelRenderButton.TabIndex = 11
        Me.CancelRenderButton.Text = "Cancel"
        Me.CancelRenderButton.UseVisualStyleBackColor = True
        Me.CancelRenderButton.Visible = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(327, 56)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(42, 13)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "(height)"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(170, 56)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(38, 13)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "(width)"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(214, 56)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(14, 13)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "X"
        '
        'HeightBox
        '
        Me.HeightBox.Location = New System.Drawing.Point(234, 54)
        Me.HeightBox.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.HeightBox.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.HeightBox.Name = "HeightBox"
        Me.HeightBox.Size = New System.Drawing.Size(87, 20)
        Me.HeightBox.TabIndex = 7
        Me.HeightBox.Value = New Decimal(New Integer() {32, 0, 0, 0})
        '
        'WidthBox
        '
        Me.WidthBox.Location = New System.Drawing.Point(77, 54)
        Me.WidthBox.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.WidthBox.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.WidthBox.Name = "WidthBox"
        Me.WidthBox.Size = New System.Drawing.Size(87, 20)
        Me.WidthBox.TabIndex = 6
        Me.WidthBox.Value = New Decimal(New Integer() {32, 0, 0, 0})
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 56)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(60, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Resolution:"
        '
        'SaveRenderButton
        '
        Me.SaveRenderButton.Enabled = False
        Me.SaveRenderButton.Location = New System.Drawing.Point(359, 85)
        Me.SaveRenderButton.Name = "SaveRenderButton"
        Me.SaveRenderButton.Size = New System.Drawing.Size(95, 23)
        Me.SaveRenderButton.TabIndex = 4
        Me.SaveRenderButton.Text = "Save Render"
        Me.SaveRenderButton.UseVisualStyleBackColor = True
        '
        'FileBrowseButton
        '
        Me.FileBrowseButton.Location = New System.Drawing.Point(379, 20)
        Me.FileBrowseButton.Name = "FileBrowseButton"
        Me.FileBrowseButton.Size = New System.Drawing.Size(75, 23)
        Me.FileBrowseButton.TabIndex = 3
        Me.FileBrowseButton.Text = "Browse"
        Me.FileBrowseButton.UseVisualStyleBackColor = True
        '
        'SceneFilePathBox
        '
        Me.SceneFilePathBox.Location = New System.Drawing.Point(77, 22)
        Me.SceneFilePathBox.Name = "SceneFilePathBox"
        Me.SceneFilePathBox.Size = New System.Drawing.Size(296, 20)
        Me.SceneFilePathBox.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 25)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(65, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Scene path:"
        '
        'RenderButton
        '
        Me.RenderButton.Location = New System.Drawing.Point(278, 85)
        Me.RenderButton.Name = "RenderButton"
        Me.RenderButton.Size = New System.Drawing.Size(75, 23)
        Me.RenderButton.TabIndex = 0
        Me.RenderButton.Text = "Render"
        Me.RenderButton.UseVisualStyleBackColor = True
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(484, 382)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.ScenePreviewGroupBox)
        Me.Controls.Add(Me.MainFormStatus)
        Me.Controls.Add(Me.MainFormMenu)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MainFormMenu
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(500, 420)
        Me.MinimumSize = New System.Drawing.Size(500, 420)
        Me.Name = "MainForm"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.Text = "BasicTrace"
        Me.MainFormStatus.ResumeLayout(False)
        Me.MainFormStatus.PerformLayout()
        Me.MainFormMenu.ResumeLayout(False)
        Me.MainFormMenu.PerformLayout()
        Me.ScenePreviewGroupBox.ResumeLayout(False)
        Me.ScenePreviewGroupBox.PerformLayout()
        CType(Me.RenderPreview, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.HeightBox, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.WidthBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MainFormStatus As System.Windows.Forms.StatusStrip
    Friend WithEvents MainFormMenu As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HelpToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OpenSceneMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SaveRenderMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HelpMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AboutMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProgressBar As System.Windows.Forms.ToolStripProgressBar
    Friend WithEvents UserHint As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ExitMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ScenePreviewGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents PreviewPromptLabel As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents RenderButton As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents FileBrowseButton As System.Windows.Forms.Button
    Friend WithEvents SceneFilePathBox As System.Windows.Forms.TextBox
    Friend WithEvents SaveRenderButton As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents HeightBox As System.Windows.Forms.NumericUpDown
    Friend WithEvents WidthBox As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents RenderPreview As System.Windows.Forms.PictureBox
    Friend WithEvents CancelRenderButton As System.Windows.Forms.Button

End Class
