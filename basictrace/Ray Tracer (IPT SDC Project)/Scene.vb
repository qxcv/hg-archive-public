﻿Imports System.Reflection

' This class represents a 3D scene, which may be constructed from a scene
' description file and used to "trace rays". It is used in Renderer.vb to serve
' as a medium through which all "business logic" takes place.

' Note that all methods of Scene raise exceptions if a rendering or parsing
' error occurs, and the exception is then caught by the main GUI loop, which
' displays an error message and cancels the render.
Public Class Scene
    ' Move this far before casting a ray
    Dim MOVE_FIRST_DIST As Double = 0.0001
    ' Render camera for ray shooting
    Dim RenderCamera As Camera
    ' A string describing the current scene
    Dim SceneString As String
    ' Scene aspect ratio
    Dim AspectRatio As Double
    ' All of the lights in the scene
    Dim Lights As New List(Of Light)
    ' All of the objects in the scene
    Dim Objects As New List(Of RenderPrimitive)
    ' Colour to return if ray does not hit anything
    Dim DefaultColour As New Vec3(0, 0, 1)
    ' The current material
    Dim CurrentMaterial As New Material
    ' Ambient colour
    Dim Ambient As New Vec3(0, 0, 0)
    ' Quadratic radiation attenuation factor in (const, lin, quad) form
    ' The default is the real value for point light sources
    Dim AttenuationParameters As New Vec3(0, 0, 1)

    ' Constructor from a scene description string (usually read from a scene
    ' description file).

    ' In a scene description string, each line is either empty or takes the
    ' format "<command name> [<argument> [<argument [...]]]<newline>". For
    ' instance, a valid command line might be "sphere 0 0 0 1<newline>", which
    ' would create a unit sphere at the origin.

    ' Note that the format also supports comments using the "#" character, and
    ' if the parser encounters a "#" character on a line, it will simply ignore the
    ' remainder of the line.
    Sub New(ByVal NewSceneDescription As String, ByVal NewAspectRatio As Double)
        SceneString = NewSceneDescription
        AspectRatio = NewAspectRatio
        RenderCamera = New Camera(90, AspectRatio, New Ray(New Vec3(0, 0, 0), New Vec3(1, 0, 0)))
        Parse()
    End Sub

    ' Parse the scene description file line-by-line
    Sub Parse()
        Dim LineNo As Integer = 0
        For Each Line As String In SceneString.Split(vbNewLine)
            LineNo += 1
            Line = Line.Trim()
            If Line.StartsWith("#") Or Line = "" Then
                ' The line starts with a comment!
                Continue For
            End If
            ' Now, get the part of the line which ISN'T a comment
            Dim SceneLine As String = Line.Split("#")(0)
            Dim SplitSceneLine As String() = SceneLine.Split()
            Dim Cmd As String = SplitSceneLine(0)
            SplitSceneLine(0) = Nothing
            ' This System.Reflection call taken from http://stackoverflow.com/questions/135443/how-do-i-use-reflection-to-invoke-a-private-method-in-c
            Dim CmdMethod As MethodInfo = Me.GetType().GetMethod("cmd_" & Cmd, BindingFlags.NonPublic Or BindingFlags.Public Or BindingFlags.Instance)
            If CmdMethod Is Nothing Then
                ' Uh oh, there's a command we haven't implemented!
                Throw New Exception("Parsing error: no such command '" & Cmd & "' at Line #" & LineNo)
            End If
            Dim CmdMethodParams As New List(Of Object)
            For Each Argument In SplitSceneLine
                ' Nothing means that it's the first element (the command itself)
                ' whilst "" means that VB is just including empty strings in its
                ' split.
                If Argument Is Nothing OrElse Argument = "" Then
                    Continue For
                End If
                Dim AsDouble As Double
                Try
                    AsDouble = CDbl(Argument)
                Catch ex As Exception
                    Throw New Exception("Parsing error: could not convert argument '" _
                                        & Argument & "' to double at line #" & LineNo)
                End Try
                CmdMethodParams.Add(AsDouble)
            Next
            Try
                CmdMethod.Invoke(Me, CmdMethodParams.ToArray())
            Catch ex As Exception
                Throw New Exception("Parsing error: could not run command '" & Cmd & "' on line #" & LineNo)
            End Try
        Next
    End Sub

    ' PARSER COMMANDS
    ' These take the format cmd_<command>, and are called when the corresponding <command> is encountered by Parse()
    Sub cmd_camera(ByVal xloc As Double, ByVal yloc As Double, ByVal zloc As Double, ByVal xdir As Double, ByVal ydir As Double, ByVal zdir As Double, ByVal fov As Double)
        RenderCamera = New Camera(fov, AspectRatio, New Ray(New Vec3(xloc, yloc, zloc), New Vec3(xdir, ydir, zdir)))
    End Sub

    Sub cmd_ambient(ByVal r As Double, ByVal g As Double, ByVal b As Double)
        Ambient = New Vec3(r, g, b)
    End Sub

    Sub cmd_background(ByVal r As Double, ByVal g As Double, ByVal b As Double)
        DefaultColour = New Vec3(r, g, b)
    End Sub

    Sub cmd_specular(ByVal r As Double, ByVal g As Double, ByVal b As Double)
        CurrentMaterial.Specular = New Vec3(r, g, b)
    End Sub

    Sub cmd_diffuse(ByVal r As Double, ByVal g As Double, ByVal b As Double)
        CurrentMaterial.Diffuse = New Vec3(r, g, b)
    End Sub

    Sub cmd_emission(ByVal r As Double, ByVal g As Double, ByVal b As Double)
        CurrentMaterial.Emission = New Vec3(r, g, b)
    End Sub

    Sub cmd_shininess(ByVal shininess As Double)
        CurrentMaterial.Shininess = shininess
    End Sub

    Sub cmd_light(ByVal xloc As Double, ByVal yloc As Double, ByVal zloc As Double, ByVal intensity As Double)
        Lights.Add(New Light(New Vec3(xloc, yloc, zloc), intensity))
    End Sub

    Sub cmd_sphere(ByVal xloc As Double, ByVal yloc As Double, ByVal zloc As Double, ByVal radius As Double)
        Objects.Add(New Sphere(CurrentMaterial, New Vec3(xloc, yloc, zloc), radius))
    End Sub

    ' Check which object (if any) the given ray collides with
    Function GetCollisionObject(ByVal R As Ray) As Tuple(Of RenderPrimitive, Double)
        Dim Nearest As Tuple(Of RenderPrimitive, Double) = Nothing
        For Each Obj As RenderPrimitive In Objects
            Dim Dist As Double? = Obj.Intersect(R)
            If Dist IsNot Nothing Then
                If Nearest Is Nothing OrElse Dist < Nearest.Item2 Then
                    Nearest = New Tuple(Of RenderPrimitive, Double)(Obj, Dist)
                End If
            End If
        Next
        Return Nearest
    End Function

    ' Turn a pixel location into a ray
    Function GetCameraRay(ByVal FromCentreLR As Double, ByVal FromCentreDU As Double) As Ray
        ' We just handle this with our Camera instance
        Return RenderCamera.GetCameraRay(FromCentreLR, FromCentreDU)
    End Function

    ' This is the business function!
    ' It takes a ray (R) and recurses so long as MaxHops is greater than 0 and
    ' the traced ray hits an object. The return value is a Vec3 indicating the
    ' colour of the traced ray (RGB, each component a double from 0-1).
    ' Remember to keep MaxHops reasonably low, or stack space will be exhausted
    Function Trace(ByVal R As Ray, Optional ByVal MaxHops As Integer = 10) As Vec3
        Dim CollObjTuple As Tuple(Of RenderPrimitive, Double) = GetCollisionObject(R)
        If CollObjTuple Is Nothing Then
            Return DefaultColour
        End If
        Dim CollObj = CollObjTuple.Item1
        Dim CollDist As Double = CollObjTuple.Item2
        Dim CollMat As Material = CollObj.ObjMaterial
        Dim Intersection As Vec3 = R.Root + R.Direction * CollDist
        Dim Normal As Vec3 = CollObj.SurfaceNormal(Intersection)
        Dim AttenuationAmount As Double = (AttenuationParameters.x + _
                                     AttenuationParameters.y * CollDist + _
                                     AttenuationParameters.z * CollDist ^ 2)
        Dim TotalColour As Vec3 = Ambient _
                                  + CollMat.Emission _
                                  / AttenuationAmount
        For Each SceneLight As Light In Lights
            ' Get a ray to the light
            Dim ToLight As Vec3 = SceneLight.Location - Intersection
            Dim LightRay As New Ray(Intersection, ToLight)
            Dim LightDist As Double = ToLight.Magnitude()
            Dim PossibleBlocker As Tuple(Of RenderPrimitive, Double) = GetCollisionObject( _
                New Ray(LightRay.Root + MOVE_FIRST_DIST * LightRay.Direction, LightRay.Direction))
            If PossibleBlocker IsNot Nothing AndAlso PossibleBlocker.Item2 < LightDist Then
                ' The ray is blocked, let's not do illumination
                Continue For
            End If

            ' How far are we away? How much will this attenuate the light energy?
            Dim Attenuation As Double = AttenuationParameters.x _
                                        + AttenuationParameters.y * LightDist _
                                        + AttenuationParameters.z * LightDist ^ 2
            Dim NewIntensity As Double = SceneLight.Intensity / Attenuation
            Dim HalfAngle As Vec3 = (LightRay.Direction - R.Direction).Normalised()
            ' Incorporate all of the parts
            TotalColour += NewIntensity * ( _
                CollMat.Diffuse * Math.Max(Normal.Dot(LightRay.Direction), 0) _
                + CollMat.Specular * Math.Max(Normal.Dot(HalfAngle), 0) ^ CollMat.Shininess)
        Next
        If MaxHops > 0 Then
            ' We can recurse
            Dim ReflectedDirection As Vec3 = R.Direction - 2 * Normal.Dot(R.Direction) * Normal
            Dim ReflectedRay As New Ray(Intersection + Normal * MOVE_FIRST_DIST, _
                                        ReflectedDirection)
            Dim RecursionColour As Vec3 = Trace(ReflectedRay, MaxHops - 1)
            ' Remember to attenuate reflections
            TotalColour += CollMat.Specular * RecursionColour / AttenuationAmount
        End If
        Return TotalColour
    End Function
End Class