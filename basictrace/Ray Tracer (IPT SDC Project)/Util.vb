﻿' These are general-purpose utility functions which don't fit anywhere else
Module Util
    ' Return Original if it is between min and max, otherwise return the relevant bound
    Function Clamp(ByVal Original As Double, ByVal Min As Double, ByVal Max As Double) As Double
        Return Math.Max(Min, Math.Min(Max, Original))
    End Function

    ' Converts a vector of doubles (each from 0 to 1, usually) to a .NET colour
    Function Vec2Colour(ByVal Source As Vec3) As Color
        Return Color.FromArgb(Clamp(255 * Source.x, 0, 255), Clamp(255 * Source.y, 0, 255), Clamp(255 * Source.z, 0, 255))
    End Function

    ' Test if two doubles are equal, accounting for numerical error
    Function ApproxEq(ByVal a As Double, ByVal b As Double, Optional ByVal epsilon As Double = 0.00001) As Boolean
        Return Math.Abs(a - b) < epsilon
    End Function

    ' Test if two vectors of doubles are equal, accounting for numerical error
    Function ApproxEqVec(ByVal a As Vec3, ByVal b As Vec3) As Boolean
        Return ApproxEq(a.x, b.x) AndAlso ApproxEq(a.y, b.y) AndAlso ApproxEq(a.z, b.z)
    End Function

    ' Test if two rays (comprised of vectors of doubles) are equal, accounting for numerical error
    Function ApproxEqRay(ByVal a As Ray, ByVal b As Ray) As Boolean
        Return ApproxEqVec(a.Root, b.Root) AndAlso ApproxEqVec(a.Direction, b.Direction)
    End Function

    ' Show the values of a vector in a form which can be copied & pasted back into VB
    ' This is primarily useful for debugging purposes.
    Function Vec2String(ByVal Input As Vec3) As String
        Return "New Vec3(" & Input.x & ", " & Input.y & ", " & Input.z & ")"
    End Function

    ' Convert degrees to radians
    Function Deg2Rad(ByVal Deg As Double) As Double
        Return Math.PI * Deg / 180
    End Function

    ' Convert radians to degrees
    Function Rad2Deg(ByVal Rad As Double) As Double
        Return 180 * Rad / Math.PI
    End Function
End Module
