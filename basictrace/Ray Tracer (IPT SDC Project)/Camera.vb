﻿' This class exists to represent a camera in the 3D scene
' It is used by Scene instances to determine which direction it should shoot rays
Public Class Camera
    ' Horizontal/vertical FOV
    Dim FovX, FovY As Double
    ' Camera root position
    Dim CamPos As Ray

    ' NewFOV is the horizontal Field Of View (FOV), AspectRatio is Image Width / Image Height
    ' NewCameraPos is the location and direction of the camera, as a ray
    Sub New(ByVal NewFOV As Double, ByVal AspectRatio As Double, ByVal NewCameraPos As Ray)
        CamPos = NewCameraPos
        FovX = NewFOV
        FovY = Rad2Deg(2 * Math.Atan(Math.Tan(Deg2Rad(FovX) / 2) / AspectRatio))
    End Sub

    ' Turn a pixel location into a ray
    Function GetCameraRay(ByVal FromCentreLR As Double, ByVal FromCentreDU As Double) As Ray
        ' What direction are we facing?
        Dim DirectionVector As Vec3 = CamPos.Direction
        ' What direction is "up" relative to us?
        Dim PlaneUpVector As Vec3 = New Vec3(0, 0, 1)
        If ApproxEqVec(PlaneUpVector, DirectionVector) OrElse ApproxEqVec(DirectionVector, New Vec3(0, 0, -1)) Then
            ' We're facing straight down or up, so "straight down or up" cannot be our up vector for this plane
            PlaneUpVector = New Vec3(0, 1, 0)
        End If
        ' Unit vector representing "left and right" on our virtual screen
        Dim AcrossLensLR As Vec3 = PlaneUpVector.Cross(DirectionVector).Normalised()
        ' Unit vector representing "up and down" on our virtual screen
        Dim UpLensDU As Vec3 = AcrossLensLR.Cross(DirectionVector).Normalised()
        Dim UpAmount As Double = Math.Tan(FovY * Math.PI / 180 / 2) * FromCentreDU
        Dim RightAmount As Double = Math.Tan(FovX * Math.PI / 180 / 2) * FromCentreLR
        Return New Ray(CamPos.Root, UpAmount * UpLensDU + RightAmount * AcrossLensLR + DirectionVector)
    End Function
End Class