﻿' Instances of this struct are put in a queue and sent back to the main thread
' to be rendered to the preview canvas
Public Structure RendererUpdate
    Dim XLocation As Integer
    Dim YLocation As Integer
    Dim Colour As Color
End Structure

' This class is instantiated by the main thread, and its Run() method run in
' its own thread to coordinate rendering resources
Public Class Renderer
    Dim RenderScene As Scene
    Dim ImageWidth, ImageHeight, PixelsRendered As Integer
    Dim SceneString As String
    ' This is shared with the main thread to send back rendered pixels
    Dim UpdateQueue As Concurrent.ConcurrentQueue(Of RendererUpdate)

    ' These are for error handling - if the render thread exits unexpectedly,
    ' these are checked to determine the cause of the error
    Public ReturnedOK As Boolean
    Public EndException As Exception

    Sub New(ByVal NewWidth As Integer, ByVal NewHeight As Integer, ByVal NewSceneString As String, ByRef RenderQueue As Concurrent.ConcurrentQueue(Of RendererUpdate))
        SceneString = NewSceneString
        ImageWidth = NewWidth
        ImageHeight = NewHeight
        PixelsRendered = 0
        UpdateQueue = RenderQueue
    End Sub

    ' This is a wrapper to run the renderer
    ' We catch all exceptions here so that the main
    ' thread can deal with them.
    Sub Run()
        ReturnedOK = False
        Try
            _Run()
        Catch ex As Exception
            ' So that we can get the exception further up
            EndException = ex
            Exit Sub
        End Try
        ReturnedOK = True
    End Sub

    ' Renderer thread logic itself
    Sub _Run()
        ' Pass in the scene string and aspect ratio
        Dim RenderScene = New Scene(SceneString, ImageWidth / ImageHeight)
        ' We may as well use Parallel.For here, it's easier than managing our own threads
        Threading.Tasks.Parallel.For(0, ImageWidth * ImageHeight,
                                     Sub(N)
                                         ' This lambda expression will be run in parallel
                                         ' by an arbitrary number of parallel workers
                                         Dim i = N Mod ImageWidth
                                         ' We use Int() here because it rounds down
                                         Dim j = Int(N / ImageWidth)
                                         Dim UpdatePacket As New RendererUpdate
                                         PixelsRendered += 1
                                         UpdatePacket.XLocation = i
                                         UpdatePacket.YLocation = j
                                         Dim TraceRay = RenderScene.GetCameraRay(i / ImageWidth - 0.5, j / ImageHeight - 0.5)
                                         Dim ColourVec = RenderScene.Trace(TraceRay)
                                         UpdatePacket.Colour = Vec2Colour(ColourVec)
                                         ' Give back some CPU time
                                         ' For some reason, the Windows
                                         ' scheduler seems incapable of
                                         ' prioritising the Winforms thread
                                         ' properly, so we need to do this to
                                         ' stop our UI from locking up
                                         Threading.Thread.Sleep(1)
                                         UpdateQueue.Enqueue(UpdatePacket)
                                     End Sub)
    End Sub
End Class
