# this file generates some of the tests in this directory
# you can safely ignore it as it is not part of basictrace

from itertools import product
from math import sin, cos, sqrt
from random import random

def random_colour():
	elems = [random() for i in xrange(3)]
	mag = sqrt(sum(x**2 for x in elems))
	return ' '.join("{0:.2f}".format(x/mag) for x in elems)

def random_colour_sphere(loc, rad):
    rancol = lambda s: "{0} {1}\n".format(s, random_colour())
    s = rancol("specular")
    s += rancol("emission")
    s += rancol("diffuse")
    s += "shininess 2\n"
    return s + "sphere {1} {2} {3} {0}\n".format(rad, *loc)

def grid(dimx, dimy, dimz, sphere_rad):
    rv = ""
    for i, j, k in product(range(dimx), range(dimy), range(dimz)):
        loc = map(lambda x: 4 * sphere_rad * x, (i, j, k))
        rv += random_colour_sphere(loc, sphere_rad)
    return rv

def spiral(number, sphere_rad, spir_rad, sub_ang):
    rv = ""
    for i in xrange(number):
        ang = sub_ang * i
        dist = i * 2 * sphere_rad + 2
        loc = (dist, spir_rad * sin(ang), spir_rad * cos(ang))
        rv += random_colour_sphere(loc, sphere_rad)
    return rv
