﻿' Class for handling all GUI code
Public Class MainForm
    ' CLASS ATTRIBUTE DECLARATIONS
    ' These are used within the form class for statefulness between GUI events

    ' Text for the user hint bar at the bottom of the dialog
    Dim HINT_INITIAL = "Press Ctrl+O to load a scene for rendering"
    Dim HINT_RENDERING = "Rendering..."
    Dim HINT_FINISHED = "Finished render"

    ' Miscellaneous declarations
    Dim RenderCanvas As Bitmap
    Dim LocalRenderer As Renderer
    Dim RenderWorker As Threading.Thread
    Dim DrawQueue As Concurrent.ConcurrentQueue(Of RendererUpdate)
    Dim DrawTimer As New Timer
    Dim PixelsRendered, PixelsInImage As Integer

    ' BUTTON CLICK HANDLERS
    ' Most of these are self explanatory. They each handle the logic behind one of the buttons in the form.
    Private Sub OpenSceneMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OpenSceneMenuItem.Click
        Dim Browser = New OpenFileDialog()
        Browser.CheckFileExists = True
        Browser.Title = "Open Scene File"
        Browser.Filter = "Scene files (*.scn)|*.scn|All files (*.*)|*.*"
        Dim Result = Browser.ShowDialog()
        If Result <> Windows.Forms.DialogResult.OK Then
            ' The user clicked "Cancel"
            Exit Sub
        End If
        SceneFilePathBox.Text = Browser.FileName
    End Sub

    ' When the user wants to save their render to a file
    Private Sub SaveRenderMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveRenderMenuItem.Click
        Dim Browser = New SaveFileDialog
        Browser.Title = "Select File to Save Render To"
        Browser.Filter = "JPEG Files (*.jpg)|*.jpg|PNG Files|*.png"
        Browser.AddExtension = True
        Dim Result As Windows.Forms.DialogResult = Browser.ShowDialog()
        If Result <> Windows.Forms.DialogResult.OK Then
            ' The user clicked "Cancel" or did not select a file
            Exit Sub
        End If
        ' We create an Image instance because that class has a Save() method
        Dim SaveImage As Image = Image.FromHbitmap(RenderCanvas.GetHbitmap())
        Try
            SaveImage.Save(Browser.FileName)
        Catch ex As Exception
            MsgBox("There was an error saving the file, '" & ex.ToString() & "'")
        End Try
    End Sub

    ' When the user clicks the "Exit" button under the "File" menu
    Private Sub ExitMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitMenuItem.Click
        QuitApplication()
    End Sub

    ' When the user presses F1 OR clicks the "Help" button under the "Help" menu
    Private Sub HelpMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles HelpMenuItem.Click
        Process.Start("HTML Documentation\index.html")
    End Sub

    ' When the user clicks the "About" button under the "Help" menu
    Private Sub AboutMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AboutMenuItem.Click
        MsgBox("BasicTrace is a ray tracer under the ISC Licence." & vbNewLine & "Please see the LICENCE.txt file for more information.",
            MsgBoxStyle.Information, "About BasicTrace")
    End Sub

    ' When the user clicks the "Render" button
    Private Sub RenderButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RenderButton.Click
        ' Let's get the file contents first
        Dim SceneString As String
        Try
            SceneString = FileIO.FileSystem.ReadAllText(SceneFilePathBox.Text)
        Catch ex As Exception
            MsgBox("Could not load contents of '" & SceneFilePathBox.Text & "'. Please check the path and try again.", MsgBoxStyle.Critical, "File error")
            Exit Sub
        End Try

        ' Next, setup the render button itself
        RenderButton.Visible = False
        CancelRenderButton.Visible = True

        ' Now, disable the save buttons
        DisableSaveButtons()

        ' Initialise the canvas!
        RenderCanvas = New Bitmap(CInt(WidthBox.Value), CInt(HeightBox.Value))
        RenderPreview.Image = RenderCanvas

        ' Now, show the render preview
        PreviewPromptLabel.Visible = False
        RenderPreview.Visible = True

        ' Set up the progress bar
        ProgressBar.Value = 0
        PixelsInImage = WidthBox.Value * HeightBox.Value
        PixelsRendered = 0

        ' Our help text
        UserHint.Text = HINT_RENDERING

        ' Construct our renderer and its resources (draw queue, Renderer instance, worker thread, etc.)
        DrawQueue = New Concurrent.ConcurrentQueue(Of RendererUpdate)

        LocalRenderer = New Renderer(WidthBox.Value, HeightBox.Value, SceneString, DrawQueue)

        Dim StartParams As Threading.ThreadStart = New Threading.ThreadStart(AddressOf LocalRenderer.Run)
        RenderWorker = New Threading.Thread(StartParams)
        RenderWorker.IsBackground = True

        DrawTimer.Interval = 500
        DrawTimer.Enabled = True

        ' And run!
        RenderWorker.Start()
    End Sub

    ' This runs every few milliseconds to draw any new pixels from the
    ' renderer to the screen, and check whether the render has finished
    Private Sub WhileRenderingTick()
        EmptyDrawQueue()
        If Not RenderWorker.IsAlive Then
            If Not LocalRenderer.ReturnedOK Then
                ' There was an exception in the renderer
                Dim ErrorMessage As String = "There was an error whilst rendering the supplied scene:" & _
                    vbNewLine & LocalRenderer.EndException.Message

                CancelRender()

                MsgBox(ErrorMessage, MsgBoxStyle.Critical, "Rendering Error")
                Exit Sub
            End If
            ' Okay then, we're finished rendering
            ' Empty the draw queue just in case
            EmptyDrawQueue()
            DrawTimer.Enabled = False
            RenderFinishedOK()
        End If
    End Sub

    ' When the user clicks the "Cancel" button
    Private Sub CancelRenderButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CancelRenderButton.Click
        CancelRender()
    End Sub

    ' MISCELLANEOUS FUNCTIONS
    ' Utility functions and pieces of code which are used in several places.
    Private Sub DisableSaveButtons()
        SaveRenderButton.Enabled = False
        SaveRenderMenuItem.Enabled = False
    End Sub

    Private Sub EnableSaveButtons()
        SaveRenderButton.Enabled = True
        SaveRenderMenuItem.Enabled = True
    End Sub

    ' This is just a stub for now. It is called when the user clicks the exit
    ' button under the "File" menu.
    Private Sub QuitApplication()
        Application.Exit()
    End Sub

    ' This function is called whenever the render worker finishes and does not
    ' raise an error. It allows the user to save their render and tells them
    ' that the render is finished.
    Private Sub RenderFinishedOK()
        UserHint.Text = HINT_FINISHED
        EnableSaveButtons()
        CancelRenderButton.Visible = False
        RenderButton.Visible = True
    End Sub

    Private Sub CancelRender()
        RenderWorker.Abort()
        ' Free all of the resources used by the renderer
        RenderWorker = Nothing
        LocalRenderer = Nothing
        DrawQueue = Nothing
        RenderCanvas = Nothing
        DrawTimer.Enabled = False
        ResetForm()
    End Sub

    Private Sub EmptyDrawQueue()
        Dim UpdatePacket As RendererUpdate
        While Not DrawQueue.IsEmpty
            If Not DrawQueue.TryDequeue(UpdatePacket) Then
                ' There's nothing there!
                ' This will happen if this is called on another thread
                ' Which really shouldn't happen, but ConcurrentQueues
                ' use this method, so we should have a handler here
                ' in case it returns "false"
                Exit While
            End If
            RenderCanvas.SetPixel(UpdatePacket.XLocation, UpdatePacket.YLocation, UpdatePacket.Colour)
            PixelsRendered += 1
        End While
        RenderPreview.Image = RenderCanvas
        ProgressBar.Value = 100 * PixelsRendered / PixelsInImage
        ' Make sure WinForms redraws the PictureBox
        RenderPreview.Update()
    End Sub

    ' This sub puts the form back into a default application startup state
    Private Sub ResetForm()
        DisableSaveButtons()

        CancelRenderButton.Visible = False
        RenderButton.Visible = True
        RenderPreview.Visible = False
        PreviewPromptLabel.Visible = True
        UserHint.Text = HINT_INITIAL

        ProgressBar.Value = ProgressBar.Minimum
    End Sub

    ' MAIN ENTRY POINT
    ' This runs when the app starts. We mostly do housekeeping tasks here.
    Private Sub MainForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' We don't need separate handlers for the in-dialog buttons, so we alias them to the menu item callbacks
        AddHandler FileBrowseButton.Click, AddressOf OpenSceneMenuItem_Click
        AddHandler SaveRenderButton.Click, AddressOf SaveRenderMenuItem_Click

        DrawTimer.Enabled = False
        AddHandler DrawTimer.Tick, AddressOf WhileRenderingTick

        ResetForm()
    End Sub
End Class