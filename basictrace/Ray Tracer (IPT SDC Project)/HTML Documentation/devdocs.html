

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
    <title>Extending BasicTrace &mdash; BasicTrace RC1 documentation</title>
    
    <link rel="stylesheet" href="_static/default.css" type="text/css" />
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" />
    
    <script type="text/javascript">
      var DOCUMENTATION_OPTIONS = {
        URL_ROOT:    '',
        VERSION:     'RC1',
        COLLAPSE_INDEX: false,
        FILE_SUFFIX: '.html',
        HAS_SOURCE:  true
      };
    </script>
    <script type="text/javascript" src="_static/jquery.js"></script>
    <script type="text/javascript" src="_static/underscore.js"></script>
    <script type="text/javascript" src="_static/doctools.js"></script>
    <link rel="shortcut icon" href="_static/logo.ico"/>
    <link rel="top" title="BasicTrace RC1 documentation" href="index.html" />
    <link rel="prev" title="Your First BasicTrace Scene" href="fileformat.html" /> 
  </head>
  <body>
    <div class="related">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="genindex.html" title="General Index"
             accesskey="I">index</a></li>
        <li class="right" >
          <a href="fileformat.html" title="Your First BasicTrace Scene"
             accesskey="P">previous</a> |</li>
        <li><a href="index.html">BasicTrace RC1 documentation</a> &raquo;</li> 
      </ul>
    </div>  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body">
            
  <div class="section" id="extending-basictrace">
<h1>Extending BasicTrace<a class="headerlink" href="#extending-basictrace" title="Permalink to this headline">¶</a></h1>
<p>One of the main goals of BasicTrace is to be a simple platform for experimenting
with and understanding ray tracers. As such, significant effort has been
expended to make BasicTrace&#8217;s code base as hackable as possible, and this
document should serve as a worthy companion when exploring BasicTrace&#8217;s source
code. If you want a good overview of the computer graphics theory behind ray
tracers, it&#8217;s worth checking out <a class="reference external" href="http://www.cs.berkeley.edu/~ravir/">Ravi Ramamoorthi&#8217;s</a> free CS184X course <a class="reference external" href="https://www.edx.org/course/uc-berkeley/cs184-1x/foundations-computer-graphics/576">at EdX</a>.
Note that this document is written for programmers, and so assumes familiarity
with basic programming techniques and jargon.</p>
<div class="section" id="the-10-000ft-view">
<h2>The 10,000ft View<a class="headerlink" href="#the-10-000ft-view" title="Permalink to this headline">¶</a></h2>
<p>BasicTrace is divided into a number of files, which each serve a different
purpose:</p>
<ul class="simple">
<li><tt class="docutils literal"><span class="pre">MainForm.vb</span></tt>: This is where all of the GUI handling code goes. Outside of
<tt class="docutils literal"><span class="pre">MainForm.vb</span></tt>, there are no WinForms procedure calls <em>at all</em>, so if you
want to work on the GUI, this is the file for you.</li>
<li><tt class="docutils literal"><span class="pre">Renderer.vb</span></tt>: Glue code to join the renderer logic (in <tt class="docutils literal"><span class="pre">Scene.vb</span></tt>) to the
main form. The code in <tt class="docutils literal"><span class="pre">Renderer.vb</span></tt> runs in a background thread and sends
rendered pixels back to <tt class="docutils literal"><span class="pre">MainForm.vb</span></tt> using a queue, which is periodically
emptied using a timer event.</li>
<li><tt class="docutils literal"><span class="pre">Scene.vb</span></tt>: This is where the logic for ray tracing goes. The most important
part of <tt class="docutils literal"><span class="pre">Scene.vb</span></tt> is the <tt class="docutils literal"><span class="pre">Trace()</span></tt> method, which shoots a ray into the
scene and returns the colour of the ray.</li>
<li><tt class="docutils literal"><span class="pre">Camera.vb</span></tt>: Logic for the scene&#8217;s &#8220;camera&#8221;. Cameras are responsible for
converting pixel locations on the virtual screen into &#8220;rays&#8221; which can be
traced by the <strong>ray</strong> tracer.</li>
<li><tt class="docutils literal"><span class="pre">SceneObjects</span></tt>: Where all of the things like material and object structure
definitions go. This also contains intersection functions for scene objects,
which are called by <tt class="docutils literal"><span class="pre">Scene.vb</span></tt> when rendering.</li>
<li><tt class="docutils literal"><span class="pre">Util.vb</span></tt>: Utility functions which don&#8217;t really fit anywhere else. Things
like vector and float comparison, colour conversion, etc. end up in here.</li>
<li><tt class="docutils literal"><span class="pre">UnitTests.vb</span></tt>: Unit tests for BasicTrace. Unit testing allows the
programmer to specify expected behaviour for certain software components (for
instance, classes or functions) and automatically execute code which checks
whether the software components conform to the expected behaviour.</li>
</ul>
</div>
<div class="section" id="addhandler-renderbutton-click-addressof-thissection">
<h2><tt class="docutils literal"><span class="pre">AddHandler</span> <span class="pre">RenderButton.Click,</span> <span class="pre">AddressOf</span> <span class="pre">ThisSection</span></tt><a class="headerlink" href="#addhandler-renderbutton-click-addressof-thissection" title="Permalink to this headline">¶</a></h2>
<p>So, what happens when you click the render button? First, WinForms calls the
<tt class="docutils literal"><span class="pre">RenderButton_Click()</span></tt> method in <tt class="docutils literal"><span class="pre">MainForm.vb</span></tt>, which does a couple of
things:</p>
<ol class="arabic simple">
<li>Instantiates the render canvas on the main form using the supplied
resolution.</li>
<li>Creates a new draw queue (<tt class="docutils literal"><span class="pre">DrawQueue</span></tt>) so that it can be notified of
updates sent by the renderer background thread.</li>
<li>Sets a timer to periodically call a function (<tt class="docutils literal"><span class="pre">WhileRenderingTick()</span></tt>) which
empties the draw queue and renders the data in the draw queue to the screen.</li>
<li>Makes a <tt class="docutils literal"><span class="pre">Renderer</span></tt> instance (defined in <tt class="docutils literal"><span class="pre">Renderer.vb</span></tt>) using the supplied
scene and image resolution, as well as the draw queue it just created.</li>
<li>Finally, it starts a thread so that <tt class="docutils literal"><span class="pre">Renderer.Run()</span></tt> can be executed in the
background.</li>
</ol>
<p>Now all of the work is being done by <tt class="docutils literal"><span class="pre">Renderer.Run()</span></tt> in <tt class="docutils literal"><span class="pre">Renderer.vb</span></tt>.
<tt class="docutils literal"><span class="pre">Run()</span></tt> starts by creating a new <tt class="docutils literal"><span class="pre">Scene</span></tt> object (defined in <tt class="docutils literal"><span class="pre">Scene.vb</span></tt>).
The <tt class="docutils literal"><span class="pre">Scene</span></tt> object&#8217;s constructor sets various local variables and parses the
scene description string&#8212;as read from a file by <tt class="docutils literal"><span class="pre">MainForm.vb</span></tt>&#8212;in its
<tt class="docutils literal"><span class="pre">Parse()</span></tt> method. Once the scene has been parsed, <tt class="docutils literal"><span class="pre">Run()</span></tt> then splits the
image into a number of chunks, and renders each chunk using a different worker
thread. After a pixel has been rendered, <tt class="docutils literal"><span class="pre">Run()</span></tt> puts it on the draw queue,
where it is later emptied by <tt class="docutils literal"><span class="pre">WhileRenderingTick()</span></tt> and rendered.</p>
<p>The ray tracing itself, however, is performed by <tt class="docutils literal"><span class="pre">Scene.Trace()</span></tt> (defined in
<tt class="docutils literal"><span class="pre">Scene.vb</span></tt>). <tt class="docutils literal"><span class="pre">Trace()</span></tt> takes an instance of the <tt class="docutils literal"><span class="pre">Ray</span></tt> class (defined in
<tt class="docutils literal"><span class="pre">SceneObjects.vb</span></tt>) and &#8220;shoots&#8221; it into the scene. If the ray intersects with
an object, the colour of the object at the intersection location is calculated
and combined with the colour of a &#8220;reflected&#8221; ray fired recursively from the
intersection location back out into the scene. The combined value (hit location
colour plus colour of reflected ray) is then returned by <tt class="docutils literal"><span class="pre">Trace()</span></tt>. On the
other hand, if the ray does not intersect with an object, <tt class="docutils literal"><span class="pre">Trace()</span></tt> simply
returns the background colour of the scene. Note that the specific mechanism by
which <tt class="docutils literal"><span class="pre">Trace()</span></tt> works is very difficult to explain in a small document like
this one, and so it is recommended that interested readers investigate <a class="reference external" href="http://www.cs.berkeley.edu/~ravir/">Ravi
Ramamoorthi&#8217;s</a> aforementioned CS184X
course <a class="reference external" href="https://www.edx.org/course/uc-berkeley/cs184-1x/foundations-computer-graphics/576">at EdX</a>.</p>
<p>Once rendering has finished, <tt class="docutils literal"><span class="pre">Renderer.Run()</span></tt> exits, killing the background
thread and alerting <tt class="docutils literal"><span class="pre">MainForm.vb</span></tt> that the render has finished. The user is
then given a &#8220;render finished&#8221; message and the option to save their finished
render.</p>
</div>
</div>


          </div>
        </div>
      </div>
      <div class="sphinxsidebar">
        <div class="sphinxsidebarwrapper">
            <p class="logo"><a href="index.html">
              <img class="logo" src="_static/logo-sidebar.png" alt="Logo"/>
            </a></p>
  <h3><a href="index.html">Table Of Contents</a></h3>
  <ul>
<li><a class="reference internal" href="#">Extending BasicTrace</a><ul>
<li><a class="reference internal" href="#the-10-000ft-view">The 10,000ft View</a></li>
<li><a class="reference internal" href="#addhandler-renderbutton-click-addressof-thissection"><tt class="docutils literal"><span class="pre">AddHandler</span> <span class="pre">RenderButton.Click,</span> <span class="pre">AddressOf</span> <span class="pre">ThisSection</span></tt></a></li>
</ul>
</li>
</ul>

  <h4>Previous topic</h4>
  <p class="topless"><a href="fileformat.html"
                        title="previous chapter">Your First BasicTrace Scene</a></p>
  <h3>This Page</h3>
  <ul class="this-page-menu">
    <li><a href="_sources/devdocs.txt"
           rel="nofollow">Show Source</a></li>
  </ul>
<div id="searchbox" style="display: none">
  <h3>Quick search</h3>
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" />
      <input type="submit" value="Go" />
      <input type="hidden" name="check_keywords" value="yes" />
      <input type="hidden" name="area" value="default" />
    </form>
    <p class="searchtip" style="font-size: 90%">
    Enter search terms or a module, class or function name.
    </p>
</div>
<script type="text/javascript">$('#searchbox').show(0);</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="related">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="genindex.html" title="General Index"
             >index</a></li>
        <li class="right" >
          <a href="fileformat.html" title="Your First BasicTrace Scene"
             >previous</a> |</li>
        <li><a href="index.html">BasicTrace RC1 documentation</a> &raquo;</li> 
      </ul>
    </div>
    <div class="footer">
        &copy; Copyright 2013, Sam Toyer.
      Created using <a href="http://sphinx.pocoo.org/">Sphinx</a> 1.1.3.
    </div>
  </body>
</html>