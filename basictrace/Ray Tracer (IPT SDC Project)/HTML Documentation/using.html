

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
    <title>Basic Rendering with BasicTrace &mdash; BasicTrace RC1 documentation</title>
    
    <link rel="stylesheet" href="_static/default.css" type="text/css" />
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" />
    
    <script type="text/javascript">
      var DOCUMENTATION_OPTIONS = {
        URL_ROOT:    '',
        VERSION:     'RC1',
        COLLAPSE_INDEX: false,
        FILE_SUFFIX: '.html',
        HAS_SOURCE:  true
      };
    </script>
    <script type="text/javascript" src="_static/jquery.js"></script>
    <script type="text/javascript" src="_static/underscore.js"></script>
    <script type="text/javascript" src="_static/doctools.js"></script>
    <link rel="shortcut icon" href="_static/logo.ico"/>
    <link rel="top" title="BasicTrace RC1 documentation" href="index.html" />
    <link rel="next" title="Your First BasicTrace Scene" href="fileformat.html" />
    <link rel="prev" title="Welcome to BasicTrace’s documentation!" href="index.html" /> 
  </head>
  <body>
    <div class="related">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="genindex.html" title="General Index"
             accesskey="I">index</a></li>
        <li class="right" >
          <a href="fileformat.html" title="Your First BasicTrace Scene"
             accesskey="N">next</a> |</li>
        <li class="right" >
          <a href="index.html" title="Welcome to BasicTrace’s documentation!"
             accesskey="P">previous</a> |</li>
        <li><a href="index.html">BasicTrace RC1 documentation</a> &raquo;</li> 
      </ul>
    </div>  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body">
            
  <div class="section" id="basic-rendering-with-basictrace">
<h1>Basic Rendering with BasicTrace<a class="headerlink" href="#basic-rendering-with-basictrace" title="Permalink to this headline">¶</a></h1>
<p>This section will guide you through the user interface of BasicTrace and
introduce some of the concepts used throughout the program. By the end of this
section, you will be able to load and render a supplied scene in BasicTrace,
and understand the various render options BasicTrace gives.</p>
<div class="section" id="the-main-window">
<h2>The Main Window<a class="headerlink" href="#the-main-window" title="Permalink to this headline">¶</a></h2>
<p>Upon starting BasicTrace, you&#8217;ll be greeted with the following window:</p>
<img alt="_images/main-before-render.png" src="_images/main-before-render.png" />
<p>From top to bottom, the main window is split into the following regions.</p>
<div class="section" id="menu-bar">
<h3>Menu Bar<a class="headerlink" href="#menu-bar" title="Permalink to this headline">¶</a></h3>
<img alt="_images/filehelp.png" src="_images/filehelp.png" />
<p>The menu bar is split into two drop-down menu items:</p>
<dl class="docutils">
<dt><strong>File</strong></dt>
<dd><p class="first">Provides options for loading scenes and saving renderings. Note that the &#8220;Save
render&#8221; button is enabled once a render has finished.</p>
<img alt="_images/filemenu.png" class="last" src="_images/filemenu.png" />
</dd>
<dt><strong>Help</strong></dt>
<dd><p class="first">Home to the help system and about box.</p>
<img alt="_images/helpmenu.png" class="last" src="_images/helpmenu.png" />
</dd>
</dl>
</div>
<div class="section" id="render-preview">
<h3>Render Preview<a class="headerlink" href="#render-preview" title="Permalink to this headline">¶</a></h3>
<img alt="_images/preview-before-render.png" src="_images/preview-before-render.png" />
<p>When no rendering has taken place, the render preview pane will simply display a
help string instructing you to load a scene. When rendering is taking place,
however, the render preview window will display the rendered data as it becomes
available, as in the following image of BasicTrace rendering a spiral of spheres:</p>
<img alt="_images/preview-rendering.png" src="_images/preview-rendering.png" />
<p>The above illustration is a good example of the horizontal barring effect which
occurs as BasicTrace splits render work across multiple CPU cores, with each
core choosing to render a certain section of the image in parallel.</p>
</div>
<div class="section" id="configuration">
<h3>Configuration<a class="headerlink" href="#configuration" title="Permalink to this headline">¶</a></h3>
<img alt="_images/config-before-render.png" src="_images/config-before-render.png" />
<p>The configuration area allows you to specify a scene path, change the image
resolution and finally render the scene. Note that the &#8220;Save Render&#8221; button is
initially disabled, but will be automatically enabled once rendering has
finished. Likewise, the &#8220;Render&#8221; button will be replaced by a &#8220;Cancel&#8221; button
once rendering starts. Clicking the &#8220;Cancel&#8221; button at any point during
rendering will cause rendering to terminate immediately.</p>
</div>
<div class="section" id="status-bar">
<h3>Status Bar<a class="headerlink" href="#status-bar" title="Permalink to this headline">¶</a></h3>
<img alt="_images/statusbar-before-render.png" src="_images/statusbar-before-render.png" />
<p>The status bar space is shared between a progress bar and a &#8220;user hint&#8221;. When a
render is in progress, the progress bar will show a green line indicating the
approximate percentage completion of the render, whilst the &#8220;user hint&#8221; will
read &#8220;Rendering...&#8221;:</p>
<img alt="_images/statusbar-rendering.png" src="_images/statusbar-rendering.png" />
<p>At completion, the render bar will display a &#8220;Render finished&#8221; message and
the progress bar will be set to 100%:</p>
<img alt="_images/statusbar-rendered.png" src="_images/statusbar-rendered.png" />
</div>
</div>
<div class="section" id="your-first-render">
<h2>Your First Render<a class="headerlink" href="#your-first-render" title="Permalink to this headline">¶</a></h2>
<p>Now that you&#8217;re familiar with the interface, it&#8217;s time for your first render.
BasicTrace comes with a number of &#8220;testing&#8221; scenes which can be used to verify
that the program is working correctly. In this case, we&#8217;ll be rendering one of
the simplest scenes&#8212;a single unit sphere on a black background. The steps for
accomplishing this are as follows:</p>
<ol class="arabic">
<li><p class="first">In the configuration area of the main window, click the &#8220;Browse&#8221; button
located next to the &#8220;Scene path&#8221; textbox.</p>
<img alt="_images/step1.png" src="_images/step1.png" />
</li>
<li><p class="first">In the file selection window which appears, navigate to the BasicTrace
directory and double click on the file named <tt class="docutils literal"><span class="pre">single-sphere.scn</span></tt> in the
&#8220;Test Scenes&#8221; folder.</p>
<img alt="_images/step2.png" src="_images/step2.png" />
</li>
<li><p class="first">Change the resolution of the image to 128x128 pixels using the resolution
number boxes.</p>
<img alt="_images/step3.png" src="_images/step3.png" />
</li>
<li><p class="first">Click &#8220;Render&#8221; and wait for rendering to complete. You will know rendering is
complete because a &#8220;Finished render&#8221; message will be displayed in the status
bar, and the &#8220;Save Render&#8221; button will be enabled.</p>
<img alt="_images/step4.png" src="_images/step4.png" />
</li>
<li><p class="first">Click the &#8220;Save Render&#8221; button and use the dialog box which appears to choose
an appropriate location for the rendered scene. Note that you may select the
image file format using the dropdown menu in the bottom right-hand corner of
the dialog.  The two supported formats are PNG (<tt class="docutils literal"><span class="pre">.png</span></tt>) and JPEG
(<tt class="docutils literal"><span class="pre">.jpg</span></tt>).</p>
<img alt="_images/step5.png" src="_images/step5.png" />
</li>
</ol>
<p>Assuming no error messages appear, you will have just produced your first render
using BasicTrace. The rendered image should look somewhat like the following:</p>
<img alt="_images/step7.png" src="_images/step7.png" />
<p>Proceed to the next section of the documentation to learn how to create your own
(hopefully more interesting) scenes to render in BasicTrace.</p>
</div>
</div>


          </div>
        </div>
      </div>
      <div class="sphinxsidebar">
        <div class="sphinxsidebarwrapper">
            <p class="logo"><a href="index.html">
              <img class="logo" src="_static/logo-sidebar.png" alt="Logo"/>
            </a></p>
  <h3><a href="index.html">Table Of Contents</a></h3>
  <ul>
<li><a class="reference internal" href="#">Basic Rendering with BasicTrace</a><ul>
<li><a class="reference internal" href="#the-main-window">The Main Window</a><ul>
<li><a class="reference internal" href="#menu-bar">Menu Bar</a></li>
<li><a class="reference internal" href="#render-preview">Render Preview</a></li>
<li><a class="reference internal" href="#configuration">Configuration</a></li>
<li><a class="reference internal" href="#status-bar">Status Bar</a></li>
</ul>
</li>
<li><a class="reference internal" href="#your-first-render">Your First Render</a></li>
</ul>
</li>
</ul>

  <h4>Previous topic</h4>
  <p class="topless"><a href="index.html"
                        title="previous chapter">Welcome to BasicTrace&#8217;s documentation!</a></p>
  <h4>Next topic</h4>
  <p class="topless"><a href="fileformat.html"
                        title="next chapter">Your First BasicTrace Scene</a></p>
  <h3>This Page</h3>
  <ul class="this-page-menu">
    <li><a href="_sources/using.txt"
           rel="nofollow">Show Source</a></li>
  </ul>
<div id="searchbox" style="display: none">
  <h3>Quick search</h3>
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" />
      <input type="submit" value="Go" />
      <input type="hidden" name="check_keywords" value="yes" />
      <input type="hidden" name="area" value="default" />
    </form>
    <p class="searchtip" style="font-size: 90%">
    Enter search terms or a module, class or function name.
    </p>
</div>
<script type="text/javascript">$('#searchbox').show(0);</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="related">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="genindex.html" title="General Index"
             >index</a></li>
        <li class="right" >
          <a href="fileformat.html" title="Your First BasicTrace Scene"
             >next</a> |</li>
        <li class="right" >
          <a href="index.html" title="Welcome to BasicTrace’s documentation!"
             >previous</a> |</li>
        <li><a href="index.html">BasicTrace RC1 documentation</a> &raquo;</li> 
      </ul>
    </div>
    <div class="footer">
        &copy; Copyright 2013, Sam Toyer.
      Created using <a href="http://sphinx.pocoo.org/">Sphinx</a> 1.1.3.
    </div>
  </body>
</html>