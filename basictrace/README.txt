BasicTrace
==========

This repo contains the ray tracer I wrote for a Year 12 course in "information
processing and technology" (read: Visual Basic and Microsoft Access).
Unfortunately, my programming skills (and, more specifically, my VB.NET skills)
left much to be desired, and I was not able to implement all of the features I
might have liked. Thus, the included renderer only renders untextured spheres
against a solid background, and is missing a lot of neat features like
Octree-based ray collision detection and so on. Nonetheless, I hope that you,
dear reader, see some value in the code within, and are not stuck writing
Visual Basic any longer than is absolutely necessary.
