#include "attitude.h"
#include "stabilisation.h"

/* Here's a demo PID controller for your viewing pleasure
kp, ki and kd are the proportional, integral and derivative weightings,
respectively
float <controller name>(float p, float dt) {
    static float integral = 0;
    static float derivative = 0;
    static float old_p = 0;
    integral += p * dt;
    derivative =  (new_p-old_p)/dt;
    old_p = p;
    return kp * p + ki * integral + kd * derivative;
} */

float get_hdg_xte() {
    return attitude_targets.hdg - aircraft_attitude.yaw;
}

float get_pitch_xte() {
    return attitude_targets.pitch - aircraft_attitude.pitch;
}

float get_roll_xte() {
    return attitude_targets.roll - aircraft_attitude.roll;
}

float get_ias_xte() {
    return attitude_targets.ias - aircraft_attitude.velocity;
}

float get_drift_xte() {
    // just return roll :)
    return attitude_targets.roll;
}

void steer(float dt) {
}

void stabilise(float dt) {
}
