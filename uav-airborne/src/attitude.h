#ifndef ATTITUDE_H_GUARD
#define ATTITUDE_H_GUARD
/* Header file for attitude reading routines */

// this structure stores the smoothed, ready to use values for attitude
struct aircraft_attitude_storage {
    float pitch;
    float pitch_rate;
    float yaw;
    float yaw_rate;
    float roll;
    float roll_rate;
    float lat;
    float lon;
    float velocity;
    float alt;
    float climb_rate;
} aircraft_attitude;

// do smoothing and update the aircraft_attitude struct
void localise(float dt);

#endif // ATTITUDE_H_GUARD
