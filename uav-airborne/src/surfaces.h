#ifndef SURFACES_H_GUARD
#define SURFACES_H_GUARD

struct surface_storage {
    float elevator;
    float rudder;
    float ailerons;
    float throttle;
} surface_status;

void set_surfaces();

#endif // SURFACES_H_GUARD
