#ifndef UTIL_H_GUARD
#define UTIL_H_GUARD

#include <stdbool.h>

#include "config.h"

float fpstoms(float fps);
float kttoms(float kts);

float gc_distance(float lat1, float lon1, float lat2, float lon2);
float gc_crs(float lat1, float lon1, float lat2, float lon2);
float gc_xte(float lat0, float lon0, float crs, float lat, float lon);

struct nmea_sentence {
    // stores NMEA sentence information
    char speaker[3];
    char type[4];
    char data[NMEA_DATA_LENGTH + 1];
    char checksum;
};

bool parse_nmea_sentence(char *data, struct nmea_sentence *dest);
bool get_nmea_chunk(struct nmea_sentence *src, char *dest, int n, char **index);

#endif //UTIL_H_GUARD
