#ifndef CONTROLS_H_GUARD
#define CONTROLS_H_GUARD

struct controller_storage {
    float ch1;
    float ch2;
    float ch3;
    float ch4;
    float ch5;
    float ch6;
    float ch7;
} controller_status;

void update_controller_status();

#endif // CONTROLS_H_GUARD
