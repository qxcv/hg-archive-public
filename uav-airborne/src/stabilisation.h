#ifndef STABILISATION_H_GUARD
#define STABILISATION_H_GUARD

struct attitude_target_struct {
    float hdg;
    float alt;
    float pitch;
    float roll;
    float ias;
} attitude_targets;

// here are our XTE functions, note that get_route_xte and get_alt_xte are
// defined in navigation.h because they deal with waypointing
float get_hdg_xte();
float get_pitch_xte();
float get_roll_xte();
float get_ias_xte();

// drift xte is what we use to correct slip/skid with the rudder, here it's just
// a function of our target roll rate. We don't store this in attitude_targets
// for this reason :)
float get_drift_xte();

// change around the targets to get proper altitude/route xte closure rate
void steer(float dt);
// this routine changes the surfaces around to attain attitude_target_struct
void stabilise(float dt);

#endif // STABILISATION_H_GUARD
