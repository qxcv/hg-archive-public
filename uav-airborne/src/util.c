// M_PI is unavailable in vanilla C99
#include <math.h>
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "config.h"
#include "util.h"

float fpstoms(float fps) {
    return 0.3048 * fps;
}

float kttoms(float kts) {
    return 0.5144444444 * kts;
}

// The following formulae are adapted from http://williams.best.vwh.net/avform.htm
// and https://en.wikipedia.org/wiki/Great_circle_distance
// - it is a treasure trove of information if you're feeling too lazy to do the
// trig yourself!

float gc_distance(float lat1, float lon1, float lat2, float lon2) {
    return EARTH_RADIUS * acos(
            sin(lat1) * sin(lat2) + cos(lat1) * cos(lat2) * cos(lon2 - lon1)
        );
}

float gc_crs(float lat1, float lon1, float lat2, float lon2) {
    return fmod(atan2(
            sin(lon1 - lon2) * cos(lat2),
            cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(lon1 - lon2)
        ), 2 * M_PI);
}

float gc_xte(float lat0, float lon0, float crs, float lat, float lon) {
    return asin(
            sin(gc_distance(lat0, lon0, lat, lon) * sin(gc_crs(lat0, lon0, lat, lon) - crs))
        );
}

// NMEA sentence parsing stuff now...

bool parse_nmea_sentence(char *data, struct nmea_sentence *dest) {
    // data is the NMEA string - MUST be null terminated - and dest is a user-
    // initialised nmea_sentence structure to put information into
    int n = strlen(data);
    char *done; // which character have we parsed up to?
    done = data;
    if (*done != '$') return false;
    done++;
    if (strlen(done) < 6) return false;
    strncpy(dest->speaker, done, 2);
    dest->speaker[2] = '\0';
    done += 2;
    strncpy(dest->type, done, 3);
    dest->type[3] = '\0';
    done += 3;
    int ctr = 1;
    while (*done != '*' && *done != '\r' && *done != '\n' && *done != '\0' && ctr < NMEA_DATA_LENGTH) {
        if (done == &(data[n-1])) return false;
        dest->data[ctr-1] = *done;
        dest->checksum ^= *done;
        ctr++;
        done++;
    }
    if (*done == '*') {
        char checksum_chars[3];
        done++;
        checksum_chars[0] = *done;
        if (!*(++done)) return false;
        checksum_chars[1] = *done;
        checksum_chars[2] = '\0'; // nice n' explicit...
        if ((char)strtol(checksum_chars, 0, 16) != dest->checksum) return false;
    }
    while (*(done++) != '\0')
        if (*done != '\r' && *done != '\n' && *done != ' ') return false;
    return true;
}

bool get_nmea_chunk(struct nmea_sentence *src, char *dest, int n, char **index) {
    if (index == NULL) index = (char**)(&(src->data));
    if (**index == '\0') return false;
    while (strlen(*index) > 0) {
        if (**index != ',') {
            if (n > 0) {
                dest[n-1] = **index;
                n--;
            }
        }
        else break;
        (*index)++;
    }
    (*index)++;
    return true;
}
