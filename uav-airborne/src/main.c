#include <stdio.h>

#include "attitude.h"
#include "plat.h"
#include "navigation.h"
#include "stabilisation.h"

void quickmain(float dt);

int main(int argc, char **argv) {
    int rv;
    double time, new_time, dt;
    time = get_time();
    rv = plat_init(argc, argv);
    if (rv != 0) return rv;
    waypoint_index = 0;
    load_flightplan();
    while (rv == 0) {
        new_time = get_time();
        dt = new_time - time;
        time = new_time;
        // rv is not really our rv, just indicates whether to continue
        // magic can be implemented later :)
        rv = plat_step(dt);
        if (rv != 0) break;
        localise(dt);
        if (navigation_status != NS_OFF) {
            navigate(dt);
            if (navigation_status == NS_AUTO) steer(dt);
            stabilise(dt);
        }
        quickmain(dt);
        if (1.0/dt > MAX_TICRATE) sleep((1.0/(float)MAX_TICRATE)-dt);
    }
    return plat_shutdown();
}

void quickmain(float dt) {
        printf("Pitch: %f\n"
               "Roll: %f\n"
               "Yaw: %f\n"
               "Lat: %f\n"
               "Lon: %f\n"
               "Altitude: %f\n"
               "dt: %f\n", 
               aircraft_attitude.pitch, 
               aircraft_attitude.roll,
               aircraft_attitude.yaw,
               aircraft_attitude.lat,
               aircraft_attitude.lon,
               aircraft_attitude.alt,
               dt);
}
