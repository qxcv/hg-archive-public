#include <stdbool.h>

#include "controls.h"
#include "flightgear/flightgear.h"

void update_controller_status() {
    fgcomm_output_data.update = true;
    controller_status.ch1 = fgcomm_output_data.axis0;
    controller_status.ch2 = fgcomm_output_data.axis1;
    controller_status.ch3 = fgcomm_output_data.axis2;
    controller_status.ch4 = fgcomm_output_data.axis3;
    controller_status.ch5 = fgcomm_output_data.axis4;
    controller_status.ch6 = fgcomm_output_data.axis5;
    controller_status.ch7 = 0;
    fgcomm_output_data.update = true;
}

/* ALL deprecated
 * // FGFS controller mappings are easy, we just 1:1 map the joystick if it exists
 * #define JOYSTICK_ROOT "/input/joysticks/js/"
 * // gotta love that concatenation
 * #define AXIS_STATUS(NUM) get_flightgear_property_float(JOYSTICK_ROOT "axis[" NUM "]/binding/setting")
 * 
 * void update_controller_status() {
 *     controller_status.ch1 = AXIS_STATUS("0");
 *     controller_status.ch2 = AXIS_STATUS("1");
 *     controller_status.ch3 = AXIS_STATUS("2");
 *     controller_status.ch4 = AXIS_STATUS("3");
 *     controller_status.ch5 = AXIS_STATUS("4");
 *     controller_status.ch6 = AXIS_STATUS("5");
 *     controller_status.ch7 = 0;
 * } */
