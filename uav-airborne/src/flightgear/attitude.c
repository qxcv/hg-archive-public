#include "attitude.h"
#include "util.h"
#include "flightgear/flightgear.h"

void localise(float dt) {
    fgcomm_output_data.update = true;
    aircraft_attitude.pitch = fgcomm_output_data.pitch;
    aircraft_attitude.pitch_rate = fgcomm_output_data.pitch_rate;
    aircraft_attitude.yaw = fgcomm_output_data.heading;
    aircraft_attitude.yaw_rate = fgcomm_output_data.yaw_rate;
    aircraft_attitude.roll = fgcomm_output_data.roll;
    aircraft_attitude.roll_rate = fgcomm_output_data.roll_rate;
    aircraft_attitude.lat = fgcomm_output_data.latitude;
    aircraft_attitude.lon = fgcomm_output_data.longitude;
    aircraft_attitude.velocity = fgcomm_output_data.knots;
    aircraft_attitude.alt = fgcomm_output_data.altitude;
    aircraft_attitude.climb_rate = fgcomm_output_data.vertical_speed;
}
