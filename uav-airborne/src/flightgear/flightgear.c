#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#include "bpack.h"
#include "flightgear/flightgear.h"

// internal function used by fgcomm_receive_data
static bool scan_sentinel(uint8_t *sentinel, size_t len);

void shutdown_simulation(int rv) {
    printf("Shutting down simulation...\n");
    close(sockfd);
    exit(rv);
}

void fgcomm_send_data() {
    uint8_t dest[4 * 8]; // we have 32 bytes of data
    pack("!dddd", dest, fgcomm_input_data.elevator, fgcomm_input_data.rudder,
            fgcomm_input_data.aileron, fgcomm_input_data.throttle);
    send(sockfd, dest, 4 * 8, 0);
}

static bool scan_sentinel(uint8_t *sentinel, size_t len) {
    if (len == 0) return true;
    bool rv = true;
    size_t i;
    uint8_t *buffer = malloc(len);
    recv(sockfd, buffer, len, MSG_PEEK);
    for (i = 0; i < len; i++) {
        if (buffer[i] != sentinel[i]) {
            rv = false;
            break;
        }
        i++;
    }
    free(buffer);
    return rv;
}

void fgcomm_receive_data() {
    uint8_t src[17 * 8];
    uint8_t sentinel[] = {0xde, 0xad, 0xbe, 0xaf}; // 0xdeadbeef
    size_t sentinel_length = sizeof(sentinel)/sizeof(sentinel[0]);
    uint8_t *buffer = malloc(sentinel_length);
    while (!scan_sentinel(sentinel, sentinel_length)) recv(sockfd, buffer, 1, 0);
    recv(sockfd, buffer, sentinel_length, 0);
    free(buffer);
    recv(sockfd, src, 17 * 8, 0);
    unpack("!dddddddddddddddd", src, // 17 doubles :o
            &(fgcomm_output_data.pitch),
            &(fgcomm_output_data.pitch_rate),
            &(fgcomm_output_data.heading),
            &(fgcomm_output_data.yaw_rate),
            &(fgcomm_output_data.roll),
            &(fgcomm_output_data.roll_rate),
            &(fgcomm_output_data.latitude),
            &(fgcomm_output_data.longitude),
            &(fgcomm_output_data.knots),
            &(fgcomm_output_data.altitude),
            &(fgcomm_output_data.vertical_speed),
            &(fgcomm_output_data.axis0),
            &(fgcomm_output_data.axis1),
            &(fgcomm_output_data.axis2),
            &(fgcomm_output_data.axis3),
            &(fgcomm_output_data.axis4),
            &(fgcomm_output_data.axis5)
    );
}
