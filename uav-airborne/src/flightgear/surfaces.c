#include <stdbool.h>

#include "surfaces.h"
#include "flightgear/flightgear.h"

void set_surfaces() {
    fgcomm_input_data.sent = false;
    fgcomm_input_data.elevator = surface_status.elevator;
    fgcomm_input_data.rudder = surface_status.rudder;
    fgcomm_input_data.aileron = surface_status.ailerons;
    fgcomm_input_data.throttle = surface_status.throttle;
}
