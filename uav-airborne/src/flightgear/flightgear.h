#ifndef FLIGHTGEAR_FLIGHTGEAR_H_GUARD
#define FLIGHTGEAR_FLIGHTGEAR_H_GUARD

#include <stdbool.h>

int sockfd;

void shutdown_simulation(int rv);

struct fgcomm_input_data_struct {
    bool sent;
    double elevator;
    double rudder;
    double aileron;
    double throttle;
} fgcomm_input_data;

struct fgcomm_output_data_struct {
    bool update;
    double pitch;
    double pitch_rate;
    double heading;
    double yaw_rate;
    double roll;
    double roll_rate;
    double latitude;
    double longitude;
    double knots;
    double altitude;
    double vertical_speed;
    double axis0;
    double axis1;
    double axis2;
    double axis3;
    double axis4;
    double axis5;
} fgcomm_output_data;

void fgcomm_send_data();
void fgcomm_receive_data();

#endif //FLIGHTGEAR_FLIGHTGEAR_H_GUARD
