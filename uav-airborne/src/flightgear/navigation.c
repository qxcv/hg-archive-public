#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

#include "config.h"
#include "navigation.h"
#include "util.h"

bool load_flightplan(void) {
    char *linebuffer;
    struct nmea_sentence current_sentence;
    size_t n;
    int lineno = 1;
    int wpno = 0;
    FILE *fpfile;
    // by default we decide to fly to west africa at 2000ft, because why the
    // hell not?
    waypoint_stack[wpno] = (struct waypoint){.lat = 0, .lon = 0, .altitude = 609, .flags = AP_LOITER};
    if ((fpfile = fopen("flightplan.txt", "r")) == NULL) {
        printf("Couldn't locate flightplan.txt in cwd, loading default\n");
        return false;
    }
    // the amount of memory allocated is arbitrary - getline will allocate
    // as much as it needs
    linebuffer = malloc(100);
    n = 100;
    while (getline(&linebuffer, &n, fpfile) != 0 && wpno < WAYPOINT_STACKSIZE) {
        if (n > 0 && linebuffer[0] == '$') {
            if (!parse_nmea_sentence(linebuffer, &current_sentence)) {
                printf("Error whilst parsing flightplan.txt on line #%i: "
                       "invalid sentence \"%s\"\n", lineno, linebuffer);
                free(linebuffer);
                fclose(fpfile);
                return false;
            }
            if (!nmea_to_waypoint(&current_sentence, &(waypoint_stack[wpno]))) {
                printf("Error whilst converting NMEA sentence \"%s\" to "
                       "waypoint on line %i\n", linebuffer, lineno);
                free(linebuffer);
                fclose(fpfile);
                return false;
            }
            wpno++;
        }
        lineno++;
    }
    free(linebuffer);
    fclose(fpfile);
    return true;
}
