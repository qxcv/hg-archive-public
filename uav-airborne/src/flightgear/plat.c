#include <arpa/inet.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <time.h>

#include "flightgear/flightgear.h"
#include "plat.h"
#include "surfaces.h"

double get_time() {
    struct timespec tv;
    double rv = 0;
    // CLOCK_MONOTONIC is a per-process clock which gives wall clock time and
    // does not jump about
    clock_gettime(CLOCK_MONOTONIC, &tv);
    rv += tv.tv_sec;
    // tv_nsec is nanoseconds, and there are 10**9 nanoseconds in a second
    rv += (double)(tv.tv_nsec)/(1000000000);
    return rv;
}

int plat_init(int argc, char **argv) {
    int portnum;
    if (argc != 3) {
        printf("Flightgear Autopilot Simulation Utility\n"
               "USAGE: %s <flightgear host> <flightgear port>\n", argv[0]);
        return 1;
    }
    // according to http://publib.boulder.ibm.com/infocenter/lnxpcomp/v8v101/index.jsp?topic=%2Fcom.ibm.xlcpp8l.doc%2Flanguage%2Fref%2Fstrin.htm
    // this initialises arithmetic types to 0 and pointers to null (with the
    // excpetion of sin_family), which is what we want!
    // I've honestly no idea why we can't just use empty braces, but obviously
    // ANSI knows better :)
    struct sockaddr_in fgcmdserver = {.sin_family=AF_INET};
    if (inet_pton(AF_INET, argv[1], &fgcmdserver.sin_addr) != 1) {
        printf("Invalid address supplied, %s\n", argv[1]);
        return 1;
    }
    if ((portnum = strtol(argv[2], NULL, 10)) < 1) {
        printf("Invalid port number %s\n", argv[2]);
        return 1;
    }
    fgcmdserver.sin_port = htons(portnum);
    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (bind(sockfd, (struct sockaddr *) &fgcmdserver, sizeof(struct sockaddr_in)) != 0) {
        printf("Connection failed, could not connect to %s:%s\n", argv[1], argv[2]);
        return 1;
    }
    return 0;
}

int plat_shutdown() {
    shutdown_simulation(0);
    return 0;
}

int plat_step(float dt) {
    fgcomm_receive_data();
    fgcomm_send_data();
    return 0;
}

void sleep(float seconds) {
    struct timespec timing;
    timing.tv_sec = floor(seconds);
    timing.tv_nsec = (seconds - floor(seconds))/(float)1000000000;
    nanosleep(&timing, NULL);
}
