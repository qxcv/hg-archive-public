#ifndef CONFIG_H_GUARD
#define CONFIG_H_GUARD

#define WAYPOINT_STACKSIZE 0xFF
#define ALTITUDE_GOOD_ZONE 4.0
#define ALTITUDE_GOOD_ZONE_STRICT 1.5

// This is an approximation in m, taken from Wikipedia
#define EARTH_RADIUS 6399594

#define NMEA_DATA_LENGTH 256

// maximum number of cycles per second, in hz
#define MAX_TICRATE 30

#endif //CONFIG_H_GUARD
