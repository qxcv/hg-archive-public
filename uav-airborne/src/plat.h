#ifndef PLAT_H_GUARD
#define PLAT_H_GUARD

double get_time();

int plat_init(int argc, char **argv);
int plat_shutdown();
int plat_step(float dt);

void sleep(float seconds);

#endif //PLAT_H_GUARD
