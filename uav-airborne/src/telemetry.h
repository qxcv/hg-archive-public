#ifndef TELEMETRY_H_GUARD
#define TELEMETRY_H_GUARD

#include <stdbool.h>

// send data over a dumb pipe, does not have to be
// delimited
bool telem_broadcast_data(char data[]);
// receive data from the same dumb pipe, up to n chars
bool telem_recv_data(char *data, int n);

#endif // TELEMETRY_H_GUARD
