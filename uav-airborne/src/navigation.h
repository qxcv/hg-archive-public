#ifndef NAVIGATION_H_GUARD
#define NAVIGATION_H_GUARD

#include <stdbool.h>

#include "config.h"
#include "util.h"

enum nav_status {
    NS_AUTO, /* the aircraft flies itself */
    NS_FBW, /* manual, but with stabilisation (Fly By Wire) */
    NS_MANUAL, /* manual, pass RC commands straight to servos */
    NS_OFF, /* manual, no navigation at all */
};

enum autopilot_modes {
    AP_ROUTE, // go from the previous waypoint (if there is one) to the current
              // waypoint
    AP_LOITER, // circle the current waypoint indefinitely
    AP_LANDING, // land on the ground. Wheeee!
};

enum waypoint_flags {
    WP_LANDING = 1 << 0, /* the aircraft should land here */
    WP_STRICT = 1 << 1, /* this waypoint absolutely MUST be crossed at the
                           specified position */
    WP_LOITER = 1 << 2, /* the aircraft should indefinitely loiter at this
                           location */
};

struct waypoint {
    float lat;
    float lon;
    float altitude;
    int flags; /* bitwise-or flags of type enum waypoint_flags */
};

struct waypoint waypoint_stack[WAYPOINT_STACKSIZE];
int waypoint_index;
enum nav_status navigation_status;
enum autopilot_modes autopilot_status;

bool load_flightplan(void);

// takes NMEA in the form "$FPWAP,<lat>,<lon>,<alt>,<landing?>,<strict?>,<loiter?>,*<checksum>
bool nmea_to_waypoint(struct nmea_sentence *src, struct waypoint *dest);
bool reached_waypoint_latlon(struct waypoint *current_waypoint);
bool reached_waypoint_altitude(struct waypoint *current_waypoint);
float get_route_xte();
void navigate(float dt);

#endif // NAVIGATION_H_GUARD
