#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "attitude.h"
#include "config.h"
#include "navigation.h"
#include "util.h"

bool nmea_to_waypoint(struct nmea_sentence *src, struct waypoint *dest) {
    if (strncmp(src->speaker, "FP", 2) != 0 || strncmp(src->type, "WPA", 3) != 0) return false;
    char buffer[6][100];
    char nullbuffer[10];
    char **nullindex = NULL;
    char **index = NULL;
    int i;
    for (i = 0; i < 6; i++) {
        if (!get_nmea_chunk(src, buffer[i], 99, index)) return false; // too few args
    }
    if (get_nmea_chunk(src, nullbuffer, 9, nullindex)) return false; // too many args
    if ((dest->lon = strtof(buffer[0], NULL)) == 0 ||
        (dest->lat = strtof(buffer[1], NULL)) == 0 ||
        (dest->altitude = strtof(buffer[2], NULL)) == 0) return false;
    dest->flags = 0;
    if (buffer[3][0] != '\0') dest->flags |= WP_LANDING;
    if (buffer[4][0] != '\0') dest->flags |= WP_STRICT;
    if (buffer[5][0] != '\0') dest->flags |= WP_LOITER;
    return true;
}

bool reached_waypoint_latlon(struct waypoint *current_waypoint) {
    return gc_distance(current_waypoint->lat, current_waypoint->lon, aircraft_attitude.lat, aircraft_attitude.lon)
        < ((current_waypoint->flags & WP_STRICT) ? ALTITUDE_GOOD_ZONE_STRICT : ALTITUDE_GOOD_ZONE);
}

bool reached_waypoint_altitude(struct waypoint *current_waypoint) {
    return abs(current_waypoint->altitude - aircraft_attitude.alt) < ALTITUDE_GOOD_ZONE;
}

void navigate(float dt) {
    struct waypoint *current_waypoint = &(waypoint_stack[waypoint_index]);
    if (reached_waypoint_latlon(current_waypoint)) {
        if (reached_waypoint_altitude(current_waypoint)) {
            if (!(current_waypoint->flags & (WP_LOITER | WP_LANDING))) {
                // Neither WP_LANDING nor WP_LOITER are set, let's move on
                waypoint_index = (waypoint_index + 1) % WAYPOINT_STACKSIZE;
            }
            else {
                // we're either loitering OR landing,
                // plus we've reached our destination :)
            }
        }
        else {
            // we're not at the right altitude yet
            autopilot_status = (current_waypoint->flags & WP_LANDING) ? AP_LANDING : AP_LOITER;
        }
    }
}
